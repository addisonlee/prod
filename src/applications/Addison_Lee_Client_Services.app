<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Access for all teams within Client Services</description>
    <label>Addison Lee Client Services</label>
    <logo>SharedDocuments/Addison_Lee_Logo.png</logo>
    <tab>standard-Account</tab>
    <tab>standard-Case</tab>
    <tab>Competitor__c</tab>
    <tab>standard-Contract</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Chatter</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Document</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-ContentSearch</tab>
    <tab>InSituCah__Ultimate_Parent__c</tab>
    <tab>standard-Workspace</tab>
    <tab>Interaction__c</tab>
    <tab>Promo_Code__c</tab>
</CustomApplication>
