<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Accident_Notification</fullName>
        <ccEmails>accountmanagers@westonecars.co.uk</ccEmails>
        <description>Accident Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Accident_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Manager_notification_of_new_case</fullName>
        <description>Account Manager notification of new case</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_Manager_Notification_for_New_Cases</template>
    </alerts>
    <alerts>
        <fullName>Addison_Lee_Case_Auto_Response</fullName>
        <description>Addison Lee Case Auto-Response</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Addison_Lee_Client_Relations_Case_Auto_Response_Email_Phone</template>
    </alerts>
    <alerts>
        <fullName>Case_Near_Violation</fullName>
        <description>Case Near Violation</description>
        <protected>false</protected>
        <recipients>
            <field>test_Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>test_Line_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Violation</template>
    </alerts>
    <alerts>
        <fullName>Case_Updated</fullName>
        <description>Case Updated</description>
        <protected>false</protected>
        <recipients>
            <recipient>Addison_Lee_Premier_Agent</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Premier_Call_Centre_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Premier_Team_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Case_Updated</template>
    </alerts>
    <alerts>
        <fullName>Case_Very_Near_Violation</fullName>
        <description>Case Very Near Violation</description>
        <protected>false</protected>
        <recipients>
            <field>test_Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>test_Line_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Violation1</template>
    </alerts>
    <alerts>
        <fullName>Coca_Cola_New_Case_Auto_Response</fullName>
        <description>Coca-Cola New Case Auto-Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>coca-cola@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Coca_Cola_Account_Exec_Case_Auto_Response_Email_Phone</template>
    </alerts>
    <alerts>
        <fullName>Driver_Liaison_Comments_Made</fullName>
        <description>Driver Liaison Comments Made</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Driver_Liaison_Comments_Made</template>
    </alerts>
    <alerts>
        <fullName>Driver_Liaison_Serious_Allegation_Notification</fullName>
        <description>Driver Liaison Serious Allegation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Driver_Liaison_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Driver_Support_Team_Leader</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Driver_Liaison_Incident_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_of_New_Case</fullName>
        <description>Email Notification of New Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>alyssia.foster@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ann.fairclough@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gurvinder.manku@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jamieb@addisonlee.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>justin.sambridge@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee.oneill@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paul.hayes@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Global_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>NY_Vendor_Manager_New_Case_to_Action</fullName>
        <description>NY Vendor Manager New Case to Action</description>
        <protected>false</protected>
        <recipients>
            <recipient>Implementation_Manager_New_York</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Vendor_Manager_New_Case_to_Action</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Email_Account_Manager</fullName>
        <description>New Case Email Account Manager</description>
        <protected>false</protected>
        <recipients>
            <field>test_Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_Manager_Notification_for_New_Cases</template>
    </alerts>
    <alerts>
        <fullName>Please_Approve_Refund</fullName>
        <description>Please Approve Refund</description>
        <protected>false</protected>
        <recipients>
            <recipient>despina.theochari@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>donna.bloomfield@addisonlee.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>grace.kelley@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>louise.elms@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stephanie.marks@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Refund_Approval</template>
    </alerts>
    <alerts>
        <fullName>Send_Feedback_Comment_Email_Alert</fullName>
        <description>Send Feedback Comment Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>FeedBackEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Templates/Feedback_Comments</template>
    </alerts>
    <alerts>
        <fullName>UK_Vendor_Manager_New_Case_to_Action</fullName>
        <description>UK Vendor Manager New Case to Action</description>
        <protected>false</protected>
        <recipients>
            <recipient>Global_Network_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>julia.doctoroff@addisonlee.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Vendor_Manager_New_Case_to_Action</template>
    </alerts>
    <alerts>
        <fullName>Vendor_Manager_Comments_Made</fullName>
        <description>Vendor Manager Comments Made</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>julia.doctoroff@addisonlee.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Vendor_Manager_Comments_Made</template>
    </alerts>
    <alerts>
        <fullName>WestOne_Case_Auto_Response</fullName>
        <description>WestOne Case Auto-Response</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Archived_Email_Templates/WestOne_Case_Auto_Response_Email_Phone</template>
    </alerts>
    <fieldUpdates>
        <fullName>Accident</fullName>
        <description>Update the type to accident</description>
        <field>Type</field>
        <literalValue>Accident</literalValue>
        <name>Accident</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Priority_to_High</fullName>
        <description>Update case priority to high</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Case Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Priority_to_Low</fullName>
        <description>Changes the Case Priority to Low</description>
        <field>Priority</field>
        <literalValue>Low</literalValue>
        <name>Case Priority to Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Priority_to_Medium</fullName>
        <description>Updates the CAse Priority to Medium</description>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Case Priority to Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Priority_to_Urgent</fullName>
        <description>Update Case Priority to Urgent.</description>
        <field>Priority</field>
        <literalValue>Urgent</literalValue>
        <name>Case Priority to Urgent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Re_Opened_Marker</fullName>
        <description>Update the checkbox to mark if a case has ever been reopened</description>
        <field>Has_the_Case_ever_been_Re_Opened__c</field>
        <literalValue>1</literalValue>
        <name>Case Re-Opened Marker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Resolution</fullName>
        <description>Used for refunds process. Update the case resolution to refund when case is submitted for refund approval.</description>
        <field>Reason</field>
        <literalValue>Refund</literalValue>
        <name>Case Resolution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_PriorValue</fullName>
        <field>Case_Status_PriorValue__c</field>
        <formula>TEXT(PRIORVALUE(Status))</formula>
        <name>Case Status PriorValue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Open</fullName>
        <description>Updates the Status to Open</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Case Update Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Re_Opened_Checkbox</fullName>
        <description>Update the case re-opened checkbox to true. Re-Opened checkbox drives milestones on a case.</description>
        <field>Case_Re_Opened__c</field>
        <literalValue>1</literalValue>
        <name>Check &quot;Re-Opened&quot; Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <description>Update case status to closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complaint_Tick_Field_Update</fullName>
        <field>Complaint__c</field>
        <literalValue>1</literalValue>
        <name>Complaint Tick Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Gross_Amount</fullName>
        <description>Copy gross amount to TECH field</description>
        <field>Gross_Amount_To_Refund__c</field>
        <formula>TECH_Gross_Amount__c</formula>
        <name>Copy Gross Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Driver_Feedback</fullName>
        <field>Rate_My_Driver__c</field>
        <literalValue>1</literalValue>
        <name>Driver Feedback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Driver_Liaison_Reassigment</fullName>
        <field>OwnerId</field>
        <lookupValue>Driver_Liaison_Admin_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Driver Liaison Reassigment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Driver_Liaison_Reassigment_1</fullName>
        <field>OwnerId</field>
        <lookupValue>Driver_Liaison_Admin_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Driver Liaison Reassigment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Driver_Liaison_Collaborator</fullName>
        <field>Driver_Liaison_Collaborator__c</field>
        <formula>$User.FirstName + &quot; &quot;+ $User.LastName</formula>
        <name>Populate Driver Liaison Collaborator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Vendor_Manager_Collaborator</fullName>
        <field>Vendor_Manager_Collaborator__c</field>
        <formula>$User.FirstName + &quot; &quot;+ $User.LastName</formula>
        <name>Populate Vendor Manager Collaborator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prepopulate_Case_Subject</fullName>
        <description>Prepopulates the Case Subject with a set format: &apos;Origin - Query Type - Email&apos;</description>
        <field>Subject</field>
        <formula>TEXT(Origin) + &quot; - &quot; +  TEXT(Web_Query_Type__c) + &quot; - &quot; + SuppliedEmail</formula>
        <name>Prepopulate Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_Case_Owner_Account_Exec</fullName>
        <description>Change case owner to account executive queue</description>
        <field>OwnerId</field>
        <lookupValue>Account_Executive_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign Case Owner - Account Exec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_Case_Owner_Customer_Relations</fullName>
        <description>Change case owner to customer relations queue</description>
        <field>OwnerId</field>
        <lookupValue>Customer_Relations_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reassign Case Owner - Customer Relations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Approved</fullName>
        <description>Update case refund status to approved</description>
        <field>Refund_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Record Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Refund_Issued</fullName>
        <description>Sets the refund issued checkbox on a case to true once the approval is approved</description>
        <field>Refund_Issued__c</field>
        <literalValue>1</literalValue>
        <name>Refund Issued</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Refund</fullName>
        <description>Refund Status picklist set to Rejected if approval is rejected</description>
        <field>Refund_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Refund</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Share_Case_with_Driver_Liaison</fullName>
        <field>Share_with_Driver_Liaison__c</field>
        <literalValue>1</literalValue>
        <name>Share Case with Driver Liaison</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Share_Case_with_NY_Vendor_Manager</fullName>
        <field>Share_with_Vendor_Manager_NY__c</field>
        <literalValue>1</literalValue>
        <name>Share Case with NY Vendor Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Share_Case_with_UK_Vendor_Manager</fullName>
        <field>Share_with_Vendor_Manager_UK__c</field>
        <literalValue>1</literalValue>
        <name>Share Case with UK Vendor Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Subject_Update</fullName>
        <field>Subject</field>
        <formula>&apos;NPS Feedback&apos; + &apos;-&apos; + TEXT( NPS_Score__c) + &apos;-&apos; + TEXT( NPS_Category__c )</formula>
        <name>Subject Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_SLA_Status_Amber</fullName>
        <description>Update current milestone status to near violation</description>
        <field>Case_Milestone_Status__c</field>
        <literalValue>Near Violation</literalValue>
        <name>Update Case SLA Status Amber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_SLA_Status_Green</fullName>
        <description>Update current milestone status to compliant</description>
        <field>Case_Milestone_Status__c</field>
        <literalValue>Compliant</literalValue>
        <name>Update Case SLA Status Green</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_SLA_Status_Red</fullName>
        <description>Update current milestone status to very near violation</description>
        <field>Case_Milestone_Status__c</field>
        <literalValue>Very Near Violation</literalValue>
        <name>Update Case SLA Status Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated_to_TRUE</fullName>
        <description>Update escalated checkbox to true</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Update Escalated to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Accident_Queue</fullName>
        <description>Update case owner to non-managed accident queue</description>
        <field>OwnerId</field>
        <lookupValue>Non_Managed_Accident_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Accident Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Re_Opened_Checkbox_to_FALSE</fullName>
        <description>Update re-opened checkbox to false. Re-opened checkbox used in milestone criteria</description>
        <field>Case_Re_Opened__c</field>
        <literalValue>0</literalValue>
        <name>Update Re-Opened Checkbox to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Relevant_City_to_New_York</fullName>
        <description>Updates the Relevant City to the value &quot;New York.&quot;</description>
        <field>Relevant_City__c</field>
        <literalValue>New York</literalValue>
        <name>Update Relevant City to New York</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Web_User_for_Cases</fullName>
        <field>OwnerId</field>
        <lookupValue>Customer_Relations_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Web User for Cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Web_User_for_Cases_AE</fullName>
        <description>Managed</description>
        <field>OwnerId</field>
        <lookupValue>Account_Executive_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Web User for Cases AE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1st_response</fullName>
        <field>X1st_Reponse__c</field>
        <literalValue>0</literalValue>
        <name>1st response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Addison Lee Manual Auto-Response</fullName>
        <actions>
            <name>Addison_Lee_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_email_sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Addison Lee Customer Relations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send email to simulate auto-response on manual creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assignment for AL Accident Cases</fullName>
        <actions>
            <name>Update_Owner_to_Accident_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Accident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Addison Lee Account Executives,Email - Addison Lee Credit Suisse,Email - Addison Lee Customer Relations,Phone - Addison Lee,Phone - WestOne,Twitter - Addison Lee,Website - Addison Lee,Website - Bespoke</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Web_Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>Addison Lee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <description>Assign Cases to the Accident queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Email Account Manager when New Case Created</fullName>
        <actions>
            <name>New_Case_Email_Account_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Account_Manager__c</field>
            <operation>equals</operation>
            <value>James Delaney</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Number__c</field>
            <operation>notEqual</operation>
            <value>99621</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Parent_Account_Number__c</field>
            <operation>notEqual</operation>
            <value>99621</value>
        </criteriaItems>
        <description>Emails account manager when a new case is submitted for an account that they&apos;re the account manager for. Trialling with James Delaney first.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Near Violation</fullName>
        <actions>
            <name>Case_Near_Violation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Near_Violation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Milestone_Status__c</field>
            <operation>equals</operation>
            <value>Near Violation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When Case Milestone Status Is Near Violation, Sending An Email to Account Manager n Line Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to High for Compensation</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Compensation</value>
        </criteriaItems>
        <description>Updaets the Priority to High if the Case type = Compensation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to High for Complaints</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Complaint__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Changes the Priority to High if the Complaint check box is ticked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to High for Web-App</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Web-App</value>
        </criteriaItems>
        <description>Updates the Priority to High if the Category = Web-App</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Low for Admin</fullName>
        <actions>
            <name>Case_Priority_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Admin</value>
        </criteriaItems>
        <description>Updates the Priority to Low for Category = Admin</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Low for Client</fullName>
        <actions>
            <name>Case_Priority_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>Updates the Priority to Low if the Category = Client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Medium for Call Centr</fullName>
        <actions>
            <name>Case_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Call Centre</value>
        </criteriaItems>
        <description>Updates the Priority to Medium if the Category = Call Ctr</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Medium for Control</fullName>
        <actions>
            <name>Case_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Control</value>
        </criteriaItems>
        <description>Updates the Priority to Medium if Category = Control</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Medium for Driver - Error</fullName>
        <actions>
            <name>Case_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Driver/Rider Error</value>
        </criteriaItems>
        <description>Updates the Priority to Medium if the Case Type = Driver Error</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority to Medium for Fraud</fullName>
        <actions>
            <name>Case_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Fraud</value>
        </criteriaItems>
        <description>Set Case Priority to Medium for Fraud</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Re-Opened Marker</fullName>
        <actions>
            <name>Case_Re_Opened_Marker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Re_Opened__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Checkbox to indicate if the case has ever been reopened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status PriorValue</fullName>
        <actions>
            <name>Case_Status_PriorValue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>sets the prior value of the Case Status when Status is Changed</description>
        <formula>ISCHANGED(Status)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Survey Subject</fullName>
        <actions>
            <name>Subject_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>NPS Feedback</value>
        </criteriaItems>
        <description>When case is created for Survey , subject will be updated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Update Status on Allocation</fullName>
        <actions>
            <name>Case_Update_Status_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Status to Open when the Case is initially allocated</description>
        <formula>AND( ISCHANGED( OwnerId ),( ISPICKVAL( PRIORVALUE( Status ) , &apos;New&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Very Near Violation</fullName>
        <actions>
            <name>Case_Very_Near_Violation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Very_Near_Violation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Milestone_Status__c</field>
            <operation>equals</operation>
            <value>Very Near Violation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When Case is Very Near Violation,send email to Account Manager n Line Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Coca-Cola New Case Auto-Response</fullName>
        <actions>
            <name>Coca_Cola_New_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Coca-Cola</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>thank you,New case email notification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>helpdesk@addisonlee.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Centre No Action 2 Hours</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Contact Center Admin Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Open</value>
        </criteriaItems>
        <description>Will update status to urgent 2 hours after a case is created and the status is still new or open. Only applies to case origin = Email - Contact Center Admin Team.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Priority_to_Urgent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Driver Liaison Comments Made</fullName>
        <actions>
            <name>Driver_Liaison_Comments_Made</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email alert to Case Owner when Driver Liaison changes status from &quot;Driver Liaison - in Progress&quot; back to &quot;Open&quot; or &quot;In Progress&quot;</description>
        <formula>AND  (  OR(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Status,&quot;In Progress&quot;)),   Case_Status_PriorValue__c=&quot;In Progress - With Driver Liaison&quot;   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Driver Liaison Further Action Required</fullName>
        <actions>
            <name>Driver_Liaison_Reassigment_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Driver_Liaison_Action_Taken__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Driver /Rider Error,Driver Feedback,Serious Misconduct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This is a reassignment for DL when a query has been completed by other departments</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NY Cases Update Relevant City</fullName>
        <actions>
            <name>Update_Relevant_City_to_New_York</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Addison Lee Credit Suisse - NY,Unilever NY - Email</value>
        </criteriaItems>
        <description>When the Case Origin = &quot;Email - Addison Lee Credit Suisse - NY&quot; OR  &quot;Unilever NY - Email,&quot; the Relevant City will be updated to &quot;New York.&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Vendor Manager Case -- New York</fullName>
        <actions>
            <name>NY_Vendor_Manager_New_Case_to_Action</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - With Vendor Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Vendor_Manager_Region__c</field>
            <operation>equals</operation>
            <value>New York</value>
        </criteriaItems>
        <description>To notify NY vendor manager of a new case for them to work on.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Vendor Manager Case -- UK</fullName>
        <actions>
            <name>UK_Vendor_Manager_New_Case_to_Action</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - With Vendor Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Vendor_Manager_Region__c</field>
            <operation>equals</operation>
            <value>United Kingdom</value>
        </criteriaItems>
        <description>To notify UK vendor manager of a new case for them to work on.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification Email</fullName>
        <actions>
            <name>Email_Notification_of_New_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Global Query International,Email - Global Query National</value>
        </criteriaItems>
        <description>Email notifications of new case for Global team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification for W1 Accident Cases</fullName>
        <actions>
            <name>Accident_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Accident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - WestOne,Phone - WestOne,Website - WestOne</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Web_Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>WestOne</value>
        </criteriaItems>
        <description>Notification sent out for W1 Accidents raised</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate DL Collaborator</fullName>
        <actions>
            <name>Populate_Driver_Liaison_Collaborator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Driver_Liaison_Picked_Up__c=TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Gross Amount</fullName>
        <actions>
            <name>Copy_Gross_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Gross Amount if net amount is added</description>
        <formula>AND(NOT(ISNULL(  TECH_Gross_Amount__c ) ),TECH_Gross_Amount__c  &gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Vendor Manager Collaborator</fullName>
        <actions>
            <name>Populate_Vendor_Manager_Collaborator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates name of current user when Vendor Manager Picked Up = TRUE.</description>
        <formula>Vendor_Manager_Picked_Up__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Premier Closed Case Notification</fullName>
        <actions>
            <name>Case_Updated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Premier</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prepopulate Case Subject for web%2Finternal cases</fullName>
        <actions>
            <name>Prepopulate_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Call Centre - Internal,Intranet - Internal,Website - Addison Lee,Website - Bespoke,Website - WestOne</value>
        </criteriaItems>
        <description>Prepopulates the Case Subject using a standard format.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Rate My Driver</fullName>
        <actions>
            <name>Driver_Feedback</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - APP Feedback</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reassign Case Owner - Account Exec</fullName>
        <actions>
            <name>Reassign_Case_Owner_Account_Exec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reassign Case Owner to Account Executive Queue when the Case is Re-Opened</description>
        <formula>Case_Re_Opened__c = TRUE &amp;&amp; NOT(ISPICKVAL(Account.Sales_Ledger__c, &quot;WestOne Sales Ledger&quot;)) &amp;&amp; (Account.Account_Managed__c = TRUE ||  Account.Parent_Account_Managed__c = TRUE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reassign Case Owner - Customer Relations</fullName>
        <actions>
            <name>Reassign_Case_Owner_Customer_Relations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reassign Case Owner to Customer Relations Queue when the Case is Re-Opened</description>
        <formula>Case_Re_Opened__c = TRUE &amp;&amp; NOT(ISPICKVAL(Account.Sales_Ledger__c, &quot;WestOne Sales Ledger&quot;)) &amp;&amp; Account.Account_Managed__c = FALSE &amp;&amp; Account.Parent_Account_Managed__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback Comment Email</fullName>
        <actions>
            <name>Send_Feedback_Comment_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Send_Feedback_Comment_Emial__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send Feedback Comment Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serious Allegation Notification</fullName>
        <actions>
            <name>Driver_Liaison_Serious_Allegation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Driver Incidents Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - With Driver Liaison</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Serious Incident - Allegation,Serious Misconduct</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Share Case with Driver Liaison</fullName>
        <actions>
            <name>Share_Case_with_Driver_Liaison</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 

Driver_Liaison_For_Review__c=TRUE, 
ISPICKVAL(Status, &quot;In Progress - With Driver Liaison&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Share Case with NY Vendor Manager</fullName>
        <actions>
            <name>Share_Case_with_NY_Vendor_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Makes sure that the case stays shared with the NY Vendor Manager team.</description>
        <formula>AND( ISPICKVAL( Vendor_Manager_Region__c, &quot;New York&quot;) ,  ISPICKVAL(Status, &quot;In Progress - With Vendor Manager&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Share Case with UK Vendor Manager</fullName>
        <actions>
            <name>Share_Case_with_UK_Vendor_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Makes sure that the case stays shared with the UK Vendor Manager team.</description>
        <formula>AND( ISPICKVAL( Vendor_Manager_Region__c, &quot;United Kingdom&quot;) ,  ISPICKVAL(Status, &quot;In Progress - With Vendor Manager&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test</fullName>
        <actions>
            <name>X1st_response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_Re_Opened__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.X1st_Reponse__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Re-Opened%22 Checkbox</fullName>
        <actions>
            <name>Check_Re_Opened_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Manual reopen of a Case - When a case is reopened the Re-Opened checkbox is checked- Used in milestone entry criteria</description>
        <formula>( ISPICKVAL( PRIORVALUE(Status) , &apos;Closed&apos;)) &amp;&amp; NOT(ISPICKVAL( Status , &apos;Closed&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Priority based on SLA</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Entitlement is 24 Hour - the Case Priority should be set to High</description>
        <formula>ISPICKVAL(Entitlement.Type, &quot;24 Hour&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Priority to High</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Damaged Goods,Affiliate Feedback,Misconduct,Technical Issue,Amendment/Cancellation,General Booking,Quote/Journey Time,Agent Error,Registration,Accident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Amendment,Multiple journeys</value>
        </criteriaItems>
        <description>When a case type is a certain value, it will update the case Priority to &quot;High.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Priority to Low</fullName>
        <actions>
            <name>Case_Priority_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Car has not been Allocated,Driver ETA,Track My Driver Error,Invoice Request,Receipt Request,Positive Feedback,Route,Deleted,Made Current,On Hold ,Closure</value>
        </criteriaItems>
        <description>When a case type is a certain value, it will update the case Priority to &quot;Low.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Priority to Medium</fullName>
        <actions>
            <name>Case_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>BACS Refund Request,Charges,Promotions,Pricing,Driver Standard,Driver Quality,Vehicle Quality,Opt Out ,ClubLee,AL Policies,Rates ,Other,Feedback,T&amp;Cs,Agent Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Lost Property,Update Payment Method</value>
        </criteriaItems>
        <description>When a case type is a certain value, it will update the case Priority to &quot;Medium&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Complaint Field</fullName>
        <actions>
            <name>Complaint_Tick_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Type__c</field>
            <operation>equals</operation>
            <value>Car has not been Allocated,Driver ETA,Track My Driver Error,Charges,Promotions,Pricing,Driver Standard,Driver Quality,Damaged Goods,Accident,Affiliate Feedback,Route,Vehicle Quality,Misconduct,Technical Issue,Feedback,Agent Error</value>
        </criteriaItems>
        <description>When a certain case type has been selected, Complaint gets updated to &quot;TRUE.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Escalated Checkbox when Case Status %3D Escalated</fullName>
        <actions>
            <name>Update_Escalated_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <description>Update Escalated Checkbox when Case Status is changed to &quot;Escalated&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Priority to %22High%22 for Incidents</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <description>Update Priority to &quot;High&quot; for Incidents</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Re-Opened Checkbox On Case Closure</fullName>
        <actions>
            <name>Update_Re_Opened_Checkbox_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Re-Opened Checkbox On Case Closure</description>
        <formula>NOT( ISPICKVAL( PRIORVALUE(Status ) , &apos;Closed&apos;) ) &amp;&amp; ISPICKVAL( Status  , &apos;Closed&apos;) &amp;&amp;  Case_Re_Opened__c  = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SLA</fullName>
        <actions>
            <name>Accident</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Accidents - Email</value>
        </criteriaItems>
        <description>Deactivated since Accident is an OLD case category. JD 18.4.16</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update SLA for AE Case Queue</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Addison Lee Account Executives</value>
        </criteriaItems>
        <criteriaItems>
            <field>Entitlement.BusinessHoursId</field>
            <operation>contains</operation>
            <value>72</value>
        </criteriaItems>
        <description>If unassigned to an account auto populate SLA 48 hrs</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Unilever Account Details</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Unilever - Email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Web User for Cases</fullName>
        <actions>
            <name>Update_Web_User_for_Cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Web User</value>
        </criteriaItems>
        <description>For Non Managed</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Web User for Cases to AE Queue</fullName>
        <actions>
            <name>Update_Web_User_for_Cases_AE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Web User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Managed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Managed Accounts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Vendor Manager Comments Made</fullName>
        <actions>
            <name>Vendor_Manager_Comments_Made</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email alert to Case Owner when Vendor Manager changes status from &quot;In Progress - With Vendor Manager&quot; back to &quot;Open&quot; or &quot;In Progress&quot;</description>
        <formula>AND  (  OR(  ISPICKVAL(Status,&quot;Open&quot;),  ISPICKVAL(Status,&quot;In Progress&quot;)),   Case_Status_PriorValue__c=&quot;In Progress - With Vendor Manager&quot;   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Website Cases Update Case Priority to High</fullName>
        <actions>
            <name>Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Website - Addison Lee</value>
        </criteriaItems>
        <description>When a case is created and the case origin is &quot;website - Addison Lee,&quot; the case priority will get updated to high. Requested by IV.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Near_Violation_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Case Near Violation Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Case_Very_Near_Violation_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Case Very Near Violation Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Thank_you_email_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Thank you email sent</subject>
    </tasks>
</Workflow>
