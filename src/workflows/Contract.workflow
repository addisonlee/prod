<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Renewal_Reminder_Template_AddisonLee</fullName>
        <description>Contract Renewal Reminder Template Addison Lee</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account_Managers_Team_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Services_Director</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Services_Manager_AL_LON</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Contract_Renewal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Contract_Renewal_Reminder_Template_WestOne</fullName>
        <description>Contract Renewal Reminder Template WestOne</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Contract_Renewal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Generic_Contract_Expiring_Soon_Reminder</fullName>
        <description>Generic: Contract Expiring Soon  - Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>donna.bloomfield@addisonlee.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nigel.davison@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Templates/Generic_Contract_Expiration_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Contract_Status</fullName>
        <description>Update Contract Status to Approved</description>
        <field>Status</field>
        <literalValue>Activated</literalValue>
        <name>Update Contract Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Status_to_Draft</fullName>
        <description>Update Contract Status to Draft</description>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Update Contract Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Status_to_Expired</fullName>
        <field>Status</field>
        <literalValue>Expired</literalValue>
        <name>Update Contract Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Status_to_In_Process</fullName>
        <description>Update Contract Status to In Approval Process</description>
        <field>Status</field>
        <literalValue>In Approval Process</literalValue>
        <name>Update Contract Status to In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contract Nearing Expiry</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Days_to_Contract_Expiry__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_Contract_Expiring_Soon_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_Contract_Expiring_Soon_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_Contract_Expiring_Soon_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_Contract_Expiring_Soon_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_Contract_Expiring_Soon_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Contract Status to Expired</fullName>
        <actions>
            <name>Update_Contract_Status_to_Expired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates any Activated Contract status to Expired when End Date = TODAY</description>
        <formula>AND(
ISPICKVAL(Status,&quot;Activated&quot;),
EndDate=TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WestOne Contract Renewal Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.EndDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Reminder 180 days before the contract end date, that the contract is ending in 180 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Renewal_Reminder_Template_WestOne</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Contract_Approved</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your contract has been approved.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Contract Approved</subject>
    </tasks>
    <tasks>
        <fullName>Contract_Rejected</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your contract has been rejected. Please review, and resubmit when appropriate changes have been made.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Rejected</subject>
    </tasks>
</Workflow>
