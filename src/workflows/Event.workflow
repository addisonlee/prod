<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Event_Created</fullName>
        <description>New Event Created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/External_Marketing_Event</template>
    </alerts>
    <rules>
        <fullName>New Event Created</fullName>
        <actions>
            <name>New_Event_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a new event is created by the external marketing team, emails the person the event is assigned to.</description>
        <formula>CreatedBy.ProfileId = &quot;00eb0000000dZSO&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
