<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Task_Creator_When_Task_Completed</fullName>
        <description>Email to Task Creator When Task Completed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Email_to_Case_Owner_when_Task_is_Completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>No_Action_Taken</fullName>
        <description>To mark a task that has been moved to Completed by this workflow, as no action taken in the description.</description>
        <field>Description</field>
        <formula>&quot;Marked as Completed by Workflow, as not closed by the assignee with 14 days of the due date.&quot;</formula>
        <name>No Action Taken</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_Spend_Update_Reason_for_Call</fullName>
        <field>Reason_for_Call__c</field>
        <literalValue>No Spend</literalValue>
        <name>No Spend Update Reason for Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Completed</fullName>
        <description>Change the status to Completed</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Not Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>1st Spend Follow-up</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Spending - 1st Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>The second in a sequence of timed tasks to follow up with an account after their first booking. DEACTIVATED 3/11/15 with permission from Ari. Duplicate.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>First_Spend_Follow_up</name>
                <type>Task</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>1st Spend Grow Services</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>First Spend Follow-up</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>The third in a sequence of follow-up task from an Account&apos;s first spend. DEACTIVATED 3/11/15 with permission from Ari. Duplicate.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>First_Spend_Sell_in_Services</name>
                <type>Task</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Close Aged No Spend Tasks</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>No Spend Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <description>Close the previous No Spend Task if it is uncompleted.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>No_Action_Taken</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Not_Completed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email to Case Owner when Task is Completed</fullName>
        <actions>
            <name>Email_to_Task_Creator_When_Task_Completed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Emails the case owner when a task is completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Spending - 2nd Call</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Spending - 1st Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>The second in a sequence of timed tasks to follow up with an account after their first booking.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Spending_second_call</name>
                <type>Task</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Spending - 3rd Call</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Spending - 2nd Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>The third in a sequence of follow-up task from an Account&apos;s first spend.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Spending_Third_Call</name>
                <type>Task</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>First_Spend_Follow_up</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is now 3 weeks since you made contact with this account.  Call the Account having first reviewed their bookings activities (Booking Summaries on Account).  Start to build rapport.</description>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Task.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>First Spend Follow-up</subject>
    </tasks>
    <tasks>
        <fullName>First_Spend_Sell_in_Services</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is now 4 weeks since you last spoke to your Account contact.  Review the Account Booking Summaries, the contact&apos;s Activity History, Case, and Individual Email Results.

Make contact and discuss what additional services they might use.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Task.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>First Spend - Sell-in Services</subject>
    </tasks>
    <tasks>
        <fullName>Spending_Third_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is now 4 weeks since you made contact with this account.  Call the Account having first reviewed their bookings, Cases and Individual Email Results.  Follow up on additional services - CLubLee/Couriers/App.</description>
        <dueDateOffset>32</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Spending - 3rd Call</subject>
    </tasks>
    <tasks>
        <fullName>Spending_second_call</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is now 3 weeks since you last spoke to your Account contact.  Review the Account Bookings, the contact&apos;s Activity History, Cases, and Individual Email Results.

Make contact and discuss what additional services they might use - ClubLee/Couriers/App.</description>
        <dueDateOffset>28</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Spending - 2nd Call</subject>
    </tasks>
</Workflow>
