/**
* @author Nimil Christopher
* @description Opportunity trigger 
*/
trigger AddLee_Lead on Lead (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    /* Get singleton handler's instance */
    AddLee_TriggerLeadHandler handler = new AddLee_TriggerLeadHandler();

    /* Before Insert */
    if (Trigger.isInsert && Trigger.isBefore) {
        handler.onBeforeInsert(Trigger.new);
    }

    /* After Insert */
    else if (Trigger.isAfter && Trigger.isInsert) {
        handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }

    /* Before Update */
    else if (Trigger.isUpdate && Trigger.isBefore) {
        handler.onBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }

    /* After Update */
    else if (Trigger.isUpdate && Trigger.isAfter) {
        handler.onAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }

    /* Before Delete */
    else if (Trigger.isDelete && Trigger.isBefore) {
        handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
    }

    /* After Delete */
    else if (Trigger.isDelete && Trigger.isAfter) {
        handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }

    /* After Undelete */
    else if (Trigger.isUnDelete) {
        handler.onAfterUndelete(Trigger.new, Trigger.newMap);
    }
    
}