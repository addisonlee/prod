trigger AddLee_EmailMessage  on EmailMessage (after insert) {
/**
* @author Prateek Gandhi
*/ 

    /* Get singleton handler's instance */
    AddLee_TriggerEmailMessageHandler handler = new AddLee_TriggerEmailMessageHandler();

    /* After Insert */
    /**
    * @author Prateek Gandhi
    * @description Email Trigger after insert : associates account with the case on the bases of emailMessage ToAddress 
    */
    if (Trigger.isAfter && Trigger.isInsert) {
Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
        handler.onAfterInsert(Trigger.newMap);
    }


}