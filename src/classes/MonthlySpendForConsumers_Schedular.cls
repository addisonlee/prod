/*
Scheduler for calculate MonthlySpend on Consumers Records
*/
global class MonthlySpendForConsumers_Schedular implements Schedulable {
  global void execute (SchedulableContext sc) {
    CalculateMonthlyBookingAverageOnAccount cms = new CalculateMonthlyBookingAverageOnAccount();
     cms.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where  Consumer_Account_Number__c = \'1\' and Reason_for_opening_account__c != \'Business\' and Date_of_Last_Booking__c != null and Total_Amount_of_Bookings__c <= 49999';
     Database.executeBatch(cms,20);
    
  }
  

}