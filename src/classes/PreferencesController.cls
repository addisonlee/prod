public class PreferencesController{

    public PageReference saveMethod() {
        boolean checkudpate=false;
        Contact con=new Contact(Id= ApexPages.currentpage().getParameters().get('id'));
        con.Surveys_And_Research_Email__c =emailfield;
        con.Surveys_And_Research_Post__c =postfield;
        con.Surveys_And_Research_SMS__c =smsfield;
        update con;
        /*if(emailfield){
            checkudpate =true;
            
        }
        
        if(postfield)
        {
            checkudpate =true;
            
        }
        
        if(smsfield){
            checkudpate =true;
            
        }
        
        if(checkudpate)
        {
            
        }*/
        
        PageReference pr=new PageReference('/apex/Unsubscribe_NPS');
        pr.setRedirect(true);
        return pr;
    }


    public Boolean postfield { get; set; }

    public Boolean smsfield { get; set; }

    public Boolean emailfield { get; set; }
    
    public PreferencesController()
    {
    
        Contact cont=[Select id,Surveys_And_Research_Email__c,Surveys_And_Research_Post__c,Surveys_And_Research_SMS__c from Contact Where Id =: ApexPages.currentpage().getParameters().get('id')];
        emailfield=cont.Surveys_And_Research_Email__c;
        postfield=cont.Surveys_And_Research_Post__c;
        smsfield=cont.Surveys_And_Research_SMS__c;
    }
}