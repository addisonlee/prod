global class AddLee_RecoveryProcedure implements Schedulable {
    global void execute (SchedulableContext sc){
        AddLee_RecoveryProcedure_Batch recovery = new AddLee_RecoveryProcedure_Batch();
        database.executebatch(recovery,5);   
    }
    /*public void initiateRecovery(){
        List<Log__c> failedLogs = new List<Log__c>();
        Integer numberOfCallouts = 0;
        failedLogs = [SELECT Account__c, Type__c, Status__c FROM Log__c WHERE isFailed__c = 1];
        
        if(failedLogs.size()>1){
            for(Log__c eachLog : failedLogs){
                if(numberOfCallouts < AddLee_IntegrationUtility.getMaxCalloutScheduledTask()){
                    if(eachLog.Type__c.equalsIgnoreCase('Create')){
                        AddLee_IntegrationWrapper.createAccountCalloutSynchronous(eachLog.account__c, eachLog.Id);
                        numberOfCallouts++;
                    }else if(eachLog.Type__c.equalsIgnoreCase('Update')){
                        AddLee_IntegrationWrapper.updateAccountCalloutAsynchronous(eachLog.account__c, eachLog.Id);
                        numberOfCallouts++;
                    }   
                }
            }
        }
    }*/
}