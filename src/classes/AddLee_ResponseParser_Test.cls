@isTest
public with sharing class AddLee_ResponseParser_Test {
	@isTest
    static void AddLee_ResponseParser_createAccountSynch() {
    	Integer expectedValue = 1;
    	
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,expectedValue)[0];
        insert testAccount;
        //testAccounts[0].ParentId = testAccounts[1].Id;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testLog.Account__c = testAccount.Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_ResponseParser responseParser = new AddLee_ResponseParser();
        	responseParser.createCustomer(testlog,testAccount, 'test');
        Test.stopTest();
        
    }
    
    @isTest
    static void AddLee_ResponseParser_updateAccountSynch() {
    	Integer expectedValue = 1;
    	
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,expectedValue)[0];
        insert testAccount;
        testAccount.SHM_number_x__c = '112234';
        update testAccount;
        //testAccounts[0].ParentId = testAccounts[1].Id;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testLog.Account__c = testAccount.Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_ResponseParser responseParser = new AddLee_ResponseParser();
        	responseParser.amendCustomer(testlog,testAccount, 'test');
        Test.stopTest();
        
    }
}