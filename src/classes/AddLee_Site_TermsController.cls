public with sharing class AddLee_Site_TermsController {
	
	public static PageReference handleTandC(){
		Map<String, String> params = ApexPages.currentPage().getParameters();
		id tncId = params.get('Id');
		boolean accept = Boolean.valueOf(params.get('accept'));
		AddLee_Terms_Conditions__c tnc = new AddLee_Terms_Conditions__c();
		Lead leadToUpdate = new Lead();
		try{
			tnc = [Select id, Internal_Id__c from AddLee_Terms_Conditions__c 
											where id =:tncId];
			leadToUpdate = [Select id from Lead where id =:tnc.Internal_Id__c limit 1];
		} catch (Exception e) {
			return Page.FileNotFound;
		}
		if(tnc.id != null){
			system.debug('The TnC found is '+tnc);
			if(accept){
				tnc.Response__c = 'Accepted';
			} else {
				tnc.Response__c = 'Declined';
			}
			update tnc;
		} else {
			return Page.FileNotFound;
		}
		return null;
	}
}