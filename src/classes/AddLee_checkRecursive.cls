public with sharing class AddLee_checkRecursive {
    //Contacts
    public static boolean firstRunContactIsBefore = true;
    public static boolean firstRunContactIsAfter = true;
    public static boolean isAfterInsertUpdate = true;
    
    //Case
    public static boolean firstRunCaseIsBefore = true;
    public static boolean firstRunCaseIsAfter = true;
    //Accounts
    public static boolean firstRunAccountIsBefore = true;
    public static boolean firstRunAccountIsAfter = true;
}