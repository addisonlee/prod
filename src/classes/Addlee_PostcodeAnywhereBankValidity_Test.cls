@isTest
public class Addlee_PostcodeAnywhereBankValidity_Test{
   @isTest
   static void AddLee_PostcodeAnywhereBankValidity_validateTestBankAccount(){

   Addlee_PostcodeAnywhereBankValidity.WebServiceSoap request = new Addlee_PostcodeAnywhereBankValidity.WebServiceSoap();
    
      Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());  
      Addlee_PostcodeAnywhereBankValidity.ArrayOfResults results = request.validate('test','testAccountNumber', 'testSortCode');  
      System.assertEquals(results.Results[0].Bank,'TestBank');

   }
}