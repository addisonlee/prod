@isTest
private class AddLee_CaseEmailMessageTriggerTestClass {

    static testMethod void validateTriggers() {
    //Id caseRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Reactive Cases').getRecordTypeId();
    
     Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = true);
    insert disabletrigger;
    
    List<Case> casesToInsert = new List<Case>();

        casesToInsert.add(new Case(Status='New',Origin='WestOne'));//0 72 hr entitlement from generic account
        //casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
       //casesToInsert.add(new Case(Status='New', Origin='Twitter - Addison Lee', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//2 24 hr entitlement from generic account
       // casesToInsert.add(new Case(Status='New',Type='Incident', Origin='Origin', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//3 24 hr entitlement from generic account
        //casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//4 for email message with To Address mappings
        //casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
        //casesToInsert.add(new Case(AccountId=accountsList[5].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));//1 for email message with To Address mappings
        //casesToInsert.add(new Case(AccountId=childAccountsList[3].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        //casesToInsert.add(new Case(AccountId=childAccountsList[4].id,Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId ));
        insert casesToInsert;
        
        disabletrigger.Bypass_Trigger_And_Validation_Rule__c = false;
        update disabletrigger;
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, ToAddress='example0@testclass.com',Incoming = true));
        //emailMessages.add(new EmailMessage(ParentId=casesToInsert[4].Id, ToAddress='example1@testclass.com',Incoming = true));
        //emailMessages.add(new EmailMessage(ParentId=casesToInsert[3].Id, ToAddress='example0@testclass.com',Incoming = true));

        insert emailMessages;
        
        disabletrigger.Bypass_Trigger_And_Validation_Rule__c = false;
        
    }
    
    
}