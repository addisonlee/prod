@isTest
global class AddleeRefundsGetSalesLedgersMock implements WebserviceMock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.salesLedgerResponse respElement =  new Addlee_refunds.salesLedgerResponse();
          respElement.salesLedgers = new List<Addlee_refunds.salesLedger>();     
          Addlee_refunds.salesLedger salesledger = new Addlee_refunds.salesLedger();
             salesledger.code = '67656';
             salesledger.id = 65675;
             salesledger.name = 'test'; 
             respElement.salesLedgers.add(salesledger);    
       response.put('response_x', respElement); 
   }
}