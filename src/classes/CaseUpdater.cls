public class CaseUpdater {

  @future (callout=true)
  public static void getJobs(Id caseid,Long accountNumber,DateTime dateFrom,DateTime dateTo,String invoiceNumber,String jobNumber,Integer maxJobsResult) 
  {
    Long invoice=null;
    Long jobnumb=null;
    System.debug('From Date'+dateFrom);
    System.debug(dateTo);
    System.debug(dateTo.addHours(23).addMinutes(59));
    if(invoiceNumber != null){
        invoice =Long.valueOf(invoiceNumber);
    }
    
    if(jobNumber != null){
        jobnumb=Long.valueOf(jobNumber);
    }
    
    Case cas=new Case(Id =caseid);
    
    try{
        AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
        AddLee_SoapIntegration_Test_v2_Fix.jobsResponse jobResponse = soapClient.getJobs(accountNumber, dateFrom, dateTo.addHours(23).addMinutes(59), invoice, jobnumb,100, AddLee_IntegrationWrapper.loginCalloutSynch());
        
        if(jobResponse.errorcode == 0 && jobResponse.jobs != null){ 
            if(jobResponse.jobs.size() == 1){        
                cas.Job_Number__c = String.ValueOf(jobResponse.jobs[0].number_x);
            }else if(jobResponse.jobs.size() >1){
            String jobnumbers='';
            for(AddLee_SoapIntegration_Test_v2_Fix.Job eachJob : jobResponse.jobs){
                jobnumbers +=eachJob.number_x+',';
            }
            jobnumbers =jobnumbers.removeEnd(',');
            cas.Tracing_Job_Number__c= 'More records found with Job numbers ('+ jobnumbers+')';
            }
        }else {
            cas.Tracing_Job_Number__c ='No Results Found';
        }
           //cas.Tracing_Job_Number__c=String.valueOf(jobResponse.jobs.size());
    }catch(Exception e)
    {
        cas.Tracing_Job_Number__c = string.valueOf(e.getMessage());
    }
    update cas;
  }
}