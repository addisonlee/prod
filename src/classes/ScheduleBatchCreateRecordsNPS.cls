global class ScheduleBatchCreateRecordsNPS implements schedulable {
    global void execute (SchedulableContext SC){
        BatchCreateRecordsNPS1 bcrNps = new BatchCreateRecordsNPS1();
        Database.executebatch(bcrNps);
    }
}