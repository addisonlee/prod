@istest(seealldata = true)
public class Test_BatchRunReports {
    
    public static testmethod void BatchTest() {
        //Nps_Report__c Nps = new Nps_Report__c(Name = 'Report Data', No_of_records_Percentage__c = '10' ,Report_Name__c= 'NPS B2C Contact Centre FINAL');
       // insert Nps;
        Nps_Report__c nps= Nps_Report__c.getValues('Report Data');
        List <Report> reportList = [SELECT DeveloperName,FolderName,Format,Id,Name FROM Report where name =: nps.Report_Name__c];
        System.debug(reportList);
        for(Report rep:reportList)
        {
            System.debug(rep.format);
        }
        Database.BatchableContext BC;
        List<sobject> scope;
        BatchRunReports batch = new BatchRunReports(); 
        DataBase.executeBatch(batch);
        
        //batch.start(bc);
        //batch.execute(bc,reportList);
        //batch.finish(bc);
    }
    
    public static testMethod void batchcreateNpsTest()
    {
        Account acc = AddLee_Trigger_Test_Utils.createAccounts(1).get(0);
        insert acc;
        contact con = new contact(lastname = 'test contact',accountid = acc.id);
        insert con;
        
        NPS__c nps=new NPS__c();
        nps.Contact__c =con.Id;
        nps.External_Id__c='oVaOCXi4U5EOxG';  //getExternalId
        nps.Date_Sent__c=System.today();
        nps.Account__c =acc.Id;
        
        Date dtToday = System.today();
        Map<Id,List<Id>> mapContactbookingSummIds=new Map<Id,List<Id>>();
        Booking_Summary__c bookSumm=new Booking_Summary__c(Name='Book Sum1', Account__c = acc.ID, Total_Journey_Price__c = 100, Number_of_Bookings__c = 10, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday); 
        mapContactbookingSummIds.put(con.Id,new List<Id>{bookSumm.Id});
        
                        
        BatchCreateNPSRecords npstest=new BatchCreateNPSRecords(new List<NPS__c>{nps},mapContactbookingSummIds);
        DataBase.Executebatch(npstest,200);
        
    }
}