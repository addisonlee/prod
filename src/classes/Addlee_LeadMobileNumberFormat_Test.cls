/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
public class Addlee_LeadMobileNumberFormat_Test {
    static testMethod void LeadMobiletest() {
        AddLee_TestDataUtilClass addleetduc = new AddLee_TestDataUtilClass();
        addleetduc.Data_InsertLogIntegrationCustomSetting();
        addleetduc.Data_InsertMobileNumberCustomSetting();
        PrivateKey__c pk = new PrivateKey__c();
        pk.Name = 'key';
        pk.CurrentKey__c = 'addleesfdc14';
        insert pk;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Blob privateKey = Addlee_Crypto.getCryptoKey();
        list<Lead>ld = new list<Lead>();
        lead ld1 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '7898918989');
        lead ld2 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '07898989189');
        lead ld3 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '444447898198989');
        lead ld4 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '+4789819898');
        lead ld5 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '4407898198989');
        lead ld6 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '444407898198989');
        lead ld7 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '44447898198989');
        lead ld8 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '447898198989');
        lead ld9 = new lead(LastName ='test1',Sales_Ledger__c = 'Sales Ledger',Rating = 'Cold',Status = 'Open',Phone = 'Sales Ledger',Payment_Type__c = 'Direct Debit',MobilePhone = '+447898198989');
        ld.add(ld1);
        ld.add(ld2);
        ld.add(ld3);
        ld.add(ld4);
        ld.add(ld5);
        ld.add(ld6);
        ld.add(ld7);
        ld.add(ld8);
        ld.add(ld9);
        insert ld;
        Addlee_LeadPhone_Batch cn = new Addlee_LeadPhone_Batch();
        cn.query = 'select Id,firstname,lastname,Phone from Lead where Phone!=null and isconverted = false';
        Database.executebatch(cn);
        Addlee_LeadMobileNumberFormat_Batch lmn = new Addlee_LeadMobileNumberFormat_Batch();
        lmn.query = 'select Id,firstname,lastname,MobilePhone,et4ae5__Mobile_Country_Code__c,Mobile_Number_To_Review__c from Lead where MobilePhone!=null and IsConverted = False';
        Database.executebatch(lmn);
        
        
        
    }
}