@isTest
private class AppFeedbackControllerTest {
	
    @testSetup 
    static void setupMethod() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = false);
     	insert disabletrigger;
        NoValidations__c noValidation = new NoValidations__c(SetupOwnerId = UserInfo.getUserID(), Active_Users__c = true);
     	insert noValidation;
	}
    
    static testMethod void NegativeCaseWithNoComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        string htmlBody = 'Job: #368180 2016-04-13 16:30';
		htmlBody += 'Name: Seona Bell';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07856025631';
        htmlBody += 'Pick Up: Andrew Duffus Ltd, Unit 4/A, 2-4 Orsman Road, London, N1 5QJ';
        htmlBody += 'Drop Off: Bell Staff Clothing, 11-15 Emerald Street, London, WC1N 3QL';
        htmlBody += 'Driver Rating: &#9733;&#9733; ';
        htmlBody += 'Overall Rating: &#9733;&#9733; ';
        htmlBody += 'Comments:  </strong> <br/>\n';
        htmlBody += '                     <br/>';
        
        List<Contact> con = AddLee_Trigger_Test_Utils.createContacts(1);
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', description=htmlBody , Subject='Test Subject',RecordTypeId=caseRecordTypeId,SuppliedEmail='test@test.com', ContactId=con[0].Id ));
        insert casesToInsert;
        
        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Positive feedback received, job 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        Case caseToVerify = [Select Id, External_Id__c, OwnerId From Case];
        system.debug('MTDebug caseToVerify.External_Id__c : ' + caseToVerify.External_Id__c);
        system.debug('MTDebug caseToVerify.Owner : ' + caseToVerify.OwnerId);
        string externalid = caseToVerify.External_Id__c;
        
        pagereference pageRef;
        
        test.startTest();
            ApexPages.currentpage().getParameters().put('msgId', externalid);
            AppFeedbackController afc = new AppFeedbackController();
            Map<String, String> params = ApexPages.currentPage().getParameters();
            system.debug('MTDebug params : ' + params);
            System.debug('Current User: ' + UserInfo.getUserName());
            system.debug('MTDebug afc.extId : ' + afc.extId);
            pageRef = afc.Redirect();
            
            system.assertEquals(null, pageRef);
            afc.comment = 'This is comment';
            pageRef = afc.Submit();
        test.stopTest();
        
        List<CaseComment> cc = [Select Id, CommentBody From CaseComment Where ParentId =: caseToVerify.Id];
        caseToVerify = [Select Id, External_Id__c From Case];
        system.assertEquals(1, cc.size());
        system.assertEquals('This is comment', cc[0].CommentBody);
        system.assertEquals(null, caseToVerify.External_Id__c);
        system.assert(String.ValueOf(pageRef).contains('/apex/Thankyou_NPS'));
    }
}