@isTest
global class AddleeRefundsGetJobDetailsMock implements webservicemock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.jobDetailsResponse respElement =  new Addlee_refunds.jobDetailsResponse();
               Addlee_refunds.jobEx testjobex = new Addlee_refunds.jobEx();
               Addlee_refunds.price testPrice= new Addlee_refunds.price();
                 testPrice.extras = 65765;
                 testPrice.journeyPrice =6565;
                 testPrice.waitingTime =65;
               testjobex.clientPrice = testPrice;
               
               Addlee_refunds.jobIndividual testjobindiv = new Addlee_refunds.jobIndividual();
                 testjobindiv.email = 'test@test,com';
                 testjobindiv.name= 'test';
                 testjobindiv.telephone = '57545547';
               testjobex.contact = testjobindiv;
               
               testjobex.delay = 10;
               testjobex.driverPrice = testjobex.clientPrice;
               
               Addlee_refunds.driverInfo testdriver = new Addlee_refunds.driverInfo();
                 testdriver.callsign = 'test';
                testdriver.name= 'testname';
                testdriver.address = 'testaddress';
                testdriver.number_x = 5465456;
                testdriver.telephone = '6567675';
                Addlee_refunds.vehicle testVeh= new Addlee_refunds.vehicle ();
                testVeh.colour = 'blue';
                testVeh.name = 'bmw';
                testVeh.radioNumber = '3';
                testVeh.regNumber = '5';
                testdriver.vehicle = testVeh;
               testjobex.driverInfo = testdriver;
               
                testjobex.jobActors = new List<Addlee_refunds.jobIndividual>();
                testjobex.jobActors.add(testjobex.contact);
               respElement.job = testjobex;
               
       response.put('response_x', respElement); 
   }
    
}