public class Addlee_Accountoptout{

public static void personaccountoptouts(list<Account>accounts,map<Id,account>oldrecordsmap){
for(Account ac:accounts){
if(trigger.isinsert){
 //update all preference fields to be false and update email optout and sms optout to be true.
   if(ac.Optout_All__pc==true)
                 {
                      ac.Add_Lib_Lifestyle_Content_Email__pc=false;
                      ac.Add_Lib_Lifestyle_Content_Post__pc=false;
                      ac.Add_Lib_Lifestyle_Content_SMS__pc=false;
                      ac.News_Letters_And_Updates_Email__pc=false;
                      ac.News_Letters_And_Updates_Post__pc=false;
                      ac.News_Letters_And_Updates_SMS__pc=false;
                      ac.Offers_Discounts_Competitions_Email__pc= false;
                      ac.Offers_Discounts_Competitions_Post__pc= false;
                      ac.Offers_Discounts_Competitions_SMS__pc= false;
                      ac.Surveys_And_Research_Email__pc= false;
                      ac.Surveys_And_Research_Post__pc= false;
                      ac.Surveys_And_Research_SMS__pc= false;
                      ac.SMS_Opt_Out__c= true;
                      ac.PersonHasOptedOutOfEmail=true;
                 }
 
}
//update all email fields to be false
if(trigger.isupdate){
if(oldrecordsmap.get(ac.Id).PersonHasOptedOutOfEmail == false && ac.PersonHasOptedOutOfEmail == true)
  {
      ac.Add_Lib_Lifestyle_Content_Email__pc=false;
      ac.News_Letters_And_Updates_Email__pc=false;
      ac.Offers_Discounts_Competitions_Email__pc=false;
      ac.Surveys_And_Research_Email__pc = false;
  }
 //update all sms fields to be false
if(ac.SMS_Opt_Out__c == true && oldrecordsmap.get(ac.id).SMS_Opt_Out__c == false)
  {
      ac.Add_Lib_Lifestyle_Content_SMS__pc=false;
      ac.News_Letters_And_Updates_SMS__pc=false;
      ac.Offers_Discounts_Competitions_SMS__pc= false;
      ac.Surveys_And_Research_SMS__pc= false;
  }
  // If any one of email preference is true update optout all to be false n Email optout to be false 
if((ac.Add_Lib_Lifestyle_Content_Email__pc == true && oldrecordsmap.get(ac.id).Add_Lib_Lifestyle_Content_Email__pc == false)||
   (ac.News_Letters_And_Updates_Email__pc== true && oldrecordsmap.get(ac.id).News_Letters_And_Updates_Email__pc== false)||
   (ac.Offers_Discounts_Competitions_Email__pc== true && oldrecordsmap.get(ac.id).Offers_Discounts_Competitions_Email__pc== false)||
   (ac.Surveys_And_Research_Email__pc== true && oldrecordsmap.get(ac.id).Surveys_And_Research_Email__pc== false))
   {
   ac.PersonHasOptedOutOfEmail = false;
   ac.Optout_All__pc= false;
   }
   // If any one of SMS Preference fields is true update optout all to be false n SMSoptout to be false 
if((ac.Add_Lib_Lifestyle_Content_SMS__pc== true && oldrecordsmap.get(ac.id).Add_Lib_Lifestyle_Content_SMS__pc== false)||
   (ac.News_Letters_And_Updates_SMS__pc== true && oldrecordsmap.get(ac.id).News_Letters_And_Updates_SMS__pc== false)||
   (ac.Offers_Discounts_Competitions_SMS__pc== true && oldrecordsmap.get(ac.id).Offers_Discounts_Competitions_SMS__pc== false)||
   (ac.Surveys_And_Research_SMS__pc== true && oldrecordsmap.get(ac.id).Surveys_And_Research_SMS__pc== false)){
   ac.SMS_Opt_Out__c = false;
   ac.Optout_All__pc= false;
   }
    // when optout all is true means update all preference fields to be false and update email optout and sms optout to be true.
if(ac.Optout_All__pc == true && oldrecordsmap.get(ac.Id).Optout_All__pc == false){
   ac.Add_Lib_Lifestyle_Content_Email__pc = false;
   ac.News_Letters_And_Updates_Email__pc = false;
   ac.Offers_Discounts_Competitions_Email__pc=false;
   ac.Surveys_And_Research_Email__pc = false;
   ac.Offers_Discounts_Competitions_SMS__pc = false;
   ac.Surveys_And_Research_SMS__pc = false;
   ac.Offers_Discounts_Competitions_SMS__pc= false;
   ac.Surveys_And_Research_SMS__pc= false;
   ac.SMS_Opt_Out__c = true;
   ac.PersonHasOptedOutOfEmail = true;
   }
}
}
}
}