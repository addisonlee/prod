global class ScheduleBatchCreateNPS implements schedulable {
    global void execute (SchedulableContext SC){
        BatchRunReports Nps = new BatchRunReports();
        Database.executebatch(Nps);
    }
}