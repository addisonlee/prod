/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddLee_RecoveryProcedure_Batch_Test {
	@isTest
    static void recoveryTestUpdate() {
   		
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
   		Account account =  AddLee_Trigger_Test_Utils.createAccounts(1)[0];
   			insert account;
   			System.Debug('From Test : ' + account);
    	Log__c log = new Log__c(Account__c = account.Id, Type__c = 'Update', Status__c = 'Failed' );
    		insert log;
    		System.DEBUG('FROM Test : ' + log);
    	Test.startTest();
    		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
    	
    		AddLee_RecoveryProcedure_Batch batch =  new AddLee_RecoveryProcedure_Batch();
    		batch.isApexTest = true;
    		Id batchId = database.executeBatch(batch,1);
    	Test.stopTest();
    
    }
    
    @isTest
    static void recoveryTestInsert() {
   		
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
   		Account account =  AddLee_Trigger_Test_Utils.createAccounts(1)[0];
   			insert account;
   			System.Debug('From Test : ' + account);
    	Log__c log = new Log__c(Account__c = account.Id, Type__c = 'Create', Status__c = 'Failed' );
    		insert log;
    		System.DEBUG('FROM Test : ' + log);
    	Test.startTest();
    		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
    	
    		AddLee_RecoveryProcedure_Batch batch =  new AddLee_RecoveryProcedure_Batch();
    		batch.isApexTest = true;
    		Id batchId = database.executeBatch(batch,1);
    	Test.stopTest();
    
    }
}