@isTest
public class AddLee_AccountOptout_Test
{
    public static testmethod void testm()
    {
         Log_Integration__c l = new Log_Integration__c();
         l.name='shamrock_setting';
         l.Enable_Bulk_Processing__c=false;
         insert l;
         RecordType r = new RecordType();
         r=[select Id,SobjectType,Name from RecordType where Name='Personal Account' and sobjectType='Account' limit 1];   
         Account acc = new Account();
         // acc.Name='CSC';
         acc.LastName='Test';
         acc.Optout_All__pc=true;
         insert acc;
         acc.PersonHasOptedOutOfEmail=false;
         update acc;
         acc.PersonHasOptedOutOfEmail=true;
         update acc;
         acc.Optout_All__pc = false;
         update acc;
         acc.Optout_All__pc = true;
         update acc;
         acc.SMS_Opt_Out__c =false; 
         update acc;
         acc.SMS_Opt_Out__c =true;
         update acc;
         acc.Add_Lib_Lifestyle_Content_Email__pc = false;
         update acc;
         acc.Add_Lib_Lifestyle_Content_Email__pc = true;
         update acc;
         acc.Add_Lib_Lifestyle_Content_SMS__pc = false;
         update acc;
         acc.Add_Lib_Lifestyle_Content_SMS__pc= true;
         update acc;
         acc.Optout_All__pc = false;
         update acc;
         acc.Optout_All__pc = true;
         update acc;   
    }
}