public without sharing class AddLee_AccountSynchHelper {

    /* Function to get all the Account in API context */
    public Map<Id, Account> getIsAPIMap(Map<Id, Account> newAccountMap){
        
        // Map with all the Account related to API context 
        Map<Id, Account> isAPIAccountMap    = new Map<Id, Account>();
        if(newAccountMap != null){
            // Split the account isAPI and not isAPI
            for(Id accId : newAccountMap.keySet()){
                if(newAccountMap.get(accId).isAPI__c == true)
                    isAPIAccountMap.put(newAccountMap.get(accId).Id, newAccountMap.get(accId));
            }   
        }
        
        return isAPIAccountMap;
    }
    
    /* Function to get all the Account not in API context */
    public Map<Id, Account> getNotIsAPIMap(Map<Id, Account> newAccountMap){
        
        // Map with all the Account related to API context 
        Map<Id, Account> notIsAPIAccountMap = new Map<Id, Account>();
        if(newAccountMap != null){
            // Split the account isAPI and not isAPI 
            for(Id accId : newAccountMap.keySet()){
                if(newAccountMap.get(accId).isAPI__c == false)
                    notIsAPIAccountMap.put(newAccountMap.get(accId).Id, newAccountMap.get(accId));
            }   
        }
        
        return notIsAPIAccountMap;
    }
    
    
    /* Function to get all the Account to be post Processed */
    public Map<Id, Account> getPostProcessMap(Map<Id, Account> accountMap){
        
        // Map with all the Account related to API context 
        Map<Id, Account> postProcessAccountMap = new Map<Id, Account>();
        if(accountMap != null){
            // Split the account isAPI and not isAPI 
            for(Id accId : accountMap.keySet()){
                if(accountMap.get(accId).goPostProcessing__c == true)
                    postProcessAccountMap.put(accountMap.get(accId).Id, accountMap.get(accId));
            }   
        }
        
        
        
        return postProcessAccountMap;
    }   
    
    /* Function to get all the Account in API context */
    public List<Account> getIsAPIList(List<Account> newAccountList){
        
        // Map with all the Account related to API context 
        List<Account> isAPIAccountList  = new List<Account>();
        if(newAccountList != null){
            // Split the account isAPI and not isAPI
            for(Account eachAccount : newAccountList){
                if(eachAccount.isAPI__c == true)
                    isAPIAccountList.add(eachAccount);
            }   
        }
        
        return isAPIAccountList;
    }
    
    /* Function to get all the Account not in API context */
    public List<Account> getNotIsAPIList(List<Account> newAccountList){
        
        // Map with all the Account related to API context 
        List<Account> notIsAPIAccountList = new List<Account>();
        if(newAccountList != null){
            // Split the account isAPI and not isAPI 
            for(Account eachAccount : newAccountList){
                if(eachAccount.isAPI__c == false)
                    notIsAPIAccountList.add(eachAccount);
            }   
        }
        
        return notIsAPIAccountList;
    }
    
    /* Function to get all the Account to be post Processed */
    public List<Account> getPostProcessList(List<Account> newAccountList){
        
        // Map with all the Account related to API context 
        List<Account> postProcessAccountList = new List<Account>();
        if(newAccountList != null){
            // Split the account isAPI and not isAPI 
            for(Account eachAccount : newAccountList){
                if(eachAccount.goPostProcessing__c == true && eachAccount.TECH_Integration_Ready__c && Trigger.isAfter && Trigger.isInsert){
                    postProcessAccountList.add(eachAccount);
                }
            }   
        }       
        
        return postProcessAccountList;
    }   
    
    
    public Map<Id,Account> populateParentAccountMap(List<Account> newAccounts, Map<Id,Account> parentAccountMap){
        Set<Id> parentIds = new Set<Id>();
        for(Account eachAccount : newAccounts){
            If(eachAccount.parentId != null){
                parentIds.add(eachAccount.parentId);
            }
        }
        parentAccountMap = new Map<Id,Account>([Select Id , Account_Status__c, exsistInShamrock__c FROM Account where Id in : parentIds]);
        return parentAccountMap;
    }

    
    public void checkParentAccountExistinShamrock(List<Account> newAccounts, Map<Id,Account> parentAccountMap){
        for(Account eachAccount : newAccounts){
            if(eachAccount.parentId != null){
                if(!parentAccountMap.get(eachAccount.parentId).exsistInShamrock__c){
                    eachAccount.parentId.addError('You cannot set an account to current, if parent account is not current or does not exist in shamrock');
                }
            }
        }
    }
    
    public void assignAccountNumber(List<Account> newAccounts,  Map<String, TECH_Auto_Number__c> currentAccountNumbersMap){
        Boolean updateMap = false;
        Decimal accountNumber = 0;
        
        for(Account eachAccount : newAccounts){
            if(eachAccount.Account_Status__c !=null && eachAccount.Account_Status__c.equalsIgnoreCase('Current')){
                if(!eachAccount.isAPI__c){
                    if(eachAccount.Account_Number__c == null && !eachAccount.Override_Account_Number__c && !eachAccount.TECH_Account_Number_Assigned__c){
                        if(eachAccount.Sales_Ledger__c !=null && eachAccount.Sales_Ledger__c.equalsIgnoreCase('WestOne Sales Ledger')){
                            currentAccountNumbersMap.get('WestOneRange').Current_Number__c += 1;
                            eachAccount.Account_Number__c = String.valueOf(Integer.valueOf(currentAccountNumbersMap.get('WestOneRange').Current_Number__c));
                            eachAccount.TECH_Account_Number_Assigned__c = true;
                            updateMap = true;
                        }else{    
                            currentAccountNumbersMap.get('AddLeeRange').Current_Number__c += 1;
                            eachAccount.Account_Number__c = String.valueOf(Integer.valueOf(currentAccountNumbersMap.get('AddLeeRange').Current_Number__c));
                            eachAccount.TECH_Account_Number_Assigned__c = true;
                            updateMap = true;
                       
                        }
                    }else if(eachAccount.Account_Number__c == null && eachAccount.Override_Account_Number__c){
                        eachAccount.Account_Number__c.addError('Account Number must be entered');
                    }else{
                        eachAccount.TECH_Account_Number_Assigned__c = true;
                    }

                }else if(eachAccount.isAPI__c && !eachAccount.TECH_Account_Number_Assigned__c){
                    eachAccount.TECH_Account_Number_Assigned__c = true;
                }
            }
        }
        try{
            if (updateMap)
                update currentAccountNumbersMap.values();
        }catch (Exception e){
            System.DEBUG(logginglevel.ERROR,'Update exception : ' + e);
        }
    }
    
    public void appendBuildingNum(List<Account> newAccountList){
        
        for(Account acc : newAccountList){
            if(acc.BuildingNumber__c != null && acc.BuildingNumber__c != ''){
                system.debug('MTDebug you are here' );
                system.debug('MTDebug acc.BillingStreet 1 ' + acc.BillingStreet);
                acc.PersonMailingStreet = acc.BuildingNumber__c + ' ' + acc.PersonMailingStreet;
                system.debug('MTDebug acc.BuildingNumber__c 2 ' + acc.BuildingNumber__c);
                system.debug('MTDebug acc.BillingStreet 2 ' + acc.BillingStreet);
            }
        }
    }
    /* Handle the preprocessing isAPI/isNotAPI Accounts */
    public void preProcess(List<Account> newAccountList, Boolean isAPI, Boolean isInsert, Boolean isUpdate){
        
            
        //Get the full list of rules and Record types available for the account
        List<Log_Integration_Preprocessing__c> ruleList = Log_Integration_Preprocessing__c.getall().values();
        
        //Apply action on all the accounts
        for(Account acc : newAccountList){
            
            
            acc.goPostProcessing__c = false;
            
            if(!acc.isAPI__c)
                acc.goPostProcessing__c = true;
            
            acc.isAPI__c = false;
                
            //Apply actions in scope
            for(Log_Integration_Preprocessing__c lip : ruleList){
                
                
                if(lip.Object__c == 'Account' && lip.isAPI__c == isAPI && lip.RecordType__c == AddLee_ContextVariables.recordTypeMap.get(acc.RecordTypeId).getName()){
                    //Apply rule with no type checking (To be improved)
                    //System.DEBUG('toField : ' + lip.ToField__c + ' ' + 'fromField : ' + acc.get(lip.FromField__c));
                    //Maybe consider changing the field types?
                    if(lip.ToField__c.equalsIgnoreCase('ShippingLatitude') || lip.ToField__c.equalsIgnoreCase('ShippingLongitude') 
                        || lip.ToField__c.equalsIgnoreCase('BillingLatitude') || lip.ToField__c.equalsIgnoreCase('BillingLongitude')){
                        acc.put(lip.ToField__c, Double.valueOf(acc.get(lip.FromField__c)));
                    }else if(lip.ToField__c.equalsIgnoreCase('SHM_main_longitude__c') || lip.ToField__c.equalsIgnoreCase('SHM_main_latitude__c')
                        || lip.ToField__c.equalsIgnoreCase('SHM_billing_longitude__c') || lip.ToField__c.equalsIgnoreCase('SHM_billing_latitude__c')){
                        acc.put(lip.ToField__c, String.valueOf(acc.get(lip.FromField__c)));
                    }else if(lip.ToField__c.equalsIgnoreCase('LastName') && lip.isAPI__c){
                        acc.put('FirstName',getFirstName(String.valueOf(acc.get(lip.FromField__c))));
                        acc.put('LastName',getLastName(String.valueOf(acc.get(lip.FromField__c))));
                    }else if(lip.FromField__c.equalsIgnoreCase('LastName') && !lip.isAPI__c){
                        acc.put(lip.ToField__c, acc.get('FirstName') + ' ' + acc.get('LastName'));
                    }
                    else{
                        acc.put(lip.ToField__c, acc.get(lip.FromField__c));
                    }
                        
                }   
            }
        }
    }
    
    public void resetPostProcessed(List<Account> postProcessedList){
        for(Account eachAccount : postProcessedList){
            eachAccount.goPostProcessing__c = false;
        }
    }

    public void convertAccount(List<Account> accountList){
        System.DEBUG(logginglevel.ERROR,'Accounts : ' +  accountList);
        for(Account eachAccount : accountList){
            System.DEBUG(logginglevel.INFO, 'RecordType : ' + AddLee_ContextVariables.recordTypeMap.get(eachAccount.RecordTypeId).getName());
            if(eachAccount.Bank_Details_Captured__c && eachAccount.RAG_Status__c.equalsIgnoreCase('Green') 
                && eachAccount.Converted_From_Lead__c && eachAccount.Account_Status__c != null
                && !eachAccount.Account_Status__c.equalsIgnoreCase('Current')
                && AddLee_ContextVariables.recordTypeMap.get(eachAccount.RecordTypeId).getName().equalsIgnoreCase('Business Account')){
                eachAccount.integrationReady__c = true;
                //eachAccount.Account_Status__c = 'Prospect';
                eachAccount.Customer_Type__c = 'Invoice';
                //eachAccount.Payment_Type__c = 'Direct Debit';
                //eachAccount.Administration_Fee__c = 17.5;
                eachAccount.Invoicing_Frequency__c = 'Monthly';
                eachAccount.Payment_Terms__c = 30;
                eachAccount.Credit_Checked__c = true;
             //   eachAccount.Credit_Limit__c = convertToDecimal(eachAccount.Estimated_Monthly_Spend__c);
            }else if(eachAccount.Bank_Details_Captured__c && eachAccount.RAG_Status__c.equalsIgnoreCase('Green') 
                && eachAccount.Converted_From_Lead__c && eachAccount.Account_Status__c != null
                && !eachAccount.Account_Status__c.equalsIgnoreCase('Current')
                && eachAccount.IsPersonAccount){
                eachAccount.integrationReady__c = true;
                //eachAccount.Account_Status__c = 'Prospect';
                eachAccount.Customer_Type__c = 'Invoice';
                //eachAccount.Payment_Type__c = 'Direct Debit';
                //eachAccount.Administration_Fee__c = 17.5;
                eachAccount.Invoicing_Frequency__c = 'Fortnightly';
                eachAccount.Payment_Terms__c = 15;
                eachAccount.Credit_Checked__c = true;
                //eachAccount.Credit_Limit__c = 50; // default value
            }else if(eachAccount.Account_Status__c !=null && eachAccount.Account_Status__c.equalsIgnoreCase('Current')  && !eachAccount.Bank_Details_Captured__c){
                eachAccount.integrationReady__c = true;
            }
        }
        System.DEBUG(logginglevel.ERROR,'Accounts : after update' +  accountList);
    }

    public Decimal convertToDecimal(String estimatedMonthlySpend){
    
    if(estimatedMonthlySpend != null){
        
        estimatedMonthlySpend = estimatedMonthlySpend.replace('+','');
        estimatedMonthlySpend = estimatedMonthlySpend.replace('£','');
        return Decimal.valueOf(estimatedMonthlySpend);
            }
            return null;
    }
    /* All the actions to be done when an Account is created (Map Edition)*/
    public void handleInsert(List<Account> newAccountList){
        //Create the list of Logs to be associated to the object
        List<Log__c> logs = this.getFilteredLogs(newAccountList);
        //Bulk Upsert based on the external key
        try {
            upsert logs ExternalKey__c;
        } catch (DmlException e) {}
    }
    
    public void handleContacts(Map<Id,Account> newAccountMap,Map<Id,Account> oldAccountMap,Boolean isInsert){
        Id BusinessAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        List<Log_Contact__c> mcs = Log_Contact__c.getall().values();
        Set<Id> accountsWithContactsToUpdate = new Set<Id>();
        List <Contact> contactsToUpdate = new List<Contact>();
        Map<String,String> logMap = new Map<String,String>();
        for(Account eachAccount : newAccountMap.values()){
            if(eachAccount.RecordTypeId == BusinessAccountId){
                for(Log_Contact__c eachLog : mcs){
                    if(eachAccount.get(eachLog.Field__c) != null && !String.valueOf(eachAccount.get(eachLog.Field__c)).equals('') && !isInsert){
                        //Check if values have changed 
                        if(!String.valueof(eachAccount.get(eachlog.Field__c)).equalsIgnoreCase(String.ValueOf(oldAccountMap.get(eachAccount.Id).get(eachLog.Field__c))) 
                            && !AddLee_IntegrationUtility.getIsFromContact()){
                            accountsWithContactsToUpdate.add(eachAccount.Id);
                        }
                    }else if(eachAccount.get(eachLog.Field__c) != null && !String.valueOf(eachAccount.get(eachLog.Field__c)).equals('') && isInsert){
                        accountsWithContactsToUpdate.add(eachAccount.Id);
                    }
                }
            }
        }   
        
        
        for(Id eachAccount : accountsWithContactsToUpdate){
            if(newAccountMap.get(eachAccount).SHM_main_contactName__c != null && newAccountMap.get(eachAccount).SHM_main_contactName__c != ''){
                Contact mainContact = new Contact();
                mainContact.phone = newAccountMap.get(eachAccount).SHM_main_telephone__c;
                mainContact.MailingCity = newAccountMap.get(eachAccount).SHM_main_town__c;
                mainContact.MailingPostalCode = newAccountMap.get(eachAccount).SHM_main_postcode__c;
                mainContact.fax = newAccountMap.get(eachAccount).SHM_main_fax__c;
                mainContact.email = newAccountMap.get(eachAccount).SHM_main_email__c ;
                mainContact.lastname = getLastName(newAccountMap.get(eachAccount).SHM_main_contactName__c);
                mainContact.FirstName = getFirstName(newAccountMap.get(eachAccount).SHM_main_contactName__c);
                mainContact.isApi__c = true;
                mainContact.Contact_Type__c = 'Main Contact';
                mainContact.Tech_ExternalKey__c = mainContact.Contact_Type__c + eachAccount;
                mainContact.accountId = eachAccount;
                mainContact.MailingStreet = newAccountMap.get(eachAccount).SHM_main_address__c ;
                contactsToUpdate.add(mainContact);
            }
            if(newAccountMap.get(eachAccount).SHM_billing_contactName__c != null && newAccountMap.get(eachAccount).SHM_billing_contactName__c != ''){
                Contact billingContact = new Contact();
                billingContact.phone = newAccountMap.get(eachAccount).SHM_billing_telephone__c;
                billingContact.MailingCity = newAccountMap.get(eachAccount).SHM_billing_town__c;
                billingContact.MailingPostalCode = newAccountMap.get(eachAccount).SHM_billing_postcode__c;
                billingContact.fax = newAccountMap.get(eachAccount).SHM_billing_fax__c;
                billingContact.email = newAccountMap.get(eachAccount).SHM_billing_email__c ;
                billingContact.lastname = getLastName(newAccountMap.get(eachAccount).SHM_billing_contactName__c);
                billingContact.FirstName = getFirstName(newAccountMap.get(eachAccount).SHM_billing_contactName__c);
                billingContact.isApi__c = true;
                billingContact.Contact_Type__c = 'Billing Contact';
                billingContact.Tech_ExternalKey__c = billingContact.Contact_Type__c + eachAccount;
                billingContact.accountId = eachAccount;
                billingContact.MailingStreet = newAccountMap.get(eachAccount).SHM_billing_address__c;
                contactsToUpdate.add(billingContact);
            }
        }
        
        
        upsert contactsToUpdate Tech_ExternalKey__c;
    }
    
    public void removeCreditCardDetails(Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap){
        for(Account newAcc : newAccountMap.values()){
            if(newAcc.exsistInShamrock__c){
                newAcc.Card_Long_Number__c = '';
                newAcc.SHM_Card_Long_Number__c = '';
            }
        }
    }
    
    /* All the actions to be done when an Account is updated and some conditions are matched (Map version) */
    public void handleUpdate(Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap){
        //General data structure
        List<Log_Field_Mapping__c> fieldMappingList = Log_Field_Mapping__c.getall().values();
        List<String> fieldsMapping                  = new List<String>();
        List<Log__c> logs                           = new List<Log__c>();
        Log_Integration__c logIntegration           = Log_Integration__c.getValues('shamrock_setting');
        Id PersonAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Personal Account').getRecordTypeId();
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult DescribeResult = sObjectType.getDescribe();
        //Accounts in scope for the integration
        Map<String,Account> accounts = new Map<String,Account>();
        
        //Getting all the fields related to the Account
        for(Log_Field_Mapping__c lfm : fieldMappingList){
            if(lfm.Object__c == 'Account')
                fieldsMapping.add(lfm.Name);
        }
        
        //Filling the Account list in scope (Integration Ready with changes in the fields in scope)
        for(Account newAcc : newAccountMap.values()){
            if(newAcc.integrationReady__c){
                Account oldAcc = oldAccountMap.get(newAcc.Id);
                Boolean boolControl = false;
                for(String objectField : fieldsMapping){
                    Boolean isAPI = newAcc.get('goPostProcessing__c') != oldAcc.get('goPostProcessing__c') ? true:false;
                    String fieldType = getFieldType(objectField, DescribeResult);
                    if(objectField.equalsIgnoreCase('parent') && newAcc.get('RecordType') == PersonAccountRecTypeId){
                        system.DEBUG('Hit Continue');
                        continue;
                    }else if(String.valueOf(oldAcc.get(objectField)) != String.valueOf(newAcc.get(objectField)) && fieldType.equalsIgnoreCase('String')){
                        boolControl = true;
                        //checkErrors(newAcc, OldAcc, objectField, logIntegration, fieldType);
                    }else if(oldAcc.get(objectField) != newAcc.get(objectField) && fieldType.equalsIgnoreCase('Double')){
                        boolControl = true;
                        //checkErrors(newAcc, OldAcc, objectField, logIntegration, fieldType);
                    }
                }
                if(boolControl)
                    accounts.put(newAcc.Id, newAcc);
            }
        }
        //Remove all the Accounts where there is already a Pending Create Action associated to it
        List<Log__c> pendingLogs = [SELECT Account__c, Type__c 
                                    FROM Log__c 
                                    WHERE   isWaiting__c = 1 AND 
                                            Account__c IN :accounts.keySet()];
        for(Log__c log : pendingLogs){
            accounts.remove(log.Account__c);
        }
        //Create the list of Logs to be associated to the object
        logs = getFilteredLogs(accounts.values());
        
        //Bulk Upsert based on the external key
        try {
            upsert logs ExternalKey__c;
        } catch (DmlException e) {
            System.debug('<<<<EXCEPTION>>>>:'+e.getMessage());
        }
    }
    /* Utility function to generate the list of Logs to be created/updated */
    
    private void checkErrors(Account newAcc, Account oldAcc, String objectField, Log_Integration__c logIntegration, String fieldType){
        
        Boolean isAPI =  ! (newAcc.goPostProcessing__c );
        if(!logIntegration.enableUpdates__c && oldAcc.Account_Status__c.equalsIgnoreCase('Current') && !newAcc.Account_Status__c.equalsIgnoreCase('Current') && !isAPI){
            newAcc.addError('Error:Cannot Update this Record. Please contact your Administrator');
        }
        else if(!logIntegration.enableUpdates__c && !oldAcc.Account_Status__c.equalsIgnoreCase('Current') && newAcc.Account_Status__c.equalsIgnoreCase('Current') && !isAPI){
            
        }else if(!logIntegration.enableUpdates__c && newAcc.Account_Status__c.equalsIgnoreCase('Current') && oldAcc.get(objectField) != newAcc.get(objectField) && !isAPI){
            newAcc.addError('Error:Cannot Update this Record. Please contact your Administrator');
        }else if(!logIntegration.enableUpdates__c && newAcc.Account_Status__c.equalsIgnoreCase('Current') && objectField.equalsIgnoreCase('ParentId') && !isAPI){
            newAcc.addError('Error:Cannot Update this Record. Please contact your Administrator');
        }
    }
    
    public static String getFieldType(String fieldName, Schema.DescribeSObjectResult DescribeResult){
    //Return String if safe to compare as string, else double
        
        Schema.DescribeFieldResult f = DescribeResult.fields.getMap().get(fieldName).getDescribe();
        if (f.getType() == Schema.DisplayType.String){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.PICKLIST){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.PICKLIST){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.DOUBLE){
            return 'Double';
        } else if(f.getType() == Schema.DisplayType.BOOLEAN){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.PERCENT){
            return 'Double';
        } else if(f.getType() == Schema.DisplayType.EMAIL){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.TEXTAREA){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.DATE){
            return 'String';
        } else if(f.getType() == Schema.DisplayType.REFERENCE){
            return 'String';
        } 
 
        return null;
    }
    
    
    private List<Log__c> getFilteredLogs(List<Account> accounts){
        
        //General data structure
        List<Log__c> logs = new List<Log__c>();
        String action = '';
        //Filtering the accounts and check the action to do
        for(Account acc: accounts){
            
            //Checking the action to be done
            if(acc.exsistInShamrock__c)
                action = 'Update';
            else
                action = 'Create';
            
            //Log creation if the record is integration ready
            if((acc.integrationReady__c || acc.TECH_Integration_Ready__c) && acc.Account_Status__c.equalsIgnoreCase('Current')){
                Log__c log = new Log__c();
                log.Account__c = acc.Id;
                log.Type__c = action;
                log.Status__c = 'Waiting';
                log.ExternalKey__c = action+':'+acc.Id;
                log.Description__c = 'Action in Queue.';
                logs.add(log);
            }
        }
        return logs;
    }
    
    // The reason for this weird logic is because shamrock does not have a firstname,lastname field and sfdc contacts expects it this way
    private String getLastName(String name){
        String[] splitString = name.split(' ');
        Integer lastValue;
        String FirstName = '';
        String LastName = ''; 
        for(integer index = 0 ; index <=splitString.size() - 2; index++){
            FirstName += splitString[index] + ' ';
            lastValue = index;
        }
        //Last value is always surname
        LastName = splitString[lastValue + 1];
        System.DEBUG(LastName);
        return LastName;
    }
    private String getFirstName(String name){
        String[] splitString = name.split(' ');
        Integer lastValue;
        String FirstName = '';
        String LastName = ''; 
        for(integer index = 0 ; index <=splitString.size() - 2; index++){
            FirstName += splitString[index] + ' ';
            lastValue = index;
        }
        //Last value is always surname
        LastName = splitString[lastValue + 1];
        System.DEBUG(LastName);
        return FirstName;
    }
    
}