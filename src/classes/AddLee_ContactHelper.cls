public with sharing class AddLee_ContactHelper {
    Map<Id,Contact> accountsMap = new Map<Id,Contact>();
    List<Contact> contactsToUpdate = new List<Contact>();
    Set<Id> contactsToIgnore = new Set<Id>();
    Map<Id,Account> accountsToUpdate = new Map<Id,Account>();
    Map<Id, String> accountStatusMap = new Map<Id, String>(); 
    Set<Id> accountIds = new Set<Id>();
    Boolean updatesAllowed = true;
    public void Addlee_ContactHelper(){
    
    }
    
    public void onBeforeInsert(List<Contact> newObjects, Map<Id, Contact> newObjectsMap){
        try{
            this.updatesAllowed = this.isUpdatesAllowed(newObjects);
           // this.populateAccountMap(newObjects);
            this.contactsToIgnore = null;
            this.processContacts();
            this.processRulesAfterInsert(newObjects);
            this.resetApi(newObjects); 
            this.setExternalIdField(newObjects);
        }catch(System.DmlException e){
            
        }
    }
    
    public void onBeforeUpdate( List<Contact> oldObjects,  Map<Id, Contact> oldObjectsMap,
                                List<Contact> newObjects,  Map<Id, Contact> newObjectsMap){
        try{
            this.updatesAllowed = this.isUpdatesAllowed(newObjects);
           // this.populateAccountMap(newObjects);
            this.contactsToIgnore = newObjectsMap.keyset();  
            this.processContacts();    
            this.processRulesAfterInsert(newObjects); 
            this.resetApi(newObjects);      
            this.setExternalIdField(newObjects);
        }catch(System.DmlException e){
            e.setMessage('Cannot Update When Account Status is Current');
            //throw new CannotUpdateException(e.getMessage());
        }   
    }
    
    private Boolean isUpdatesAllowed(List<Contact> newObjects){
        Log_Integration__c logIntegration = Log_Integration__c.getValues('shamrock_setting');
        Set<Id> accountIds = new Set<Id>();
        for(Contact eachContact : newObjects){
            accountIds.add(eachContact.accountId);
        }
        
        List<Account> accountStatusList = [SELECT Id, Account_Status__c FROM Account where Id in : accountIds];
        
        for(Account eachAccount : accountStatusList){
            this.accountStatusMap.put(eachAccount.Id, eachAccount.Account_Status__c);
        }
        
        if(logIntegration.enableUpdates__c){
            return true;
        }else{
            return false;
        }
    }
    
    private void populateAccountMap(List<Contact> newObjects){
        for(Contact eachContact : newObjects){
            if(this.accountStatusMap != null){
                if(!eachContact.isApi__c && 
                    !this.updatesAllowed && 
                    (eachContact.Contact_Type__c.equals('Main Contact') || eachContact.Contact_Type__c.equals('Billing Contact'))
                    && this.accountStatusMap.get(eachContact.AccountId).equalsIgnoreCase('Current')){
                    eachContact.addError('Updates/Inserts Disabled for Main and Billing Contacts');
                }
            }
            this.accountsMap.put(eachContact.AccountId, eachContact);
        }    
    }
    
    private void processContacts(){
        for(Contact eachContact : [SELECT Id, Contact_Type__c,AccountId FROM Contact 
                                    WHERE (Contact_Type__c = 'Main Contact' OR Contact_Type__c = 'Billing Contact') 
                                    AND AccountId in :accountsMap.KeySet() AND Id not in :contactsToIgnore AND isApi__c = false]){
                                        System.DEBUG('Accounts Map : ' + accountsMap);
                                        System.DEBUG('Previous Contact : ' + this.accountsMap.get(eachContact.AccountId).Contact_Type__c);
                                        System.DEBUG('Current Contact : ' + eachContact.Contact_Type__c);
            if(this.accountsMap.containsKey(eachContact.AccountId) && this.accountsMap.get(eachContact.AccountId).Contact_Type__c != null
                    && this.accountsMap.get(eachContact.AccountId).Contact_Type__c.equalsIgnoreCase(eachContact.Contact_Type__c)){
                eachContact.Contact_Type__c = 'Other Contact';
                if(eachContact.Tech_ExternalKey__c == null){
                    eachContact.Tech_ExternalKey__c = eachContact.Contact_Type__c + + eachContact.accountId;
                }else{
                    eachContact.Tech_ExternalKey__c = eachContact.Contact_Type__c + + eachContact.accountId;
                }
                this.contactsToUpdate.add(eachContact);
            }
        }
        if(contactsToUpdate.size() > 0){
            update this.contactsToUpdate;
        }
    }
    
    private void processRulesAfterInsert(List<Contact> newContacts){
        this.getAccountsInContext(newContacts);
        this.queryAccounts();
        if(this.accountsToUpdate.values().size()>0)
            this.updateAccountsWithContactValues();
        
    }
    
    private void resetApi( List<Contact> newObjects){
        System.DEBUG(logginglevel.ERROR,'After Update Objects : ' +  newObjects);
        for(Contact eachContact : newObjects){
            System.debug(logginglevel.ERROR,'EachContact : ' + eachContact.isApi__c);
            if(eachContact.isApi__c){
                eachContact.isApi__c = false;
            }
        }
    }
    
    private void setExternalIdField(List<Contact> newObjects){
    
        for(Contact eachContact : newObjects){
                eachContact.Tech_ExternalKey__c = eachContact.Contact_Type__c + eachContact.accountId;
        }
        
    }
    
    private void getAccountsInContext(List<Contact> newContacts){
        this.contactsToUpdate = new List<Contact>();
        for(Contact eachContact : newContacts){
            System.DEBUG(eachContact.Contact_Type__c);
            if((eachContact.Contact_Type__c !=null && (eachContact.Contact_Type__c.equalsIgnoreCase('Main Contact') || eachContact.Contact_Type__c.equalsIgnoreCase('Billing Contact'))) && !eachContact.isApi__c){
                this.accountIds.add(eachContact.AccountId);
                this.contactsToUpdate.add(eachContact);
            }
        }
    }
    
    private void queryAccounts(){
        RecordType RecType = AddLee_IntegrationUtility.getRecordTypeByName('Business_Account');
        if(accountIds!=null)
            this.accountsToUpdate = new Map<Id,Account>([Select a.Id, a.SHM_main_town__c, a.SHM_main_telephone__c, a.SHM_main_postcode__c, 
                                                                a.SHM_main_longitude__c, a.SHM_main_latitude__c, a.SHM_main_fax__c, a.SHM_main_email__c, 
                                                                a.SHM_main_contactName__c, a.SHM_main_address__c, a.SHM_billing_town__c, 
                                                                a.SHM_billing_telephone__c, a.SHM_billing_postcode__c, a.SHM_billing_longitude__c, 
                                                                a.SHM_billing_latitude__c, a.SHM_billing_fax__c, a.SHM_billing_email__c, 
                                                                a.SHM_billing_contactName__c, a.SHM_billing_address__c 
                                                            From Account a WHERE Id in: this.accountIds and recordTypeId =: rectype.Id]);
    }
    
    private void updateAccountsWithContactValues(){
        for(Contact eachContact : this.contactsToUpdate){
            if(eachContact.Contact_Type__c.equalsIgnoreCase('Main Contact') && !eachContact.isApi__c){
            	if(eachContact.MailingCity!= null){
                accountsToUpdate.get(eachContact.accountId).SHM_main_town__c = eachContact.MailingCity;
            	}
                accountsToUpdate.get(eachContact.accountId).SHM_main_telephone__c = eachContact.phone;
                accountsToUpdate.get(eachContact.accountId).SHM_main_postcode__c = eachContact.MailingPostalCode;
                //accountsToUpdate.get(eachContact.accountId).SHM_main_longitude__c = ''
                //accountsToUpdate.get(eachContact.accountId).SHM_main_latitude__c = eachContact.
                accountsToUpdate.get(eachContact.accountId).SHM_main_fax__c = eachContact.fax;
                accountsToUpdate.get(eachContact.accountId).SHM_main_email__c = eachContact.email;
                if(eachContact.FirstName != null && eachContact.FirstName != ''){
                    accountsToUpdate.get(eachContact.accountId).SHM_main_contactName__c = eachContact.FirstName + ' ' + eachContact.lastname;
                }else{
                    accountsToUpdate.get(eachContact.accountId).SHM_main_contactName__c = eachContact.lastname;
                }
                accountsToUpdate.get(eachContact.accountId).SHM_main_address__c = eachContact.MailingStreet + ' ' +eachContact.MailingCity + ' '+ eachContact.MailingCountry;
            }else if(eachContact.Contact_Type__c.equalsIgnoreCase('Billing Contact') && !eachContact.isApi__c){
                accountsToUpdate.get(eachContact.accountId).SHM_billing_town__c = eachContact.MailingCity;
                accountsToUpdate.get(eachContact.accountId).SHM_billing_telephone__c = eachContact.phone;
                accountsToUpdate.get(eachContact.accountId).SHM_billing_postcode__c = eachContact.MailingPostalCode;
                //accountsToUpdate.get(eachContact.accountId).SHM_main_longitude__c = ''
                //accountsToUpdate.get(eachContact.accountId).SHM_main_latitude__c = eachContact.
                accountsToUpdate.get(eachContact.accountId).SHM_billing_fax__c = eachContact.fax;
                accountsToUpdate.get(eachContact.accountId).SHM_billing_email__c = eachContact.email;
                if(eachContact.FirstName != null && eachContact.FirstName != ''){
                    accountsToUpdate.get(eachContact.accountId).SHM_billing_contactName__c = eachContact.FirstName + ' ' +  eachContact.lastname;
                }else{
                    accountsToUpdate.get(eachContact.accountId).SHM_billing_contactName__c = eachContact.lastname;
                }
                accountsToUpdate.get(eachContact.accountId).SHM_billing_address__c = eachContact.MailingStreet + ' ' + eachContact.MailingCity + ' ' +eachContact.MailingCountry;
            }
        }
        update accountsToUpdate.values();
        
        /*Database.SaveResult[] lsr = Database.update(accountsToUpdate.values(), false);

        for(Database.SaveResult sr: lsr){ 
            if (!sr.isSuccess()) { 
                System.debug('SaveResult : ' + sr);
            }
        } */
        
    }
    //update campaign member status to 'Booked ' when customer books after receving campaigns.
        
        public void campaingnBookingStatus(List<Contact> obj){
            Set<Id> contactids  = new set<Id>();
			List<CampaignMember> cmember = new List<CampaignMember>();
			List<Reactivated_Campaign_Member__c> membersinsert = new List<Reactivated_Campaign_Member__c>();
			map<Id,String> mapstatus = new map<Id,String>();
    		for(Contact c : obj){
    			system.debug('status++'+c.Booked__c);
    			if((c.Booked__c == true)||(c.Account_Managed__c == true)){
    				contactids.add(c.id);
    			}
    			
    		}
    		for(CampaignMember cmm:[select Id,Status,ContactId,Campaign_name__c from CampaignMember where ContactId =:contactids]){
    			system.debug('test.booked'+ cmm);
    			if(cmm != null ){
    				Reactivated_Campaign_Member__c rcm =  new Reactivated_Campaign_Member__c ();
    				rcm.Contact__c = cmm.ContactId;
    				rcm.Status__c = 'Booked';
    				rcm.Campaign_Name__c = cmm.Campaign_name__c;
    				rcm.Reactivation_Date__c = Date.today();
       				membersinsert.add(rcm); 
    				//cmm.Status = 'Booked';
    				cmember.add(cmm);
    			}
    		}
    		 
    		insert membersinsert;
    		delete cmember;
    }
    
}