global class ContactsFlagCheck implements Database.Batchable<sObject>{

    String objName;

    global ContactsFlagCheck(String objName)
    {
        this.objName =objName; 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        return Database.getQueryLocator('SELECT Id,Name,Email,phone,MobilePhone,Grouping_No__c From '+objName+' order by Name');
    }
    
   /* global List<Sobject> start(Database.BatchableContext BC)
    {    
        List<Sobject> obj=new List<Sobject>();
        List<Sobject> lstContact=[SELECT Id,Name,Email,phone,MobilePhone,Grouping_No__c From Contact order by Name ];
        List<Sobject> lstLead=[SELECT Id,Name,Email,phone,MobilePhone,Grouping_No__c From Lead order by Name ];
        obj.addAll(lstContact);
        obj.addAll(lstLead);
        return obj;
    }*/
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
        
        Set<Contact> updatedupcontacts=new Set<Contact>();
        Set<Lead> updatedupLeads=new Set<Lead>();
        Map<String,List<Id>> mapNEDups=new Map<String,List<Id>>();
        Map<String,List<Id>> mapEDups=new Map<String,List<Id>>();
        Map<String,List<Id>> mapNPDups=new Map<String,List<Id>>();
        Map<String,List<Id>> mapNMDups=new Map<String,List<Id>>();
        Map<String,List<Id>> mapNDups=new Map<String,List<Id>>();
        Set<Id> setgroupIds=new Set<Id>();
        for(Sobject cont:scope)
        {
        
            if((Id)cont.get('Grouping_No__c') != null)
            {
                setgroupIds.add((Id)cont.get('Grouping_No__c'));
                continue;
            }
            
            if(!((String)cont.get('Name') =='None' || (String)cont.get('Name') == 'unknown' || (String)cont.get('Name') == 'unknown name' || (String)cont.get('Name') == 'null null' || (String)cont.get('Name') =='Default')){
                if(mapNDups.containsKey((String)cont.get('Name')))
                {
                    List<Id> lstContIds=new List<Id>();
                    lstContIds.add(cont.Id);
                    lstContIds.addAll(mapNDups.get((String)cont.get('Name')));
                    mapNDups.put((String)cont.get('Name'),lstContIds);
                }else{
                    mapNDups.put((String)cont.get('Name'),new List<Id>{cont.Id});
                }
            }
            
            if((String)cont.get('Phone') != null ){
                if(mapNPDups.containsKey((String)cont.get('Name')+'-'+(String)cont.get('Phone')))
                {
                    List<Id> lstContIds=new List<Id>();
                    lstContIds.add(cont.Id);
                    lstContIds.addAll(mapNPDups.get((String)cont.get('Name')+'-'+(String)cont.get('Phone')));
                    mapNPDups.put((String)cont.get('Name')+'-'+(String)cont.get('Phone'),lstContIds);
                }else{
                    mapNPDups.put((String)cont.get('Name')+'-'+(String)cont.get('Phone'),new List<Id>{cont.Id});
                }
            }
            
            if((String)cont.get('MobilePhone') != null ){
                if(mapNMDups.containsKey((String)cont.get('Name')+'-'+(String)cont.get('MobilePhone')))
                {
                    List<Id> lstContIds=new List<Id>();
                    lstContIds.add(cont.Id);
                    lstContIds.addAll(mapNMDups.get((String)cont.get('Name')+'-'+(String)cont.get('MobilePhone')));
                    mapNMDups.put((String)cont.get('Name')+'-'+(String)cont.get('MobilePhone'),lstContIds);
                }else{
                    mapNMDups.put((String)cont.get('Name')+'-'+(String)cont.get('MobilePhone'),new List<Id>{cont.Id});
                }
            }
            
           
            if((String)cont.get('Email') != null)
            {
                if(mapNEDups.containsKey((String)cont.get('Name')+'-'+(String)cont.get('Email')))
                {
                    List<Id> lstContIds=new List<Id>();
                    lstContIds.add(cont.Id);
                    lstContIds.addAll(mapNEDups.get((String)cont.get('Name')+'-'+(String)cont.get('Email')));
                    mapNEDups.put((String)cont.get('Name')+'-'+(String)cont.get('Email'),lstContIds);
                }else{
                    mapNEDups.put((String)cont.get('Name')+'-'+(String)cont.get('Email'),new List<Id>{cont.Id});
                }
                String email;
                if(((String)cont.get('Email')).containsIgnoreCase('Info') || ((String)cont.get('Email')).containsIgnoreCase('Recep'))
                {
                    if(((String)cont.get('Email')).containsIgnoreCase('Info'))
                    email= ((String)cont.get('Email')).replace('Info','');
                    else
                    email =((String)cont.get('Email')).replace('Recep','');
                }else
                {
                    email = (String)cont.get('Email');
                }
                
                if(mapEDups.containsKey(email))
                {
                    List<Id> lstContIds=new List<Id>();
                    lstContIds.add(cont.Id);
                    lstContIds.addAll(mapEDups.get(email));
                    mapEDups.put(email,lstContIds);
                }else{
                    mapEDups.put(email,new List<Id>{cont.Id});
                }
            }
        }
        
        if(!setgroupIds.isEmpty()){
            List<Contact> lstcontact;
            List<Lead> lstLead;
            if(objName =='Contact'){
                lstcontact=[SELECT Id,Name,Email,phone,MobilePhone,Grouping__c,Grouping_No__c From Contact where Grouping_No__c IN : setgroupIds];
            }else{
                lstLead=[SELECT Id,Name,Email,phone,MobilePhone,Grouping__c,Grouping_No__c From Lead where Grouping_No__c IN : setgroupIds];
            }
            
            if(lstcontact != null)
            {
                for(Contact con:lstcontact)
                {
                    if(mapNEDups.containsKey(con.Name+con.Email)){
                        List<Id> lstIds=mapNEDups.get(con.Name+con.Email);
                        for(Id ids:lstIds){
                            Contact cont=new Contact(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupcontacts.add(cont);
                        }
                        mapNEDups.remove(con.Name+con.Email);
                    }
                    else if(mapEDups.containsKey(con.Email)){
                        List<Id> lstIds=mapEDups.get(con.Email);
                        for(Id ids:lstIds){
                            Contact cont=new Contact(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupcontacts.add(cont);
                        }
                        mapEDups.remove(con.Email);
                    }
                    else if(mapNPDups.containsKey(con.Name+con.phone)){
                        List<Id> lstIds=mapNPDups.get(con.Name+con.phone);
                        for(Id ids:lstIds){
                            Contact cont=new Contact(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupcontacts.add(cont);
                        }
                        mapNPDups.remove(con.Name+con.phone);
                    }
                    else if(mapNMDups.containsKey(con.Name +con.MobilePhone)){
                        List<Id> lstIds=mapNMDups.get(con.Name+con.MobilePhone);
                        for(Id ids:lstIds){
                            Contact cont=new Contact(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupcontacts.add(cont);
                        }
                        mapNMDups.remove(con.Name+con.MobilePhone);
                    }
                    else if(mapNDups.containsKey(con.Name)){
                        List<Id> lstIds=mapNDups.get(con.Name);
                        for(Id ids:lstIds){
                            Contact cont=new Contact(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupcontacts.add(cont);
                        }
                        mapNDups.remove(con.Name);
                    }
                }
                if(!updatedupcontacts.isEmpty()){
                    try{
                    List<Contact> lstcont=new List<Contact>();
                    lstcont.addAll(updatedupcontacts);
                    update lstcont;
                    }catch(Exception e)
                    {
                        System.debug(e.getMessage()+e.getStackTraceString());
                    }
                }
            }
            if(lstLead != null)
            {
                for(Lead con:lstLead)
                {
                    if(mapNEDups.containsKey(con.Name+con.Email)){
                        List<Id> lstIds=mapNEDups.get(con.Name+con.Email);
                        for(Id ids:lstIds){
                            Lead cont=new Lead(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupLeads.add(cont);
                        }
                        mapNEDups.remove(con.Name+con.Email);
                    }
                    else if(mapEDups.containsKey(con.Email)){
                        List<Id> lstIds=mapEDups.get(con.Email);
                        for(Id ids:lstIds){
                            Lead cont=new Lead(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupLeads.add(cont);
                        }
                        mapEDups.remove(con.Email);
                    }
                    else if(mapNPDups.containsKey(con.Name+con.phone)){
                        List<Id> lstIds=mapNPDups.get(con.Name+con.phone);
                        for(Id ids:lstIds){
                            Lead cont=new Lead(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupLeads.add(cont);
                        }
                        mapNPDups.remove(con.Name+con.phone);
                    }
                    else if(mapNMDups.containsKey(con.Name +con.MobilePhone)){
                        List<Id> lstIds=mapNMDups.get(con.Name+con.MobilePhone);
                        for(Id ids:lstIds){
                            Lead cont=new Lead(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupLeads.add(cont);
                        }
                        mapNMDups.remove(con.Name+con.MobilePhone);
                    }
                    else if(mapNDups.containsKey(con.Name)){
                        List<Id> lstIds=mapNDups.get(con.Name);
                        for(Id ids:lstIds){
                            Lead cont=new Lead(Id =ids,Grouping_No__c=con.Grouping_No__c,Flagging_Record__c =true);
                            updatedupLeads.add(cont);
                        }
                        mapNDups.remove(con.Name);
                    }
                }
                if(!updatedupLeads.isEmpty()){
                    try{
                    System.debug(updatedupLeads);
                    List<Lead> lstLeads=new List<Lead>();
                    lstLeads.addAll(updatedupLeads);
                    update lstLeads;
                    }catch(Exception e)
                    {
                        System.debug(e.getMessage()+e.getStackTraceString());
                    }
                }
            }
        }
        
        Set<Id> lstContactId =new Set<Id>();
        Map<Id,Sobject> lstMapcontact=new Map<Id,Sobject>();
        Map<Decimal,String> lstMapchkString=new Map<Decimal,String>();
        Integer countvalue;
        AggregateResult result=[SELECT MAX(Grouping__c) grp FROM Grouping__c];
        if(result != null){
            countvalue =Integer.valueOf(result.get('grp'));
        }
        if(countvalue ==null)
        {
            countvalue =0;
        }
        
        if(!mapNEDups.isEmpty())
        {
            for(String field:mapNEDups.keySet())
            {
                if(mapNEDups.get(field).size() >1){
                    for(Id contIds:mapNEDups.get(field)){
                        Sobject so;
                        if(((String)contIds).startsWith('003')){
                            so=new Contact();
                        }else{
                            so=new Lead();
                        }
                        so.put('Id',contIds);
                        so.put('Grouping__c',countvalue+1);
                        //Contact cont=new Contact(Id= contIds,Grouping__c =countvalue+1,Flagging_Record__c =true);
                        lstMapchkString.put(countvalue+1,field);
                        lstMapcontact.put(contIds,so);
                    }
                    countvalue++;
                    lstContactId.addAll(mapNEDups.get(field));
                }
            }
        }
        if(!mapEDups.isEmpty())
        {
            for(String field:mapEDups.keySet())
            {
                if(mapEDups.get(field).size() >1){
                    for(Id contIds:mapEDups.get(field)){
                        Sobject so;
                        if(((String)contIds).startsWith('003')){
                            so=new Contact();
                        }else{
                            so=new Lead();
                        }
                        so.put('Id',contIds);
                        so.put('Grouping__c',countvalue+1);
                        //Contact cont=new Contact(Id= contIds,Grouping__c =countvalue+1,Flagging_Record__c =true);
                        lstMapchkString.put(countvalue+1,field);
                        lstMapcontact.put(contIds,so);
                    }
                    countvalue++;
                    lstContactId.addAll(mapEDups.get(field));
                }
            }
        }
        if(!mapNPDups.isEmpty())
        {    
            for(String field:mapNPDups.keySet())
            {
                if(mapNPDups.get(field).size() >1){
                    for(Id contIds:mapNPDups.get(field)){
                        Sobject so;
                        if(((String)contIds).startsWith('003')){
                            so=new Contact();
                        }else{
                            so=new Lead();
                        }
                        so.put('Id',contIds);
                        so.put('Grouping__c',countvalue+1);
                        //Contact cont=new Contact(Id= contIds,Grouping__c =countvalue+1,Flagging_Record__c =true);
                        lstMapchkString.put(countvalue+1,field);
                        lstMapcontact.put(contIds,so);
                    }
                    countvalue++;
                    lstContactId.addAll(mapNPDups.get(field));
                }
            }
        }
        if(!mapNMDups.isEmpty())
        {
            for(String field:mapNMDups.keySet())
            {
                if(mapNMDups.get(field).size() >1){
                    for(Id contIds:mapNMDups.get(field)){
                        Sobject so;
                        if(((String)contIds).startsWith('003')){
                            so=new Contact();
                        }else{
                            so=new Lead();
                        }
                        so.put('Id',contIds);
                        so.put('Grouping__c',countvalue+1);
                        //Contact cont=new Contact(Id= contIds,Grouping__c =countvalue+1,Flagging_Record__c =true);
                        lstMapchkString.put(countvalue+1,field);
                        lstMapcontact.put(contIds,so);
                    }
                    countvalue++;
                    lstContactId.addAll(mapNMDups.get(field));
                }
            }
        }
        if(!mapNDups.isEmpty())
        {
            for(String field:mapNDups.keySet())
            {
                if(mapNDups.get(field).size() >1){
                    for(Id contIds:mapNDups.get(field)){
                        Sobject so;
                        if(((String)contIds).startsWith('003')){
                            so=new Contact();
                        }else{
                            so=new Lead();
                        }
                        so.put('Id',contIds);
                        so.put('Grouping__c',countvalue+1);
                        //Contact cont=new Contact(Id= contIds,Grouping__c =countvalue+1,Flagging_Record__c =true);
                        lstMapchkString.put(countvalue+1,field);
                        lstMapcontact.put(contIds,so);
                    }
                    countvalue++;
                    lstContactId.addAll(mapNDups.get(field));
                }
            }
        }
        
        if(!lstMapcontact.isEmpty())
        {
            List<Grouping__c> lstquerygrouping=[Select Id,Grouping__c,External_Field__c from Grouping__c Where External_Field__c IN : lstMapchkString.values()];
            Map<String,Id> mapextgroup=new Map<String,Id>();
            
            if(!lstquerygrouping.isEmpty())
            {
                for(Grouping__c grp:lstquerygrouping){
                    mapextgroup.put(grp.External_Field__c,grp.Id);
                }
            }
            
            Set<Grouping__c> lstgrouping=new Set<Grouping__c>();
            Map<Decimal,Id> mapgrouping=new Map<Decimal,Id>();
            
            for(Id cont:lstMapcontact.keySet())
            {
                Grouping__c grp=new Grouping__c(Grouping__c=(Decimal)(lstMapcontact.get(cont)).get('Grouping__c'));
                grp.External_Field__c = lstMapchkString.get((Decimal)lstMapcontact.get(cont).get('Grouping__c'));
                lstgrouping.add(grp);
            }
            
            /*for(Sobject cont:lstMapcontact.values())
            {
                Grouping__c grp=new Grouping__c(Grouping__c=(Decimal)cont.get('Grouping__c'),External_Field__c = );
                lstgrouping.add(grp);
            }*/
            
            if(!lstgrouping.isEmpty())
            {
                List<Grouping__c> lstgrp=new List<Grouping__c>();
                lstgrp.addAll(lstgrouping);
                
                upsert lstgrp External_Field__c;
                for(Grouping__c grp:lstgrp){
                    mapgrouping.put(grp.Grouping__c,grp.Id);
                }
            }
            
            List<Sobject> lstupdContact =new List<Sobject>();
            for(Sobject con:lstMapcontact.values())
            {
                Sobject ob;
                if(((String)con.get('Id')).startsWith('003'))
                {
                    ob=new Contact();
                }else
                {
                    ob=new Lead();
                }
                ob.put('Id',con.Id);
                ob.put('Grouping_No__c',mapgrouping.get((Decimal)con.get('Grouping__c')));
                ob.put('Flagging_Record__c',true);
                ob.put('Grouping__c',null);
                //Contact cont=new Contact(Id=con.Id,= mapgrouping.get((Decimal)con.get('Grouping__c')),Flagging_Record__c =true,Grouping__c=null);
                lstupdContact.add(ob);
            }
            if(!lstupdContact.isEmpty())
            {
                update lstupdContact;
                //update lstMapcontact.values();
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }
}