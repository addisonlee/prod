/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddLee_IntegrationWrapper_Test {
	@isTest
    static void AddLee_IntegrationWrapper_createAccountSessionExpireSynchronous() {
    	Integer expectedValue = 2;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(false,expectedValue);
        testAccounts[1].exsistInShamrock__c = true;
        insert testAccounts;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testAccounts[0].ParentId = testAccounts[1].Id;
        update testAccounts[0];
        testLog.Account__c = testAccounts[0].Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_IntegrationWrapper.createAccountCalloutSynchronous(testAccounts[0].Id,testLog.Id,'testresponse');
        Test.stopTest();
        
    }
    
    @isTest
    static void AddLee_IntegrationWrapper_createAccountSynchronous() {
    	Integer expectedValue = 2;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(false,expectedValue);
        testAccounts[1].exsistInShamrock__c = true;
        insert testAccounts;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testAccounts[0].ParentId = testAccounts[1].Id;
        update testAccounts[0];
        testLog.Account__c = testAccounts[0].Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_IntegrationWrapper.createAccountCalloutSynchronous(testAccounts[0].Id,testLog.Id,'test');
        Test.stopTest();
        
    }

    @isTest
    static void AddLee_IntegrationWrapper_createAccountSynchronousSessionExpired() {
        Integer expectedValue = 2;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(false,expectedValue);
        testAccounts[1].exsistInShamrock__c = true;
        insert testAccounts;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testAccounts[0].ParentId = testAccounts[1].Id;
        update testAccounts[0];
        testLog.Account__c = testAccounts[0].Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
            AddLee_IntegrationWrapper.createAccountCalloutSynchronous(testAccounts[0].Id,testLog.Id,'testresponse');
        Test.stopTest();
        
    }
    
    @isTest
    static void AddLee_IntegrationWrapper_createAccountAsynchSessionExpire() {
    	Integer expectedValue = 1;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(false,expectedValue)[0];
        insert testAccount;
        //testAccounts[0].ParentId = testAccounts[1].Id;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testLog.Account__c = testAccount.Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_IntegrationWrapper.createAccountCalloutAsynch(testAccount.Id,testLog.Id);
        Test.stopTest();
        
    }
    
    @isTest
    static void AddLee_IntegrationWrapper_createAccountAsynch() {
    	Integer expectedValue = 1;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(false,expectedValue)[0];
        insert testAccount;
        //testAccounts[0].ParentId = testAccounts[1].Id;
        Log__c testlog = AddLee_Trigger_Test_Utils.createLogs(1,'Create')[0];
        testLog.Account__c = testAccount.Id;
        insert testLog;
        //AddLee_IntegrationWrapper integrationWrapper = new AddLee_IntegrationWrapper();
        Test.startTest();
        	Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        	AddLee_IntegrationWrapper.createAccountCalloutAsynch(testAccount.Id,testLog.Id);
        Test.stopTest();
        
    }
}