@isTest
private class ACPControllerTest {
    private static final String SEARCHTERM = 'Test ';
    
    static testMethod void searchAccountsAndLeads_Test() {

        AddLee_Trigger_Test_Utils.insertCustomSettings();
        insert TestDataFactory.createTestAccount('Test Account', 'Business Account');
        insert TestDataFactory.createTestLead('Test Lead');
        Map<String,String> inputString = new Map<String,String>{'term' => SEARCHTERM};
        
        List<User> userList = [Select Id from User Where Profile.Name like 'WestOne%' And IsActive = true limit 1];
        userList.add([Select Id from User 
                    Where (Not Profile.Name like 'WestOne%')
                    And Profile.Name != 'System Administrator'
                    And IsActive = true limit 1]);
        List<Map<Object, Object>> dtoResult = new List<Map<Object, Object>>();
        Test.startTest();
            if(userList.size() > 0){
                for(User thisUser : userList){
                    system.runAs(thisUser){
                        dtoResult = ACPController.searchAccountsAndLeads(inputString);
                        ACPController.searchPersonAccountsAndLeads(inputstring);
                        ACPController.getAccountSalesLedgerSet();
                    }
                }
            }
            dtoResult = ACPController.searchAccountsAndLeads(inputString);
            
        Test.stopTest();
        system.assertNotEquals(dtoResult.size(), 0);
        system.assertEquals(dtoResult.get(0).get('value'), 'Test Lead');
        system.assertEquals(dtoResult.get(1).get('value'), 'Test 0');

    }
}