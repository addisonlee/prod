/****************************************************************************
This is the handler class for NPS Trigger
1. Craeting case for detractor scores
2. Calculating difference between recent 2 NPS scores for Contact
3. Copying External id from NPS to contact
*****************************************************************************/

public Class NpsTriggerHandler
{
    public static Boolean BYPASSTRIGGER = false;
    public Static void afterInsert(List<NPS__c> lstNpsRecords)
    {
        List<Case> lstCase=new List<Case>();
        List<Id> lstContactId=new List<Id>();
        Set<Id> lstnpsId =new Set<Id>();
        Map<Id,List<Interaction__c>> mapNpsInteractionIds=new Map<Id,List<Interaction__c>>();
        for(NPS__c nps:lstNpsRecords)
        {
            if(nps.Submit__c && (nps.Score__c == 'Red' || nps.Score__c == 'Amber') && nps.Case__c == null && nps.Account__c != null)
            {
                lstnpsId.add(nps.Id);
            }
        }
        
        if(lstnpsId.isEmpty()){
            return;
        }
        
        if(!lstnpsId.isEmpty())
        {
            List<Interaction__c> lstInteraction=[Select Id,NPS__c,Booking_Summary__c from Interaction__c Where NPS__c IN : lstnpsId];
            if(!lstInteraction.isEmpty())
            {
                for(Interaction__c inte:lstInteraction)
                {
                    if(mapNpsInteractionIds.containsKey(inte.NPS__c))
                    {
                        List<Interaction__c> lstint=mapNpsInteractionIds.get(inte.NPS__c);
                        lstint.add(inte);
                        mapNpsInteractionIds.put(inte.NPS__c,lstint);
                    }else{
                        mapNpsInteractionIds.put(inte.NPS__c,new List<Interaction__c>{inte});
                    }
                }
            }
        }
        
        Group grp=[select Id from Group where DeveloperName = 'Contact_Centre_Admin_Case_Queue' and Type = 'Queue']; // Assigning to case queue 
        for(NPS__c nps:lstNpsRecords) // Creating case when score =Red or Amber
        {
            if(nps.Submit__c && (nps.Score__c == 'Red' || nps.Score__c == 'Amber') && nps.Case__c == null)
            {
                Case cas=new Case();
                if(nps.Account__c != null)
                {
                    cas.AccountId=nps.Account__c;
                }
                cas.OwnerId=grp.Id;
                cas.origin = 'Email-NPS';
                cas.ContactId=nps.Contact__c;
                cas.NPS_Score__c=nps.Score__c;
                cas.NPS_Category__c=nps.Category__c;
                cas.Subject='NPS Feedback';
                cas.NPS_Date_Responded__c=nps.Date_Responded__c;
                cas.NPS__c = nps.Id;
                if(mapNpsInteractionIds.containsKey(nps.Id) && mapNpsInteractionIds.get(nps.Id).size() ==1){
                    cas.Booking_Summary__c = mapNpsInteractionIds.get(nps.Id)[0].Booking_Summary__c;
                }
                if(nps.Comments__c != null){
                    cas.Description =nps.Comments__c;
                }
                lstCase.add(cas);
            }
        }
        
        if(!lstCase.isEmpty())
        {
            System.debug(lstCase);
            insert lstCase;
            
            List<NPS__c> lstupdatenps=new List<NPS__c>();
            for(Case caseInfo:lstCase)
            {
                NPS__c nps=new NPS__c(Id=caseInfo.NPS__c);
                nps.Case__c =caseInfo.Id;
                lstupdatenps.add(nps);
            }
            NpsTriggerHandler.BYPASSTRIGGER =true;
            update lstupdatenps;
            NpsTriggerHandler.BYPASSTRIGGER =false;
            
            Case qCase=[Select Id,Booking_Summary__c,Invoice_Number__c,Job_Number__c,Booking_Summary__r.Account_Number__c,Booking_Summary__r.Date_From__c,Booking_Summary__r.Date_To__c,Booking_Summary__r.Pick_Up_time__c from case Where Id IN :lstCase limit 1];
            if(qCase != null && qCase.Booking_Summary__c != null && qCase.Booking_Summary__r.Pick_Up_time__c != null)
            {
                Datetime fromDateTime = (DateTime)(qCase.Booking_Summary__r.Pick_Up_time__c.date());
                Datetime toDateTime = (DateTime)(qCase.Booking_Summary__r.Pick_Up_time__c.date());
                CaseUpdater.getJobs(qCase.Id,Long.valueOf(qCase.Booking_Summary__r.Account_Number__c),fromDateTime,toDateTime,qCase.Invoice_Number__c,qCase.Job_Number__c,1);
            }
        }
        
        
        
    }
    
    public Static void afterUpdate(List<NPS__c> lstNpsRecords,Map<Id,NPS__c> mapNpsRecords)
    {
        List<Id> lstContactId=new List<Id>();
        for(NPS__c nps:lstNpsRecords)
        {
            System.debug(nps.nps_score__c);
            System.debug(mapNpsRecords.get(nps.Id).nps_score__c);
            if(nps.Score__c != null && nps.Score__c != mapNpsRecords.get(nps.Id).Score__c)
            {
                lstContactId.add(nps.Contact__c);
            }
        }
        
        
        if(!lstContactId.isEmpty()){
            Map<Id,NPS__c> mapNps=new Map<Id,NPS__c>([Select Id,Score__c,Score_Numeric__c,nps_score__c,Contact__c from NPS__c where Contact__c IN : lstContactId and Score_Numeric__c != null order by LastModifiedById asc]);
            System.debug('Size: '+mapNps.size());
            Map<Id,Decimal> mapContactId=new Map<Id,Decimal>();
            for(Id npsid:mapNps.keySet())
            {
                for(NPS__c nps:lstNpsRecords)
                {
                    if(npsid != nps.Id && nps.Contact__c == mapNps.get(npsid).Contact__c)
                    {
                        mapContactId.put(nps.Contact__c,(nps.Score_Numeric__c-mapNps.get(npsid).Score_Numeric__c));    
                    }
                }
            }
            
            // Calculating difference between the scores based on latest 2 survey results submitted by same conatact
            List<Contact> lstContacts=[Select Id from Contact where Id IN : mapContactId.keySet()]; // Updating the differnce between NPS score based on recent score results
            if(!lstContacts.isEmpty())
            {
                List<Contact> lstUpdContacts=new List<Contact>();
                for(Contact cont:lstContacts)
                {
                    Contact con=new Contact(Id=cont.Id);
                    con.NPS_Score_Difference__c = mapContactId.get(cont.Id);
                    lstUpdContacts.add(con);
                }
                if(!lstUpdContacts.isEmpty())
                {
                    update lstUpdContacts;
                }
            }
            
        }
        
    }
    /*Public static void UpdateContact(List<NPS__c> lstNpsRecords) { // Coping externalid from NPS TO Contact ( Because we cant pull NPS id from external Target)
        set<id> contactIdSet = new set<id>();
        for(Nps__c np : lstNpsRecords) {
            if(np.Contact__c != null) {
                contactIdSet.add(np.Contact__c);
            }
        }
        map<id,contact> ContactMap = new map<id,contact>([select id,latest_NPS_ID__c from contact where id in : contactIdSet]);
        for(Nps__c nps : lstNpsRecords) {
            if(nps.Contact__c != null && ContactMap.containskey(nps.Contact__c)) { //!string.isBlank(nps.External_Id__c)
                ContactMap.get(nps.Contact__c).latest_NPS_ID__c = nps.External_Id__c;
                //ContactMap.get(nps.Contact__c).Last_NPS_Score__c= nps.Score_Numeric__c;
            }
        }
        if(ContactMap.size() > 0)
            update ContactMap.values();
        
    }*/
}