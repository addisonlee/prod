@isTest
global class AddleeRefundsGetComplaintTypesMock implements webservicemock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.complaintTypesResponse respElement =  new Addlee_refunds.complaintTypesResponse();
           respElement.complaintTypes = new List<Addlee_refunds.complaintType>();
               Addlee_refunds.complaintType testComplainType = new Addlee_refunds.complaintType();
               testComplainType.id = 68767687;
               testComplainType.name = 'test';
            respElement.complaintTypes.add(testComplainType);   
            respElement.errorCode = '0';   
       response.put('response_x', respElement); 
   }
}