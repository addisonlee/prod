@isTest
private class AddLee_AccountSynch_Test {
    
    @isTest
    static void AccountTrigger_InsertAccount_isAccountInserted() {
		Integer expectedValue = 1;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Account> accounts = AddLee_Trigger_Test_Utils.createAccounts(expectedValue);
		Test.StartTest();
			insert accounts;
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Account WHERE Id in :accounts];
		System.AssertEquals(expectedValue,actualValue);
    }
    
    @isTest
    static void AccountTrigger_UpdateAccount_isAccountUpdated() {
		Integer expectedValue = 1;
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Account> accounts = AddLee_Trigger_Test_Utils.createAccounts(expectedValue);
		insert accounts;
		for(Account eachAccount : accounts){
			eachAccount.AccountNumber = '1234';
	    	eachAccount.ShippingLatitude = 0.01;
            eachAccount.ShippingLongitude = 0.01;
	    	eachAccount.BillingLatitude = 0.02;
            eachAccount.BillingLongitude = 0.02;
		}
		Test.StartTest();
			update accounts;	
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Account WHERE Id in :accounts];
		System.AssertEquals(expectedValue,actualValue);
    }
    
   @isTest
    static void AccountTrigger_InsertContact_isBillingContactInsertedWhenBillingFieldsPopulated(){
    	Integer expectedValue = 1;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	//Log_Contact__c logContact ;
    	List<Log_Contact__c> logContacts = new List<Log_Contact__c>();
    	logContacts.add(new Log_Contact__c(Name = 'Billing' , Field__c = 'SHM_billing_contactName__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Billing1' , Field__c = 'SHM_billing_email__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Billing2' , Field__c = 'SHM_billing_address__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Billing3' , Field__c = 'SHM_billing_fax__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Main' , Field__c = 'SHM_main_contactName__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Main1' , Field__c = 'SHM_main_email__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Main2' , Field__c = 'SHM_main_address__c', Type__c = 'Billing'));
    	logContacts.add(new Log_Contact__c(Name = 'Main3' , Field__c = 'SHM_main_fax__c', Type__c = 'Billing'));
    	insert logContacts;
    	List<Account> accounts = AddLee_Trigger_Test_Utils.createAccount(expectedValue);
    	
    	
    	Test.StartTest();
            for(Account eachAccount : accounts){
    	    	eachAccount.SHM_billing_email__c = 'test@test.com'; 
    	    	eachAccount.SHM_billing_contactName__c = 'Test SurnameTest';
    	    	eachAccount.SHM_billing_address__c = '13th Street';
    	    	eachAccount.SHM_billing_fax__c = '0099';
            }
        	insert accounts;
            for(Account eachAccount : accounts){
    	    	eachAccount.SHM_main_email__c = 'test@test.com'; 
    	    	eachAccount.SHM_main_contactName__c = 'Test SurnameTest';
    	    	eachAccount.SHM_main_address__c = '13th Street';
    	    	eachAccount.SHM_main_fax__c = '0099';
            }
        	update accounts;
    	Test.StopTest();
    	
    	
    	System.assertEquals(expectedValue, [SELECT count() FROM Contact WHERE AccountId in : accounts]);
   	}
    
}