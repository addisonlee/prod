public class AppFeedbackController {
    public String comment {get; set;}
    public List<String> strings {get; set;}
    public String extId {get; set;}
    Private Case caseToUpdate;
    
    public PageReference Redirect(){
        PageReference pRef;
        Map<String, String> params = ApexPages.currentPage().getParameters();
		extId = params.get('msgId');
		if(extId != null) {
            try{
                Case cse = [Select description From Case Where External_Id__c =: extId];
                strings = cse.description.split('\n');
            }Catch(exception e){
                pRef = new PageReference('https://www.addisonlee.com/contact/');
                pRef.setredirect(true);
		        return pRef;
            }
        }
        return null;
    }
    
    public PageReference Submit(){
        PageReference ref;
		caseToUpdate = new Case();
		try{
		    if(comment == null || comment == ''){
		        return null;
		    }
		    system.debug('Input extId '+extId);
		    if(extId != null) caseToUpdate = [Select Id From Case Where External_Id__c =: extId];
		}catch(Exception e){
		    return Page.FileNotFound;
		}
		
		if(caseToUpdate.Id != null  && comment != null){
		    CaseComment com = new CaseComment();
		    com.ParentId = caseToUpdate.Id;
		    com.CommentBody= comment;
            insert com;
		    ref=new PageReference('/apex/Thankyou_NPS');
            ref.setRedirect(true);
            return ref;
		} else{
		    return Page.FileNotFound;
		}
		
		return null;
	}
}