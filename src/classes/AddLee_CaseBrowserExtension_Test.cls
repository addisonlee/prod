@isTest
private class AddLee_CaseBrowserExtension_Test{
	@isTest
	static void CaseBrowserExtension_SearchShamrock_isSearchResultsReturned(){
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		//Account testAccount = AddLee_Trigger_Test_Utils.createAccount(1)[0];
		//testAccount.Account_Number__c = '111';
		//insert testAccount;
		Case testCase = AddLee_Trigger_Test_Utils.createCases(1)[0];
		//testCase.AccountId = testAccount.Id;
		insert testCase;
		Test.startTest();
			PageReference pageRef = Page.CaseBrowser;
			//pageRef.getParameters().put('businessName','test');
			
			Test.setCurrentPage(pageRef);
			ApexPages.StandardController controller = new ApexPages.StandardController(testCase);
			//controller.
			AddLee_CaseBrowserExtension caseBrowserController = new AddLee_CaseBrowserExtension(controller);
			caseBrowserController.invoiceNumber = '11';
			caseBrowserController.dateFrom = System.NOW();
			caseBrowserController.dateTo = System.NOW().addDays(1);

			caseBrowserController.search();
		Test.stopTest();

		System.assert(caseBrowserController.JobResponses.size() > 0);
		System.assert(caseBrowserController.renderResults == true);
	}

	@isTest
	static void CaseBrowserExtension_SearchShamrock_isSelectedSearchResultDetailsReturned(){
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		//Account testAccount = AddLee_Trigger_Test_Utils.createAccount(1)[0];
		//testAccount.Account_Number__c = '111';
		//insert testAccount;
		Case testCase = AddLee_Trigger_Test_Utils.createCases(1)[0];
		//testCase.AccountId = testAccount.Id;
		insert testCase;
		Test.startTest();
			PageReference pageRef = Page.CaseBrowser;
			//pageRef.getParameters().put('businessName','test');
			
			Test.setCurrentPage(pageRef);
			ApexPages.StandardController controller = new ApexPages.StandardController(testCase);
			//controller.
			AddLee_CaseBrowserExtension caseBrowserController = new AddLee_CaseBrowserExtension(controller);
			caseBrowserController.invoiceNumber = '11';
			caseBrowserController.dateFrom = System.NOW();
			caseBrowserController.dateTo = System.NOW().addDays(1);
			caseBrowserController.search();
			caseBrowserController.JobResponses[0].isSelected = true;
			caseBrowserController.getDetails();
		Test.stopTest();

		System.assert(caseBrowserController.jobDetailRecord != null);
		System.assert(caseBrowserController.renderJobDetail == true);
	}

	@isTest
	static void CaseBrowserExtension_UpdateCase_isCaseUpdatedWithSearchResultDetailsReturned(){
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		//Account testAccount = AddLee_Trigger_Test_Utils.createAccount(1)[0];
		//testAccount.Account_Number__c = '111';
		//insert testAccount;
		Case testCase = AddLee_Trigger_Test_Utils.createCases(1)[0];
		//testCase.AccountId = testAccount.Id;
		testCase.Invoice_Number__c = '';
		testCase.Job_Number__c = '';
		insert testCase;
		Test.startTest();
			PageReference pageRef = Page.CaseBrowser;
			//pageRef.getParameters().put('businessName','test');
			
			Test.setCurrentPage(pageRef);
			ApexPages.StandardController controller = new ApexPages.StandardController(testCase);
			//controller.
			AddLee_CaseBrowserExtension caseBrowserController = new AddLee_CaseBrowserExtension(controller);
			caseBrowserController.invoiceNumber = '11';
			caseBrowserController.dateFrom = System.NOW();
			caseBrowserController.dateTo = System.NOW().addDays(1);
			caseBrowserController.search();
			caseBrowserController.JobResponses[0].isSelected = true;
			caseBrowserController.getDetails();
			caseBrowserController.updateCase();
		Test.stopTest();

		testCase = [SELECT ID, Invoice_Number__c, Job_Number__c FROM Case WHERE ID =: testCase.Id];
		System.assert(testCase.Invoice_Number__c == String.ValueOf(1111));
		System.assert(testCase.Job_Number__c == String.ValueOf(1111));
	}
}