/* This is the batch class which is used to create NPS records segmentation engine from the selected reports*/

global class BatchCreateRecordsNPS implements Database.Batchable<sObject>{
    
    //Map<String, Nps_Report__c> nsp;
    global Nps_Report__c nsp;
    global BatchCreateRecordsNPS()
    {
        nsp= Nps_Report__c.getValues('Report Data'); // Custom setting 
        System.debug(nsp);
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select DeveloperName,FolderName,Format,Id,Name from Report Where Name =' + '\'' + nsp.Report_Name__c + '\'');
    }
    
    global void execute(Database.BatchableContext BC, List<sobject> scope){
        
        // Run a report synchronously
        Reports.reportResults results = Reports.ReportManager.runReport(((Report)scope[0]).Id, true);
        List<NPS__c> lstnps=new List<NPS__c>();
        Map<Id,List<Id>> mapContactbookingSummIds=new Map<Id,List<Id>>();
        
        if(((Report)scope[0]).Format == 'Summary') // Summary report
        {
            List<String> lstdetailcolumns=results.getReportMetadata().getDetailColumns();
            integer columnvalue=0;
            integer bookingsummcolumnvalue=0;
            for(Integer j=0;j<lstdetailcolumns.size();j++)
            {
                if(lstdetailcolumns[j] =='Account.Name')
                {
                    columnvalue =j;
                }
                if(lstdetailcolumns[j] =='booking_Summary__c.Name' || lstdetailcolumns[j] == 'CUST_NAME')
                {
                    bookingsummcolumnvalue =j;
                }
            }
            Reports.Dimension dim = results.getGroupingsDown();
            Reports.ReportFactWithDetails detailFact = (Reports.ReportFactWithDetails)results.getFactMap().get('T!T');
            Map<Id,List<Id>> mapAccountContactIds=new Map<Id,List<Id>>();
            Integer recordSize;
            if(nsp.No_of_records_Percentage__c.contains('%')) // Segmenation based on % calcualtion based on report or No of records given as number
            {
                recordSize=(Integer.valueOf(detailFact.getAggregates()[0].getvalue())*Integer.valueOf(nsp.No_of_records_Percentage__c.removeEnd('%')))/100;
            }else{
                recordSize =Integer.valueOf(nsp.No_of_records_Percentage__c);
            }
            System.debug(recordSize);
            
            for(Integer i=0;i<dim.getGroupings().size();i++)
            {
                Reports.GroupingValue groupingVal = dim.getGroupings()[i];
                String factMapKey = groupingVal.getKey() + '!T';
                Reports.ReportFactWithDetails factDetails =(Reports.ReportFactWithDetails)results.getFactMap().get(factMapKey);
                for(Reports.ReportDetailRow rows:factDetails.getRows())
                {
                    if(recordSize > mapAccountContactIds.size() ){
                        Id contactId=(Id)groupingVal.getValue();
                        Id accountId=(Id)rows.getDataCells()[columnvalue].getValue();
                        if(mapAccountContactIds.containsKey(contactId))
                        {
                            List<Id> lstaccount=mapAccountContactIds.get(contactId);
                            lstaccount.add(accountId);
                            mapAccountContactIds.put(contactId,lstaccount);
                        }else{
                            mapAccountContactIds.put(contactId,new List<Id>{accountId});
                        }
                    }
                    
                    if(recordSize > mapContactbookingSummIds.size() ){
                        Id contactId=(Id)groupingVal.getValue();
                        Id bookingSummId=(Id)rows.getDataCells()[bookingsummcolumnvalue].getValue();
                        if(mapContactbookingSummIds.containsKey(contactId))
                        {
                            List<Id> lstbooksum=mapContactbookingSummIds.get(contactId);
                            lstbooksum.add(bookingSummId);
                            mapContactbookingSummIds.put(contactId,lstbooksum);
                        }else{
                            mapContactbookingSummIds.put(contactId,new List<Id>{bookingSummId});
                        }
                    }
                    
                }
            }
            
            if(!mapAccountContactIds.isEmpty()) // Creating NPS Records and assigning values
            {
                
                for(Id contactId:mapAccountContactIds.keySet())
                {
                    NPS__c nps=new NPS__c();
                    nps.Contact__c =contactId;
                    nps.External_Id__c=getExternalId();
                    nps.Date_Sent__c=date.Today();
                    nps.Account__c =mapAccountContactIds.get(contactId)[0];
                    lstnps.add(nps);
                }
            }
        }
        
        if(((Report)scope[0]).Format == 'Tabluar') // Tabular report segmentation
        {
            for(String data:results.getFactMap().keySet())
            {
                Reports.ReportFactWithDetails factDetails =(Reports.ReportFactWithDetails)results.getFactMap().get(data);
                //for(Reports.ReportDetailRow detailRow:factDetails.getRows())
                Integer recordSize;
                if(nsp.No_of_records_Percentage__c.contains('%'))
                {
                    recordSize=(Integer.valueOf(factDetails.getAggregates()[0].getLabel())*Integer.valueOf(nsp.No_of_records_Percentage__c.removeEnd('%')))/100;
                }else{
                    recordSize =Integer.valueOf(nsp.No_of_records_Percentage__c);
                }
                
                for(integer i=0;i<recordSize;i++)
                {   
                    if(lstnps.size() >= recordSize)
                    {
                        break;
                    }
                    
                    NPS__c nps=new NPS__c(); 
                    for(Reports.ReportDataCell detailCell:factDetails.getRows()[i].getDataCells())
                    {
                        if(detailCell.getlabel().startswith('003')) // Getting contactid
                        {
                            nps.Contact__c =(Id)(detailCell.getvalue());
                        }
                        if(detailCell.getlabel().startswith('001'))// Getitng accountid
                        {
                            nps.Account__c =(Id)detailCell.getvalue();
                        }
                        if(detailCell.getlabel().startswith(booking_Summary__c.sObjectType.getDescribe().getKeyPrefix())){
                            mapContactbookingSummIds.put(nps.Contact__c,new List<Id>{(Id)detailCell.getvalue()});
                        }
                    }
                    nps.External_Id__c=getExternalId();
                    lstnps.add(nps);
                }
            }
        }
        
        if(!lstnps.isEmpty())
        {
            System.debug(lstnps.size());
            System.debug(lstnps);
            insert lstnps;
            List<Interaction__c> lstinteraction=new List<Interaction__c>();
            for(NPS__c nps:lstnps){
                for(Id bookingSummId:mapContactbookingSummIds.get(nps.Contact__c)){
                    System.debug(bookingSummId);
                    Interaction__c inte=new Interaction__c(NPS__c=nps.Id,Booking_Summary__c=bookingSummId);
                    lstinteraction.add(inte);
                }
            }
            if(!lstinteraction.isEmpty()){
                insert lstinteraction;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) //External id random creation on NPS record
    {
    }
    
    private static String getExternalId(){
        Integer len = 14;
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
}