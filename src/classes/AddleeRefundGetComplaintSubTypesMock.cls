@isTest
global class AddleeRefundGetComplaintSubTypesMock implements WebServiceMock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.complaintSubTypesResponse respElement =  new Addlee_refunds.complaintSubTypesResponse();
             respElement.complaintSubTypes = New list<Addlee_refunds.complaintSubType>();
             Addlee_refunds.complaintSubType testSubType =  new Addlee_refunds.complaintSubType();
             Addlee_refunds.complaintType testComplainType = new Addlee_refunds.complaintType();
               testComplainType.id = 3232243;
               testComplainType.name = 'salesforce';
             testSubType.complaintType = testComplainType;  
             testSubType.id = 2412144;
             testSubType.name = 'test';  
             respElement.complaintSubTypes.add(testSubType);
         
       response.put('response_x', respElement); 
   }
    
}