global class ScheduleBookingSummaryBatch implements schedulable {
    global void execute (SchedulableContext SC){
        BookingSummaryBatch bSB = new BookingSummaryBatch();
        Database.executebatch(bSB);
    }
}