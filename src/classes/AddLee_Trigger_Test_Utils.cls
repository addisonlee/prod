@isTest
public with sharing class AddLee_Trigger_Test_Utils {

    public static List<Case> createCases(Integer expectedCases){
        List<Case> casesList = new List<Case>();
        for(Integer index = 0; index < expectedCases; index++){
            Case a = new Case(Subject = 'Test Subject ' + index);
            casesList.add(a);
        }
        return casesList;
    }

    public static List<Lead> createLeads(Integer expectedLeads){
        List<Lead> leadsList = new List<Lead>();
        for(Integer index = 0; index < expectedLeads; index++){
            Lead a = new Lead(LastName = 'Test ' + index,  Company = 'Test', PostalCode = 'SE1 0HS', Payment_Type__c = 'Direct Debit');
            leadsList.add(a);
        }
        return leadsList;
    }
    
    public static List<Contact> createContacts(Integer expectedContacts){
        List<Contact> contactsList = new List<Contact>();
        Account account = createAccounts(1)[0];
        insert account;
        for(Integer index = 0; index < expectedContacts; index++){
            Contact a = new Contact(LastName = 'Test ' + index, AccountId = account.Id,SHM_contactName__c = 'test',Contact_Type__c = 'Main Contact', email = 'test@test.com');
            contactsList.add(a);
        }
        return contactsList;
    }
    
    public static List<Contact> createAllContacts(Integer expectedContacts, string SHMName){
        List<Contact> contactsList = new List<Contact>();
        Account account = createAccounts(1)[0];
        insert account;
        for(Integer index = 0; index < expectedContacts; index++){
            Contact a = new Contact(LastName = 'Test ' + index, FirstName = 'Test ' + index, AccountId = account.Id,SHM_contactName__c = SHMName,Contact_Type__c = 'Main Contact', email = 'test@test.com');
            contactsList.add(a);
        }
        return contactsList;
    }
    public static List<Contact> optOutContacts(Integer expectedContacts){
        List<Contact> contactsList = new List<Contact>();
        Account account = optOutAccount(1)[0];
        insert account;
        for(Integer index = 0; index < expectedContacts; index++){
            Contact a = new Contact(LastName = 'Test ' + index, AccountId = account.Id, SHM_contactName__c = 'test', Contact_Type__c = 'Main Contact', email = 'test@test.com');
            contactsList.add(a);
        }
        return contactsList;
    }
    public static List<Contact> optOutContacts2 (Integer expectedContacts){
        List<Contact> contactsList = new List<Contact>();
        Account account = optOutAccount2(1)[0];
        insert account;
        for(Integer index = 0; index < expectedContacts; index++){
            Contact a = new Contact(LastName = 'Test ' + index, AccountId = account.Id,SHM_contactName__c = 'test',Contact_Type__c = 'Main Contact', email = 'test@test.com');
            contactsList.add(a);
        }
        return contactsList;
    }
    
    public static List<Account> createAccounts(Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index, Industry = 'Professional, Scientific and Technical', Industry_Sub_Sector__c = 'Management Consulting',OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c =true);
            accountsList.add(a);
        }
        return accountsList;
    }
    public static List<Account> optOutAccount ( Integer expectedAccounts ){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index, Opt_Out_Of_Email_Marketing__c = true );
            accountsList.add(a);
        
         }
         return accountsList;
    }
    public static List<Account> optOutAccount2 ( Integer expectedAccounts ){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index, Opt_Out_Of_SMS_Marketing__c = true );
            accountsList.add(a);
        
         }
         return accountsList;
    }
    
    
    public static List<Account> createAccount(Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index);
            accountsList.add(a);
        }
        return accountsList;
    }
    
    public static List<Account> createAccountOne(Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index,Booked__c=false,Account_Number__c = '1');
            accountsList.add(a);
        }
        return accountsList;
    }
     public static List<Account> createAccountBooker(Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Account a = new Account(Name = 'Test ' + index,type= 'main', Account_Number__c = '63630978');
            accountsList.add(a);
        }
        return accountsList;
    }
    public static List<Contact> ContactBooker(Integer expectedContacts){
        List<Contact> contactsList = new List<Contact>();
        Account account = optOutAccount2(1)[0];
        insert account;
        for(Integer index = 0; index < expectedContacts; index++){
            Contact a = new Contact(LastName = 'Test ' + index, AccountId = account.Id,SHM_contactName__c = 'test',Booker_Id__c='12345678',Contact_Type__c = 'Main Contact', email = 'test@test.com');
            contactsList.add(a);
        }
        return contactsList;
    }

    public static List<Log__c> createLogs(Integer expectedLogs, String Type){
        Account a = new Account(Name = 'Test Logs' , SHM_number_x__c='123456');
        insert a;
        
        List<Log__c> logsList = new List<Log__c>();
        for(Integer index = 0; index < expectedLogs; index++){
            Log__c l = new Log__c(Account__c = a.Id, Status__c = 'Waiting', Type__c = Type, Description__c = 'Test');
            logsList.add(l);
        }
        return logsList;
    }
    
    public static List<Account> createAccountWithIntegrationReady(Boolean integrationReady, Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        if(integrationReady){
            for(Integer index = 0; index < expectedAccounts; index++){
                Account a = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Prospect');
                accountsList.add(a);
            }
        }else{
            for(Integer index = 0; index < expectedAccounts; index++){
                Account a = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = false, SHM_status_name__c = 'Prospect');
                accountsList.add(a);
            }
        }
        return accountsList;
    }

    public static List<Account> createAccountWithIntegrationReadyAndCurrent(Boolean integrationReady, Integer expectedAccounts){
        List<Account> accountsList = new List<Account>();
        if(integrationReady){
            for(Integer index = 0; index < expectedAccounts; index++){
                Account a = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Current', Account_Status__c = 'Current',
                                        Industry = 'Construction', Administration_Fee__c = 10, Invoicing_Frequency__c = 'Monthly', Payment_Terms__c = 30, Payment_Type__c = 'BACS', Company_Charity_Reg_No__c = '122', Credit_Checked__c = true, 
                                        Credit_Limit__c = 50, Sales_Ledger__c = 'Sales Ledger');
                accountsList.add(a);
            }
        }else{
            for(Integer index = 0; index < expectedAccounts; index++){
                Account a = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = false, SHM_status_name__c = 'Prospect');
                accountsList.add(a);
            }
        }
        return accountsList;
    }
    
    public static Account createAccountWithIntegrationReadyAndNotExistInShamrock(){
        return new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', exsistInShamrock__c = false, integrationReady__c = true);
    }
    
    public static List<Booking_Summary__c> createBookingSummaryTest(String accountId, Integer expectedAccounts){
        List<Booking_Summary__c> bookingList = new List<Booking_Summary__c>();
        for(Integer index = 0; index < expectedAccounts; index++){
            Booking_Summary__c b = new Booking_Summary__c(
                                                individual_id__c = '123456', 
                                                account__c = accountId, 
                                                individual_name__c = 'TestName', 
                                                individual_pin__c = '1234', 
                                                individual_telephone__c = '123456', 
                                                individual_email__c = 'test@gmail.com', 
                                                number_of_bookings__c = 3, 
                                                payment_type__c = 'CASH', 
                                                service_type__c = 'TAXI',
                                                total_journey_price__c = 200, 
                                                total_extras__c = 10, 
                                                total_waiting_time__c = 100);
            bookingList.add(b);
        }
        return bookingList;
    }
    
    public static void insertCustomSettings(){
        Log_Field_Mapping__c fieldMapping = Log_Field_Mapping__c.getValues('Object');
        if(fieldMapping == null){
            fieldMapping = new Log_Field_Mapping__c(Name='Object');
            fieldMapping.Object__c = 'Account';
            fieldMapping.Name = 'Name';
            insert fieldMapping;
        }
        
        TECH_Auto_Number__c autoNumber1 = new TECH_Auto_Number__c(Name = 'AddLeeRange', Current_Number__c = 1 , End_Number__c = 3, Start_Number__c = 1);
        insert autoNumber1;
        
        TECH_Auto_Number__c autoNumber2 = new TECH_Auto_Number__c(Name = 'WestOneRange', Current_Number__c = 4 , End_Number__c = 6, Start_Number__c = 4);
        insert autoNumber2;

        Log_Integration__c logIntegration = new Log_Integration__c(name = 'shamrock_setting', enableUpdates__c = true, Username__c = 'test', Password__c = 'test', CurrentSessionId__c = 'test', EndPoint__c='http://test.com', Enable_Bulk_Processing__c = false);
        insert logIntegration;
        
        Account_Auto_Number_Increment__c accountAutoNumber = new Account_Auto_Number_Increment__c(name = 'Account Number', Current_Number__c = 1, Start_Number__c = 1, End_Number__c = 99999);
        insert accountAutoNumber;
        
        TECH_Auto_Number__c currentAccountNumberRange = new TECH_Auto_Number__c(name = 'Account Number', Current_Number__c = 1, Start_Number__c = 1, End_Number__c = 99999) ;
        insert currentAccountNumberRange;

        PrivateKey__c privateKey = new PrivateKey__c(name = 'Key', CurrentKey__c='addisonleeTEST14');
        insert privateKey;

        Account_Creation_Settings__c accountCreationSettings = new Account_Creation_Settings__c(Name='credentials', pcaKey__c = 'test');
        insert accountCreationSettings;

        Call_Credit__c callCredit = new Call_Credit__c(Name='credentials', CompanyName__c = 'test', Password__c = 'test', Threshold__c = 580.0 , Username__c='test', endpointURL__c = 'http://www.test.com');
        insert callCredit;

        D_AND_B__c dAndB = new D_AND_B__c(name='credentials', Username__c = 'test', Password__c = 'test');
        insert dAndB;

        Segmentation_London_Employees_Banding__c londonBanding = new Segmentation_London_Employees_Banding__c(Name = '100-199', London_Employee_Value__c = 150, Number_of_London_Employees__c = '100-199');
        insert londonBanding;
        
        Account account = optOutAccount2(1)[0];
        insert account;
        
        Entitlement entitlement=new Entitlement(Name='Account Demo',AccountId=account.Id,StartDate=system.Today().addDays(-1));
        insert entitlement;
        SLA_Unmanaged__c slaunmanged=new SLA_Unmanaged__c(X72_Hour_Support__c =account.Id);
        insert slaunmanged;

        //List<Campaign_Automation__c> campaignAutomation = new List<Campaign_Automation__c>();
        List<sObject> testData = Test.loadData(Campaign_Automation__c.sObjectType,'testCases'); 
        /*for(sObject eachSObject : testData){
            Campaign_Automation__c eachCampaignRecord = (Campaign_Automation__c)eachSObject;
            campaignAutomation.add(eachCampaignRecord);
        }
        System.DEBUG(campaignAutomation);
        upsert campaignAutomation;*/

    }
    
    public static Id accRecordType(string accType){
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        return (ID)AccountRecordTypeInfo .get(accType).getRecordTypeId();
    }

    @isTest
    public static void insertTestEmployeeSizeband(){ 
      Swim_Lane__c testEmployeeSizeBand = new Swim_Lane__c(Industry_Type__c = 'Management Consulting', Sole_Trader__c = 'Small', X6_to_10__c = 'Small', X50_to_99__c = 'Medium', X500_to_999__c = 'Large', X20_to_49__c = 'Medium', X200_to_499__c = 'Large', X1_to_5__c = 'Small' , X11_to_19__c = 'Small', X100_to_199__c = 'Large', X1000__c = 'Large' );
      insert testEmployeeSizeBand;
    }
    
    @isTest
    public static void insertTestTargetActiveUsersBand(){ 
      Active_Users__c testActiveUsers = new Active_Users__c(Industry_Type__c = 'Management Consulting', Type__c = 'Target Users', Sole_Trader__c = null, X6_to_10__c = 54, X50_to_99__c = 20, X500_to_999__c = 40, X20_to_49__c = 38, X200_to_499__c = 40, X1_to_5__c = 91 , X11_to_19__c = 59, X100_to_199__c = 40, X1000__c = 34 );
      insert testActiveUsers;
    }
    
    @isTest
    public static void insertTestExpectedActiveUsersBand(){ 
      Active_Users__c testActiveUsers = new Active_Users__c(Industry_Type__c = 'Management Consulting', Type__c = 'Expected Users', Sole_Trader__c = null, X6_to_10__c = 34, X50_to_99__c = 10, X500_to_999__c = 9, X20_to_49__c = 20, X200_to_499__c = 12, X1_to_5__c = 54 , X11_to_19__c = 27, X100_to_199__c = 15, X1000__c = 8 );
      insert testActiveUsers;
    }
    
    @isTest
    public static void insertTestTargetSpendExpenditure(){ 
      Spend_Per_Employee__c testSpendPerEmployee = new Spend_Per_Employee__c(Industry_Type__c = 'Management Consulting', Type__c = 'Target Spend', Sole_Trader__c = null, X6_to_10__c = 724, X50_to_99__c = 274, X500_to_999__c = 783, X20_to_49__c = 496, X200_to_499__c = 848, X1_to_5__c = 1039 , X11_to_19__c = 645, X100_to_199__c = 645, X1000__c = 593 );
      insert testSpendPerEmployee;
    }
    
    @isTest
    public static void insertTestExpectedSpendExpenditure(){ 
      Spend_Per_Employee__c testSpendPerEmployee = new Spend_Per_Employee__c(Industry_Type__c = 'Management Consulting', Type__c = 'Expected Spend', Sole_Trader__c = null, X6_to_10__c = 271, X50_to_99__c = 102, X500_to_999__c = 173, X20_to_49__c = 204, X200_to_499__c = 307, X1_to_5__c = 354 , X11_to_19__c = 264, X100_to_199__c = 276, X1000__c = 155 );
      insert testSpendPerEmployee;
    }
}