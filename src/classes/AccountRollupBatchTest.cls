@istest(SeeAllData=false)
// *********************************************************************************** 
//
//  AccountRollupBatchTest
//    
//  Desciption:
//  Class to test the processing of the Account Rollup Batch Apex Calculation, including
//  the scheduled execution.
//   
//  History:
//  InSitu  01/31/2014  Original version. 
//
// ***********************************************************************************
//*********************************************************************************** 
//
//  AccountRollupBatchTest
//
//  Desciption:
//  This class was created by InSitu Software for AddisonLee. It it the Class to test
//  the processing of the Account Rollup Batch Apex Calculation, including
//  the scheduled execution.
//	FOR EXCLUSIVE USE OF ADDISON LEE ONLY
//
//  History:
//  InSitu  02/18/2016  Original version.
// ***********************************************************************************

class AccountRollupBatchTest 
{
	public static final String CASE_STATUS_OPEN = 'New';
    public static final String CASE_STATUS_CLOSED = 'Closed';

    public static final String CASE_ORIGIN = 'Facebook';
    public static final String CASE_CATEGORY = 'Admin';
    public static final String CASE_TYPE = 'Other';
    	
	
    //--------------------------------------------------------------------------------
    //  testAccountRollupBatch_Sched()
    // -------------------------------------------------------------------------------
    public static testMethod void testAccountRollupBatch_Sched() 
    {
        Test.startTest();

        // Run the scheduled job at midnight Sept. 3rd. 2022  
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('testScheduledUPCalc', CRON_EXP, new AccountRollupSched());
        
        // Get the information from the CronTrigger API object  
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same  
        System.assertEquals(CRON_EXP, ct.CronExpression);

        // Verify the job has not run  
        System.assertEquals(0, ct.TimesTriggered);

        // Verify the next time the job will run  
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
    //--------------------------------------------------------------------------------
    //  testAccountRollupBatch_RelatedObj()
    // -------------------------------------------------------------------------------
    public static testMethod void testAccountRollupBatch_RelatedObj() 
    {
    	// Create custom settings to bypass trigger processing.
    	Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = true);
		insert disabletrigger;
		
		//NoValidations__c noValidations = new NoValidations__c (SetupOwnerID = UserInfo.getUserID(), Active_Users__c = true);
		//insert noValidations;

		RecordType rtBusinessAcct = [SELECT ID FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Business_Account' LIMIT 1];

		// Query for Case Record Types
		RecordType rtComplaint = [SELECT ID FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Complaints' LIMIT 1];
		RecordType rtOther = [SELECT ID FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName <> 'Complaints' LIMIT 1];
		
        try
        {
	        // ==============================================
	        // Create test data - UP with 2 child accounts - total of 3 locations 
	        // ==============================================
	        Account acctUP1 = new Account(Name = 'AAA-UP1', RecordTypeId = rtBusinessAcct.ID);
	        insert acctUP1;
	        ID idUP1 = acctUP1.Id;
	        
	        // Add 2 Accounts to the ultimate parent.
	        Account acctUP1_Child1 = new Account(Name = 'BBB-Num01', ParentID = idUP1, RecordTypeId = rtBusinessAcct.ID);
	        insert acctUP1_Child1;
	        
	        Account acctUP1_Child2 = new Account(Name = 'CCC-Num02', ParentID = idUP1, RecordTypeId = rtBusinessAcct.ID);
	        insert acctUP1_Child2;
	
	        // Add 1 Accounts as a grand-child
	        Account acctUP1_Child1a = new Account(Name = 'DDD-Num01A', ParentID = acctUP1_Child1.ID, RecordTypeId = rtBusinessAcct.ID);
	        insert acctUP1_Child1a;
	        
	        // Create a contact.
	        Contact contactTest = new Contact (LastName = 'Test Contact', AccountId = idUP1, Active__c = true, Contact_Type__c = 'Other Contact');
	   		insert contactTest;
	   		
			// Create some cases - Complaints, Open, Closed, etc.
			List<Case> listCases = new List<Case>
			{ 
				new Case(Subject = 'AAA', AccountId = acctUP1.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'BBB', AccountId = acctUP1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'CCC', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'DDD', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'EEE', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'FFF', AccountId = acctUP1_Child2.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'GGG', AccountId = acctUP1_Child2.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'HHH', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'III', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'JJJ', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE)
			};			
			insert listCases;
			
			Date dtToday = System.today();
			Date dtLastMonth = System.today().addMonths(-1);
	
			List<Booking_Summary__c> listBookings = new List<Booking_Summary__c>
			{
				new Booking_Summary__c(Name='Book Sum1', Account__c = acctUP1.ID, Total_Journey_Price__c = 100, Number_of_Bookings__c = 10, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum2', Account__c = acctUP1.ID, Total_Journey_Price__c = 200, Number_of_Bookings__c = 20, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum3', Account__c = acctUP1_Child1.ID, Total_Journey_Price__c = 400, Number_of_Bookings__c = 40, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum4', Account__c = acctUP1_Child1.ID, Total_Journey_Price__c = 800, Number_of_Bookings__c = 80, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum5', Account__c = acctUP1_Child2.ID, Total_Journey_Price__c = 1000, Number_of_Bookings__c = 10, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum6', Account__c = acctUP1_Child2.ID, Total_Journey_Price__c = 2000, Number_of_Bookings__c = 20, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum7', Account__c = acctUP1_Child1a.ID, Total_Journey_Price__c = 4000, Number_of_Bookings__c = 40, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum8', Account__c = acctUP1_Child1a.ID, Total_Journey_Price__c = 8000, Number_of_Bookings__c = 80, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth)	
			};
			insert listBookings;
        }
        catch(Exception e)
        {   
            // Error creating contacts - site probably has required custom fields on the object.
            system.debug(Logginglevel.WARN, 'InSitu Test: The following error occurred while creating test Accounts, Cases and Bookings: ' + e.getMessage());
        }
		
		
    	Test.startTest();

    	// Just call the static method to execute the queuing of the batch job.
        ID idJob = AccountRollupBatch.runAccountRollupBatch(10, 20, AccountRollupBatch.RUTYPE_RELATEDOBJS);

    	Test.stopTest();

	    // Query Accounts
	    List<Account> listAccts = [SELECT id, Total_Open_Cases__c, Total_Complaints__c, Total_Open_Complaints__c,
	    								Total_Bookings_Amount_CY__c, Total_Bookings_Amount_CM__c 
	    						   FROM Account 
	    						   ORDER BY Name ASC];
	    						   
	    System.assertEquals(4, listAccts.size());
	    System.assertEquals(1,listAccts[0].Total_Open_Cases__c);
	    System.assertEquals(2,listAccts[0].Total_Complaints__c);
	    System.assertEquals(1,listAccts[0].Total_Open_Complaints__c);
	    System.assertEquals(300, listAccts[0].Total_Bookings_Amount_CY__c);
	    System.assertEquals(100, listAccts[0].Total_Bookings_Amount_CM__c);

	    System.assertEquals(1,listAccts[1].Total_Open_Cases__c);
	    System.assertEquals(1,listAccts[1].Total_Complaints__c);
	    System.assertEquals(0,listAccts[1].Total_Open_Complaints__c);
	    System.assertEquals(1200, listAccts[1].Total_Bookings_Amount_CY__c);
	    System.assertEquals(400, listAccts[1].Total_Bookings_Amount_CM__c);

	    System.assertEquals(1,listAccts[2].Total_Open_Cases__c);
	    System.assertEquals(1,listAccts[2].Total_Complaints__c);
	    System.assertEquals(1,listAccts[2].Total_Open_Complaints__c);
	    System.assertEquals(3000, listAccts[2].Total_Bookings_Amount_CY__c);
	    System.assertEquals(1000, listAccts[2].Total_Bookings_Amount_CM__c);

	    System.assertEquals(2,listAccts[3].Total_Open_Cases__c);
	    System.assertEquals(2,listAccts[3].Total_Complaints__c);
	    System.assertEquals(1,listAccts[3].Total_Open_Complaints__c);
	    System.assertEquals(12000, listAccts[3].Total_Bookings_Amount_CY__c);
	    System.assertEquals(4000, listAccts[3].Total_Bookings_Amount_CM__c);
    }

    //--------------------------------------------------------------------------------
    //  testAccountRollupBatch_RUTYPE_UP()
    // -------------------------------------------------------------------------------
    public static testMethod void testAccountRollupBatch_RUTYPE_UP() 
    {
    	// Create custom settings to bypass trigger processing.
    	Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = true);
		insert disabletrigger;
		
		//NoValidations__c noValidations = new NoValidations__c (SetupOwnerID = UserInfo.getUserID(), Active_Users__c = true);
		//insert noValidations;

		RecordType rtBusinessAcct = [SELECT ID FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Business_Account' LIMIT 1];
		RecordType rtOpp = [SELECT ID FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Default_Sales_Opportunity' LIMIT 1];

		// Query for Case Record Types
		RecordType rtComplaint = [SELECT ID FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Complaints' LIMIT 1];
		RecordType rtOther = [SELECT ID FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName <> 'Complaints' LIMIT 1];
		
		Date dtToday = System.today();
		Date dtLastMonth = System.today().addMonths(-1);
		
		// Query Opportunity Info
		OpportunityStage stageOpenFirst = [SELECT ID, MasterLabel, DefaultProbability 
											FROM OpportunityStage
											WHERE IsClosed = false AND
												  IsActive = true
											Order By SortOrder ASC
											Limit 1];
		String strStageOpenFirst = stageOpenFirst.MasterLabel;
		
		OpportunityStage stageClosedWon = [SELECT ID, MasterLabel, DefaultProbability 
											FROM OpportunityStage
											WHERE IsClosed = true AND
												  IsWon = true AND
												  IsActive = true
											Order By SortOrder ASC
											Limit 1];
		String strStageClosedWon = stageClosedWon.MasterLabel;
	
        try
        {
	        // ==============================================
	        // Create test data - UP with 2 child accounts - total of 3 locations 
	        // ==============================================
	        Account acctUP1 = new Account(Name = 'AAA-UP1', RecordTypeId = rtBusinessAcct.ID);
	        
	        acctUP1.January_Average__c = acctUP1.February_Average__c = acctUP1.March_Average__c = acctUP1.April_Average__c = acctUP1.May_Average__c = acctUP1.June_Average__c = acctUP1.July_Average__c
 				= acctUP1.August_Average__c = acctUP1.September_Average__c = acctUP1.October_Average__c = acctUP1.November_Average__c = acctUP1.December_Average__c = 100;
	        
		    acctUP1.Total_Open_Cases__c = 1;
		    acctUP1.Total_Complaints__c = 2;
		    acctUP1.Total_Open_Complaints__c = 1;
		    acctUP1.Total_Bookings_Amount_CY__c = 300;
		    acctUP1.Total_Bookings_Amount_CM__c = 100;

	        insert acctUP1;
	        ID idUP1 = acctUP1.Id;
	        
	        // Add 2 Accounts to the ultimate parent.
	        Account acctUP1_Child1 = new Account(Name = 'BBB-Num01', ParentID = idUP1, RecordTypeId = rtBusinessAcct.ID);
	        acctUP1_Child1.January_Average__c = acctUP1_Child1.February_Average__c = acctUP1_Child1.March_Average__c = acctUP1_Child1.April_Average__c = acctUP1_Child1.May_Average__c = acctUP1_Child1.June_Average__c = acctUP1_Child1.July_Average__c
 				= acctUP1_Child1.August_Average__c = acctUP1_Child1.September_Average__c = acctUP1_Child1.October_Average__c = acctUP1_Child1.November_Average__c = acctUP1_Child1.December_Average__c = 200;
	        
		    acctUP1_Child1.Total_Open_Cases__c = 1;
		    acctUP1_Child1.Total_Complaints__c = 1;
		    acctUP1_Child1.Total_Open_Complaints__c = 0;
		    acctUP1_Child1.Total_Bookings_Amount_CY__c = 1200;
		    acctUP1_Child1.Total_Bookings_Amount_CM__c = 400;

	        insert acctUP1_Child1;
	        
	        Account acctUP1_Child2 = new Account(Name = 'CCC-Num02', ParentID = idUP1, RecordTypeId = rtBusinessAcct.ID);
	        acctUP1_Child2.January_Average__c = acctUP1_Child2.February_Average__c = acctUP1_Child2.March_Average__c = acctUP1_Child2.April_Average__c = acctUP1_Child2.May_Average__c = acctUP1_Child2.June_Average__c = acctUP1_Child2.July_Average__c
 				= acctUP1_Child2.August_Average__c = acctUP1_Child2.September_Average__c = acctUP1_Child2.October_Average__c = acctUP1_Child2.November_Average__c = acctUP1_Child2.December_Average__c = 300;

		    acctUP1_Child2.Total_Open_Cases__c = 1;
		    acctUP1_Child2.Total_Complaints__c = 1;
		    acctUP1_Child2.Total_Open_Complaints__c = 1;
		    acctUP1_Child2.Total_Bookings_Amount_CY__c = 3000;
		    acctUP1_Child2.Total_Bookings_Amount_CM__c = 1000;

	        insert acctUP1_Child2;
	
	        // Add 1 Accounts as a grand-child
	        Account acctUP1_Child1a = new Account(Name = 'DDD-Num01A', ParentID = acctUP1_Child1.ID, RecordTypeId = rtBusinessAcct.ID);
		    acctUP1_Child1a.Total_Open_Cases__c = 2;
		    acctUP1_Child1a.Total_Complaints__c = 2;
		    acctUP1_Child1a.Total_Open_Complaints__c = 1;
		    acctUP1_Child1a.Total_Bookings_Amount_CY__c = 12000;
		    acctUP1_Child1a.Total_Bookings_Amount_CM__c = 4000;

	        acctUP1_Child1a.January_Average__c = acctUP1_Child1a.February_Average__c = acctUP1_Child1a.March_Average__c = acctUP1_Child1a.April_Average__c = acctUP1_Child1a.May_Average__c = acctUP1_Child1a.June_Average__c = acctUP1_Child1a.July_Average__c
 				= acctUP1_Child1a.August_Average__c = acctUP1_Child1a.September_Average__c = acctUP1_Child1a.October_Average__c = acctUP1_Child1a.November_Average__c = acctUP1_Child1a.December_Average__c = 400;

	        insert acctUP1_Child1a;
	        
	        // Create a contact.
	        Contact contactTest = new Contact (LastName = 'Test Contact', AccountId = idUP1, Active__c = true, Contact_Type__c = 'Other Contact');
	   		insert contactTest;
	   		
			// Create some cases - Complaints, Open, Closed, etc.
			List<Case> listCases = new List<Case>
			{ 
				new Case(Subject = 'AAA', AccountId = acctUP1.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'BBB', AccountId = acctUP1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'CCC', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'DDD', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'EEE', AccountId = acctUP1_Child1.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'FFF', AccountId = acctUP1_Child2.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'GGG', AccountId = acctUP1_Child2.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				
				new Case(Subject = 'HHH', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'III', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_OPEN, RecordTypeId = rtOther.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE),
				new Case(Subject = 'JJJ', AccountId = acctUP1_Child1a.ID, Status = CASE_STATUS_CLOSED, RecordTypeId = rtComplaint.ID, Type = CASE_CATEGORY, Origin = CASE_ORIGIN, ContactID=contactTest.ID, Case_Type__c = CASE_TYPE)
			};			
			insert listCases;
			
			// Create some Bookings - Today and last month
			List<Booking_Summary__c> listBookings = new List<Booking_Summary__c>
			{
				new Booking_Summary__c(Name='Book Sum1', Account__c = acctUP1.ID, Total_Journey_Price__c = 100, Number_of_Bookings__c = 10, Date_To__c = dtToday, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum2', Account__c = acctUP1.ID, Total_Journey_Price__c = 200, Number_of_Bookings__c = 20, Date_To__c = dtLastMonth, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum3', Account__c = acctUP1_Child1.ID, Total_Journey_Price__c = 400, Number_of_Bookings__c = 40, Date_To__c = dtToday, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum4', Account__c = acctUP1_Child1.ID, Total_Journey_Price__c = 800, Number_of_Bookings__c = 80, Date_To__c = dtLastMonth, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum5', Account__c = acctUP1_Child2.ID, Total_Journey_Price__c = 1000, Number_of_Bookings__c = 10, Date_To__c = dtToday, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum6', Account__c = acctUP1_Child2.ID, Total_Journey_Price__c = 2000, Number_of_Bookings__c = 20, Date_To__c = dtLastMonth, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth),	
				
				new Booking_Summary__c(Name='Book Sum7', Account__c = acctUP1_Child1a.ID, Total_Journey_Price__c = 4000, Number_of_Bookings__c = 40, Date_To__c = dtToday, Date_From__c = dtToday, Service_Type__c = 'Lapsed', Date_Time__c = dtToday, Pick_Up_time__c = dtToday),	
				new Booking_Summary__c(Name='Book Sum8', Account__c = acctUP1_Child1a.ID, Total_Journey_Price__c = 8000, Number_of_Bookings__c = 80, Date_To__c = dtLastMonth, Date_From__c = dtLastMonth, Service_Type__c = 'Lapsed', Date_Time__c = dtLastMonth, Pick_Up_time__c = dtLastMonth)	
			};
			
			insert listBookings;

			// Create some Opportunities - Today and last month
			List<Opportunity> listOpps = new List<Opportunity>
			{
				new Opportunity(Name='Opp1', AccountID = acctUP1.ID, Amount = 100, RecordTypeID = rtOpp.ID, CloseDate = dtToday, StageName = strStageOpenFirst),	
				new Opportunity(Name='Opp2', AccountID = acctUP1.ID, Amount = 200, RecordTypeID = rtOpp.ID, CloseDate = dtLastMonth, StageName = strStageClosedWon),	
				
				new Opportunity(Name='Opp3', AccountID = acctUP1_Child1.ID, Amount = 400, RecordTypeID = rtOpp.ID, CloseDate = dtToday, StageName = strStageOpenFirst ),	
				new Opportunity(Name='Opp4', AccountID = acctUP1_Child1.ID, Amount = 800, RecordTypeID = rtOpp.ID, CloseDate = dtLastMonth, StageName = strStageClosedWon ),	
				
				new Opportunity(Name='Opp5', AccountID = acctUP1_Child2.ID, Amount = 1000, RecordTypeID = rtOpp.ID, CloseDate = dtToday, StageName = strStageOpenFirst ),	
				new Opportunity(Name='Opp6', AccountID = acctUP1_Child2.ID, Amount = 2000, RecordTypeID = rtOpp.ID, CloseDate = dtLastMonth, StageName = strStageClosedWon ),	
				
				new Opportunity(Name='Opp7', AccountID = acctUP1_Child1a.ID, Amount = 4000, RecordTypeID = rtOpp.ID, CloseDate = dtToday, StageName = strStageOpenFirst ),	
				new Opportunity(Name='Opp8', AccountID = acctUP1_Child1a.ID, Amount = 8000, RecordTypeID = rtOpp.ID, CloseDate = dtLastMonth, StageName = strStageClosedWon )	
			};
			
			insert listOpps;


        }
        catch(Exception e)
        {   
            // Error creating contacts - site probably has required custom fields on the object.
            system.debug(Logginglevel.WARN, 'InSitu Test: The following error occurred while creating test Accounts, Bookings and Opportunities: ' + e.getMessage());
        }
		
		
    	Test.startTest();
			        	
    	// Just call the static method to execute the queuing of the batch job.
        ID idJob = AccountRollupBatch.runAccountRollupBatch(10, null, AccountRollupBatch.RUTYPE_UP);

    	Test.stopTest();
	
	    // Query Accounts
	    List<Account> listAccts = [SELECT id, Name, InSituCah__Ultimate_Parent__r.ID, 
										Total_Spend_YTD__c, Parent_Total_Spend_YTD__c, 
										Total_Spend_MTD__c, Parent_Total_Spend_MTD__c, 
										Total_Booking_Spend__c, Parent_Total_Booking_Spend__c, 
										Total_Amount_of_Bookings__c, Parent_Total_Num_Bookings_for_Account__c, 
										Total_Bookings_Amount_CY__c, Parent_Total_Bookings_Amount_CY__c, 
										Total_Bookings_Amount_CM__c, Parent_Total_Bookings_Amount_CM__c, 
										Date_of_Last_Booking__c, Parent_Date_of_Last_Booking__c, 
										
										Total_Expected_Revenue__c, Parent_Total_Expected_Revenue__c, 
										Total_Expected_Revenue_Open_Opps__c, Parent_Total_Expected_Revenue_Open_Opps__c, 
										Total_Expected_Revenue_Closed_Opps__c, Parent_Total_Expected_Rev_Closed_Opp__c, 
										Max_Opp_Close_Date__c, Parent_Max_Opp_Close_Date__c, 
										Min_Opp_Close_Date__c, Parent_Min_Opp_Close_Date__c, 
										
										Total_Complaints__c, Parent_Total_Complaints__c, 
										Total_Open_Complaints__c, Parent_Total_Open_Complaints__c, 
										Total_Open_Cases__c, Parent_Total_Open_Cases__c
									FROM Account Account ORDER BY Name ASC];

	    System.assertEquals(4, listAccts.size());
	    System.assertEquals(12000,listAccts[0].Parent_Total_Spend_YTD__c);
	    System.assertEquals(1000,listAccts[0].Parent_Total_Spend_MTD__c);
	    System.assertEquals(16500,listAccts[0].Parent_Total_Booking_Spend__c);
	    System.assertEquals(300, listAccts[0].Parent_Total_Num_Bookings_for_Account__c);
	    System.assertEquals(16500, listAccts[0].Parent_Total_Bookings_Amount_CY__c);
	    System.assertEquals(5500, listAccts[0].Parent_Total_Bookings_Amount_CM__c);
	    System.assertEquals(dtToday, (date)listAccts[0].Parent_Date_of_Last_Booking__c);
	    
	    System.assertEquals((5500*stageOpenFirst.DefaultProbability)/100 + 11000, listAccts[0].Parent_Total_Expected_Revenue__c);
	    System.assertEquals((5500*stageOpenFirst.DefaultProbability)/100, listAccts[0].Parent_Total_Expected_Revenue_Open_Opps__c);
	    System.assertEquals(11000, listAccts[0].Parent_Total_Expected_Rev_Closed_Opp__c);
	    System.assertEquals(dtToday, listAccts[0].Parent_Max_Opp_Close_Date__c);
	    System.assertEquals(dtToday, listAccts[0].Parent_Min_Opp_Close_Date__c);

	    System.assertEquals(6, listAccts[0].Parent_Total_Complaints__c);
	    System.assertEquals(3, listAccts[0].Parent_Total_Open_Complaints__c);
	    System.assertEquals(5, listAccts[0].Parent_Total_Open_Cases__c);

	    System.assertEquals(7200,listAccts[1].Parent_Total_Spend_YTD__c);
	    System.assertEquals(600,listAccts[1].Parent_Total_Spend_MTD__c);
	    System.assertEquals(13200,listAccts[1].Parent_Total_Booking_Spend__c);
	    System.assertEquals(240, listAccts[1].Parent_Total_Num_Bookings_for_Account__c);
	    System.assertEquals(13200, listAccts[1].Parent_Total_Bookings_Amount_CY__c);
	    System.assertEquals(4400, listAccts[1].Parent_Total_Bookings_Amount_CM__c);
	    System.assertEquals(dtToday, (date)listAccts[1].Parent_Date_of_Last_Booking__c);
	    
	    System.assertEquals((4400*stageOpenFirst.DefaultProbability)/100 + 8800, listAccts[1].Parent_Total_Expected_Revenue__c);
	    System.assertEquals((4400*stageOpenFirst.DefaultProbability)/100, listAccts[1].Parent_Total_Expected_Revenue_Open_Opps__c);
	    System.assertEquals(8800, listAccts[1].Parent_Total_Expected_Rev_Closed_Opp__c);
	    System.assertEquals(dtToday, listAccts[1].Parent_Max_Opp_Close_Date__c);
	    System.assertEquals(dtToday, listAccts[1].Parent_Min_Opp_Close_Date__c);
	    
	    System.assertEquals(3, listAccts[1].Parent_Total_Complaints__c);
	    System.assertEquals(1, listAccts[1].Parent_Total_Open_Complaints__c);
	    System.assertEquals(3, listAccts[1].Parent_Total_Open_Cases__c);
    }

    //--------------------------------------------------------------------------------
    //  testAccountRollupBatch_NOTINHIER()
    // -------------------------------------------------------------------------------
    public static testMethod void testAccountRollupBatch_NOTINHIER() 
    {
    	// Create custom settings to bypass trigger processing.
    	Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = true);
		insert disabletrigger;
		
		//NoValidations__c noValidations = new NoValidations__c (SetupOwnerID = UserInfo.getUserID(), Active_Users__c = true);
		//insert noValidations;

		RecordType rtBusinessAcct = [SELECT ID FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Business_Account' LIMIT 1];
		
		Date dtToday = System.today();
		Date dtLastMonth = System.today().addMonths(-1);
		
        try
        {
	        // ==============================================
	        // Create test data - UP with 2 child accounts - total of 3 locations 
	        // ==============================================
	        Account acctUP1 = new Account(Name = 'AAA-UP1', RecordTypeId = rtBusinessAcct.ID);
	        
	        acctUP1.January_Average__c = acctUP1.February_Average__c = acctUP1.March_Average__c = acctUP1.April_Average__c = acctUP1.May_Average__c = acctUP1.June_Average__c = acctUP1.July_Average__c
 				= acctUP1.August_Average__c = acctUP1.September_Average__c = acctUP1.October_Average__c = acctUP1.November_Average__c = acctUP1.December_Average__c = 100;
	        
		    acctUP1.Total_Open_Cases__c = 1;
		    acctUP1.Total_Complaints__c = 2;
		    acctUP1.Total_Open_Complaints__c = 1;
		    acctUP1.Total_Bookings_Amount_CY__c = 300;
		    acctUP1.Total_Bookings_Amount_CM__c = 100;

		    acctUP1.Parent_Total_Spend_YTD__c = 12000;
		    acctUP1.Parent_Total_Spend_MTD__c = 1000;
		    acctUP1.Parent_Total_Booking_Spend__c = 16500;
		    acctUP1.Parent_Total_Num_Bookings_for_Account__c = 300;
		    acctUP1.Parent_Total_Bookings_Amount_CY__c = 16500;
		    acctUP1.Parent_Total_Bookings_Amount_CM__c = 5500;
		    acctUP1.Parent_Date_of_Last_Booking__c = dtToday;
		    
		    acctUP1.Parent_Total_Expected_Revenue__c = 5500 + 11000; 
		    acctUP1.Parent_Total_Expected_Revenue_Open_Opps__c = 5500; 
		    acctUP1.Parent_Total_Expected_Rev_Closed_Opp__c = 11000;
		    acctUP1.Parent_Max_Opp_Close_Date__c = dtToday;
		    acctUP1.Parent_Min_Opp_Close_Date__c = dtToday;
	
		    acctUP1.Parent_Total_Complaints__c = 6;
		    acctUP1.Parent_Total_Open_Complaints__c = 3;
		    acctUP1.Parent_Total_Open_Cases__c = 5;

	        insert acctUP1;
	        ID idUP1 = acctUP1.Id;
	        
	        // Add an Account to the ultimate parent.
	        Account acctUP1_Child1 = new Account(Name = 'BBB-Num01', ParentID = idUP1, RecordTypeId = rtBusinessAcct.ID);
	        acctUP1_Child1.January_Average__c = acctUP1_Child1.February_Average__c = acctUP1_Child1.March_Average__c = acctUP1_Child1.April_Average__c = acctUP1_Child1.May_Average__c = acctUP1_Child1.June_Average__c = acctUP1_Child1.July_Average__c
 				= acctUP1_Child1.August_Average__c = acctUP1_Child1.September_Average__c = acctUP1_Child1.October_Average__c = acctUP1_Child1.November_Average__c = acctUP1_Child1.December_Average__c = 200;
	        
		    acctUP1_Child1.Total_Open_Cases__c = 1;
		    acctUP1_Child1.Total_Complaints__c = 1;
		    acctUP1_Child1.Total_Open_Complaints__c = 0;
		    acctUP1_Child1.Total_Bookings_Amount_CY__c = 1200;
		    acctUP1_Child1.Total_Bookings_Amount_CM__c = 400;

		    acctUP1_Child1.Parent_Total_Spend_YTD__c = 12000;
		    acctUP1_Child1.Parent_Total_Spend_MTD__c = 1000;
		    acctUP1_Child1.Parent_Total_Booking_Spend__c = 16500;
		    acctUP1_Child1.Parent_Total_Num_Bookings_for_Account__c = 300;
		    acctUP1_Child1.Parent_Total_Bookings_Amount_CY__c = 16500;
		    acctUP1_Child1.Parent_Total_Bookings_Amount_CM__c = 5500;
		    acctUP1_Child1.Parent_Date_of_Last_Booking__c = dtToday;
		    
		    acctUP1_Child1.Parent_Total_Expected_Revenue__c = 5500 + 11000; 
		    acctUP1_Child1.Parent_Total_Expected_Revenue_Open_Opps__c = 5500; 
		    acctUP1_Child1.Parent_Total_Expected_Rev_Closed_Opp__c = 11000;
		    acctUP1_Child1.Parent_Max_Opp_Close_Date__c = dtToday;
		    acctUP1_Child1.Parent_Min_Opp_Close_Date__c = dtToday;
	
		    acctUP1_Child1.Parent_Total_Complaints__c = 6;
		    acctUP1_Child1.Parent_Total_Open_Complaints__c = 3;
		    acctUP1_Child1.Parent_Total_Open_Cases__c = 5;

	        insert acctUP1_Child1;
	        
	        // Add 1 Account not in a hierarchy.
	        Account acctNotInHier = new Account(Name = 'CCC-NotInHier', RecordTypeId = rtBusinessAcct.ID);
			acctNotInHier.Parent_Total_Spend_YTD__c = 12000;
			acctNotInHier.Parent_Total_Spend_MTD__c = 1000;
			acctNotInHier.Parent_Total_Booking_Spend__c = 16500;
			acctNotInHier.Parent_Total_Num_Bookings_for_Account__c = 300;
			acctNotInHier.Parent_Total_Bookings_Amount_CY__c = 16500;
			acctNotInHier.Parent_Total_Bookings_Amount_CM__c = 5500;
			acctNotInHier.Parent_Date_of_Last_Booking__c = dtToday;
			    
			acctNotInHier.Parent_Total_Expected_Revenue__c = 5500 + 11000; 
			acctNotInHier.Parent_Total_Expected_Revenue_Open_Opps__c = 5500; 
			acctNotInHier.Parent_Total_Expected_Rev_Closed_Opp__c = 11000;
			acctNotInHier.Parent_Max_Opp_Close_Date__c = dtToday;
			acctNotInHier.Parent_Min_Opp_Close_Date__c = dtToday;
			
			acctNotInHier.Parent_Total_Complaints__c = 6;
			acctNotInHier.Parent_Total_Open_Complaints__c = 3;
			acctNotInHier.Parent_Total_Open_Cases__c = 5;

	        insert acctNotInHier;
        }
        catch(Exception e)
        {   
            // Error creating contacts - site probably has required custom fields on the object.
            system.debug(Logginglevel.WARN, 'InSitu Test: The following error occurred while creating test Accounts, Bookings and Opportunities: ' + e.getMessage());
        }
		
    	Test.startTest();
    
    	// Just call the static method to execute the queuing of the batch job.
        ID idJob = AccountRollupBatch.runAccountRollupBatch(10, 201, AccountRollupBatch.RUTYPE_NOTINHIER);

    	Test.stopTest();

	    // Query Accounts
	    List<Account> listAccts = [SELECT id, Name, InSituCah__Ultimate_Parent__r.ID, 
										Parent_Total_Spend_YTD__c, 
										Parent_Total_Spend_MTD__c, 
										Parent_Total_Booking_Spend__c, 
										Parent_Total_Num_Bookings_for_Account__c, 
										Parent_Total_Bookings_Amount_CY__c, 
										Parent_Total_Bookings_Amount_CM__c, 
										Parent_Date_of_Last_Booking__c, 
										
										Parent_Total_Expected_Revenue__c, 
										Parent_Total_Expected_Revenue_Open_Opps__c, 
										Parent_Total_Expected_Rev_Closed_Opp__c, 
										Parent_Max_Opp_Close_Date__c, 
										Parent_Min_Opp_Close_Date__c, 
										
										Parent_Total_Complaints__c, 
										Parent_Total_Open_Complaints__c, 
										Parent_Total_Open_Cases__c
									FROM Account Account ORDER BY Name ASC];

	    System.assertEquals(3, listAccts.size());
	    System.assertNotEquals(null,listAccts[0].Parent_Total_Spend_YTD__c);
	    System.assertNotEquals(null,listAccts[0].Parent_Total_Spend_MTD__c);
	    System.assertNotEquals(null,listAccts[0].Parent_Total_Booking_Spend__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Num_Bookings_for_Account__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Bookings_Amount_CY__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Bookings_Amount_CM__c);
	    System.assertNotEquals(null, (date)listAccts[0].Parent_Date_of_Last_Booking__c);
	    
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Expected_Revenue__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Expected_Revenue_Open_Opps__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Expected_Rev_Closed_Opp__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Max_Opp_Close_Date__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Min_Opp_Close_Date__c);

	    System.assertNotEquals(null, listAccts[0].Parent_Total_Complaints__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Open_Complaints__c);
	    System.assertNotEquals(null, listAccts[0].Parent_Total_Open_Cases__c);

	    System.assertNotEquals(null,listAccts[1].Parent_Total_Spend_YTD__c);
	    System.assertNotEquals(null,listAccts[1].Parent_Total_Spend_MTD__c);
	    System.assertNotEquals(null,listAccts[1].Parent_Total_Booking_Spend__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Num_Bookings_for_Account__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Bookings_Amount_CY__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Bookings_Amount_CM__c);
	    System.assertNotEquals(null, (date)listAccts[1].Parent_Date_of_Last_Booking__c);
	    
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Expected_Revenue__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Expected_Revenue_Open_Opps__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Expected_Rev_Closed_Opp__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Max_Opp_Close_Date__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Min_Opp_Close_Date__c);

	    System.assertNotEquals(null, listAccts[1].Parent_Total_Complaints__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Open_Complaints__c);
	    System.assertNotEquals(null, listAccts[1].Parent_Total_Open_Cases__c);
	    
	    System.assertEquals(null,listAccts[2].Parent_Total_Spend_YTD__c);
	    System.assertEquals(null,listAccts[2].Parent_Total_Spend_MTD__c);
	    System.assertEquals(null,listAccts[2].Parent_Total_Booking_Spend__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Num_Bookings_for_Account__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Bookings_Amount_CY__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Bookings_Amount_CM__c);
	    System.assertEquals(null, (date)listAccts[2].Parent_Date_of_Last_Booking__c);
	    
	    System.assertEquals(null, listAccts[2].Parent_Total_Expected_Revenue__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Expected_Revenue_Open_Opps__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Expected_Rev_Closed_Opp__c);
	    System.assertEquals(null, listAccts[2].Parent_Max_Opp_Close_Date__c);
	    System.assertEquals(null, listAccts[2].Parent_Min_Opp_Close_Date__c);
	    
	    System.assertEquals(null, listAccts[2].Parent_Total_Complaints__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Open_Complaints__c);
	    System.assertEquals(null, listAccts[2].Parent_Total_Open_Cases__c);
    }

}