public without sharing class AddLee_IntegrationUtility {

    public static String nameIntegrationSettings = 'shamrock_setting'; 
    public static String nameGeneralSettings = 'settings';
    public static String nameScheduledJobSettings = 'scheduleSettings';
    public static Integer defaultMaxCalloutSingleTask = 10; 
    
    public static boolean isFromContact = false;
    
    
    public static void setIsFromContact(){
        isFromContact = true;
    }
    
    public static boolean getIsFromContact(){
        return isFromContact;
    }
    /*
    public static void sendEmail(String email, String subject, String bodyText, String bodyHtml){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String emailAddr = email;
        String[] toAddresses = new String[] {emailAddr};
        
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(bodyText);
        mail.setHtmlBody(bodyHtml);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    */

    public static List<Account> getListAccount(List<Log__c> listLogs){
        //Create the list of ids
        List<String> listIds = new List<String>();  
        for(Log__c log:listLogs){
            listIds.add(log.Id);
        }
        
        System.debug('<<<GETLISTACCOUNT listIds>>>:'+listIds);
        
        List<Account> accounts = [SELECT Id, Name, Account_Number__c, AccountNumber, PersonEmail, ExsistInShamrock__c, ParentId , 
                                    (SELECT Email FROM Contacts), 
                                    (SELECT Status__c, Type__c, isWaiting__c,isFailed__c,isCompleted__c FROM Logs__r WHERE Id IN :listIds) 
                                 FROM Account WHERE id in  (SELECT Account__c FROM Log__c WHERE Id IN :listIds) ];
        
        System.debug('<<<GETLISTACCOUNT accounts>>>:'+accounts);
        
        return accounts;
    }
    
    public static RecordType getRecordTypeByName(String recordTypeDeveloperName){
        return [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = :recordTypeDeveloperName];     
    }
    
    public static String getSessionId(){
        Log_Integration__c logIS = Log_Integration__c.getValues(nameIntegrationSettings);
        if(logIS != null)
            return logIS.CurrentSessionId__c;
        else
            return null; 
    }   
    
    public static Boolean storeSessionId(String sessionId){
        Log_Integration__c logIS = Log_Integration__c.getValues(nameIntegrationSettings);
        logIS.CurrentSessionId__c = sessionId;
        
        try{
            update logIS;
            return true;
        }catch(Exception e){
            return false;
        }
    }   
    
    public static String getUsername(){
        Log_Integration__c logIS = Log_Integration__c.getValues(nameIntegrationSettings);
        if(logIS != null)
            return logIS.Username__c;
        else
            return null; 
    }

    public static String getPassword(){
        Log_Integration__c logIS = Log_Integration__c.getValues(nameIntegrationSettings);
        if(logIS != null)
            return logIS.Password__c;
        else
            return null; 
    }


    public static Integer getMaxCalloutSingleTask() {
        Log_GeneralSettings__c callouts = Log_GeneralSettings__c.getValues(nameGeneralSettings);
        if(callouts != null)
            return Integer.valueOf(callouts.numCallout__c);
        else
            return defaultMaxCalloutSingleTask;
    }
    
    public static Integer getMaxCalloutScheduledTask() {
        Log_GeneralSettings__c callouts = Log_GeneralSettings__c.getValues(nameScheduledJobSettings);
        if(callouts != null)
            return Integer.valueOf(callouts.numCallout__c);
        else
            return defaultMaxCalloutSingleTask;
    }

    public static Integer GetNumFutureCallsInLast24Hours() {
        return [select count() from AsyncApexJob 
                where CreatedDate >= :Datetime.now().addHours(-24) 
                and JobType = 'Future'];
    }
    
    public static Integer getMaxFutureCallsAllowed() {
        Integer usersCount = [
            SELECT COUNT()
            FROM User 
            WHERE Profile.UserLicense.LicenseDefinitionKey IN ('SFDC','AUL','PID_FDC_FREE')];
        return Math.max(250000, usersCount*200);
    }

    // Can we run future calls right now?
    public static boolean CanUseFutureContext() {
        System.debug('<<<FUTURE_CALL_LIMIT>>>:'+Limits.getLimitFutureCalls());
        System.debug('<<<FUTURE_CALL_USED>>>:'+Limits.getFutureCalls());
        
        boolean callStackLimitExceeded = (Limits.getFutureCalls() >= Limits.getLimitFutureCalls());
        if (!callStackLimitExceeded) {
            // Check 24-hour rolling window limit
            // of 200 * # of Salesforce, Salesforce Platform, or Force.com One App Licenses
            // *NOTE 1*: These queries may be cacheable depending on your situation
            // *NOTE 2*: If you have not used up all of your allocated licenses,
            // you will have more available licenses than this, so you may need to adjust.
            // However, you can't use SOQL to determine 
            //how many AVAILABLE licenses you have, 
            // only how many have actually been assigned to users
            return GetNumFutureCallsInLast24Hours() < getMaxFutureCallsAllowed();
        } else return false;
    }
    
    //Added 16/09/2014 For CaseBrowser
    public static AddLee_SoapIntegration_Test_v2_Fix.jobsResponse getJobs(Long accountNumber,DateTime dateFrom,
                                                                        DateTime dateTo,Long invoiceNumber,
                                                                        Long jobNumber,Integer maxJobsResult,
                                                                        String sessionHandle){
        try{
            AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
            return soapClient.getJobs(accountNumber, dateFrom, dateTo, invoiceNumber, jobNumber,maxJobsResult, sessionHandle);
        }catch (Exception e){
            System.DEBUG('Exception e : ' + e);
        }
        return null;
    }

    //Added 16/09/2014 For CaseBrowser
    public static AddLee_SoapIntegration_Test_v2_Fix.jobDetailsResponse getJobDetails(Long id,
                                                                                        Boolean isDocket, 
                                                                                        String sessionHandle){
        try{
            AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
            return soapClient.getJobDetails(id, isDocket, sessionHandle);
        }catch(Exception e){
            System.DEBUG('Exception e : ' + e);
        }

        return null;
    }

    public static Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> createSoapCustomer(Id accountId, Id parentId){
        Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> toReturnCustomerMapList = new Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer>();
        //List<AddLee_SoapIntegration_Test_v2_Fix.customer> toReturnCustomerList = new List<AddLee_SoapIntegration_Test_v2_Fix.customer>();
        AddLee_SoapIntegration_Test_v2_Fix.customer toReturnCustomerParent = new AddLee_SoapIntegration_Test_v2_Fix.customer();
        AddLee_SoapIntegration_Test_v2_Fix.customer toReturnCustomerAccount = new AddLee_SoapIntegration_Test_v2_Fix.customer();
        List<Account> account = new List<Account>();
        Map<Id,Account> accountMap;
        account = [SELECT Id, Name, SHM_invoicingPolicy_code__c, 
                        SHM_invoicingPolicy_id__c, 
                        SHM_invoicingPolicy_name__c,
                        SHM_message__c,
                        SHM_name__c,
                        SHM_number_x__c,
                        SHM_serviceCharge__c,
                        SHM_type_x__c,
                        SHM_useWeb__c,
                        SHM_discountRate__c,
                        SHM_vatRate__c,
                        SHM_bookingNote__c,
                        SHM_creditDays__c,
                        SHM_creditLimit__c,
                        SHM_generateIndividualInvoice__c,
                        SHM_invoiceClearanceType__c,
                        SHM_id__c,
                        SHM_birthday__c,
                        SHM_status_code__c,
                        SHM_status_id__c,
                        SHM_status_name__c,
                        SHM_substatus_code__c,
                        SHM_substatus_id__c,
                        SHM_substatus_name__c,
                        SHM_bookingEmail__c,
                        SHM_salesLedger_code__c,
                        SHM_salesLedger_id__c,
                        SHM_salesLedger_name__c,
                        SHM_grade_id__c,
                        SHM_grade_name__c,
                        SHM_main_address__c,
                        SHM_main_contactName__c,
                        SHM_main_email__c,
                        SHM_main_fax__c,
                        SHM_main_latitude__c,
                        SHM_main_longitude__c,
                        SHM_main_postcode__c,
                        SHM_main_telephone__c,
                        SHM_main_town__c,
                        SHM_billing_address__c,
                        SHM_billing_contactName__c,
                        SHM_billing_email__c,
                        SHM_billing_fax__c,
                        SHM_billing_latitude__c,
                        SHM_billing_longitude__c,
                        SHM_billing_postcode__c,
                        SHM_billing_telephone__c,
                        SHM_billing_town__c,
                        SHM_Bank_Account_Number__c,
                        SHM_Bank_Sort_Code__c,
                        SHM_Bank_Address__c,
                        SHM_Bank_c__c, 
                        SHM_Bank_Date_DD__c,
                        SHM_Bank_Account_Name__c,
                        SHM_Card_Expiry_Date__c,
                        SHM_Card_Holder_Name__c,
                        SHM_Card_Long_Number__c,
                        SHM_Business_Area__c,   
                        SHM_Account_Product__c,    
                        SHM_How_did_you_hear_about_us__c,       
                        SHM_Salesman__c 
          FROM Account WHERE Id = :accountId OR Id =: parentId];  
        if(account.size() > 0){
             accountMap = new Map<Id,Account>(account);
        }    
        toReturnCustomerAccount = getCustomer(accountMap.get(accountId));
        toReturnCustomerMapList.put('childAccount',toReturnCustomerAccount);
        
        if(parentId != null){
            toReturnCustomerParent = getCustomer(accountMap.get(parentId));
            toReturnCustomerMapList.put('parentAccount',toReturnCustomerParent);
        }
        
        
        //Account acc  = accountMap.get(accountId);
                                
        
        
        //toReturnCustomerList.add(toReturnCustomer);
        return toReturnCustomerMapList;
    }
    public static Integer getIntegerValue(Decimal val){
        if(val != null)
            return Integer.valueOf(val);
        
        return 0;
    }
    
    public static Long getLongValue(String val){
        if(val != null)
            return Long.valueOf(val);
        
        return 0;
    }


    public static Integer getIntegerValue(String val){
        if(val != null)
            return Integer.valueOf(val);
        
        return 0;
    }
    
    public static Decimal getDecimalValue(String val){
        if(val != null)
            return Decimal.valueOf(val);
        
        return 0;
    }
    
    public static AddLee_SoapIntegration_Test_v2_Fix.customer getCustomer(Account acc){
        System.debug('<<<CHECKPOINT_ACCOUNT>>>: '+acc);
        
        AddLee_SoapIntegration_Test_v2_Fix.customer toReturnCustomer = new AddLee_SoapIntegration_Test_v2_Fix.customer();
        
        AddLee_SoapIntegration_Test_v2_Fix.agent cst_accountMng = new AddLee_SoapIntegration_Test_v2_Fix.agent();
        AddLee_SoapIntegration_Test_v2_Fix.bank cst_bank = new AddLee_SoapIntegration_Test_v2_Fix.bank();
        AddLee_SoapIntegration_Test_v2_Fix.creditCard cst_creditCard = new AddLee_SoapIntegration_Test_v2_Fix.creditCard();
        AddLee_SoapIntegration_Test_v2_Fix.businessArea cst_businessArea = new AddLee_SoapIntegration_Test_v2_Fix.businessArea(); 
        AddLee_SoapIntegration_Test_v2_Fix.site cst_billingAddress = new AddLee_SoapIntegration_Test_v2_Fix.site();
        AddLee_SoapIntegration_Test_v2_Fix.site cst_mainAddress = new AddLee_SoapIntegration_Test_v2_Fix.site();
        AddLee_SoapIntegration_Test_v2_Fix.grade cst_grade = new AddLee_SoapIntegration_Test_v2_Fix.grade();
        AddLee_SoapIntegration_Test_v2_Fix.invoicingPolicy cst_invoicingPolicy = new AddLee_SoapIntegration_Test_v2_Fix.invoicingPolicy();
        AddLee_SoapIntegration_Test_v2_Fix.salesLedger cst_salesLedger = new AddLee_SoapIntegration_Test_v2_Fix.salesLedger();
        AddLee_SoapIntegration_Test_v2_Fix.agent cst_salesman = new AddLee_SoapIntegration_Test_v2_Fix.agent();
        AddLee_SoapIntegration_Test_v2_Fix.status cst_status = new AddLee_SoapIntegration_Test_v2_Fix.status();
        AddLee_SoapIntegration_Test_v2_Fix.subStatus cst_substatus = new AddLee_SoapIntegration_Test_v2_Fix.subStatus();
        
        //added in 22/09/2015
        AddLee_SoapIntegration_Test_v2_Fix.productOffer cst_productoffer = new AddLee_SoapIntegration_Test_v2_Fix.productOffer ();
        cst_productoffer.name =acc.SHM_Account_Product__c;
       // AddLee_SoapIntegration_Test_v2_Fix.product cst_product = new AddLee_SoapIntegration_Test_v2_Fix.product();
       // cst_product .Offers.add( cst_productoffer) ;
       // cst_product .loyaltyGrade  = null;
        
        /*********************************************************************/
       // system.debug('cst_product>>>>>'+ cst_product ); 
        System.debug('<<<CHECKPOINT_SOAP_1>>>');
            
        //SFDC Record TYpe
        //Business Account  Standard
        //Consumer Account  Person
        //Indipendent Traveller Person
        //Personal Custom   Person
        //Supplier/Partner  Standard
        
        //Billing address
        AddLee_SoapIntegration_Test_v2_Fix.location billing_location = new AddLee_SoapIntegration_Test_v2_Fix.location();
        billing_location.address        = acc.SHM_billing_address__c;
        billing_location.latitude       = getDecimalValue(acc.SHM_billing_latitude__c);             
        billing_location.longitude      = getDecimalValue(acc.SHM_billing_longitude__c);            
        billing_location.postcode       = acc.SHM_billing_postcode__c;
        billing_location.town           = acc.SHM_billing_town__c;
        cst_billingAddress.contactName  = acc.SHM_billing_contactName__c; 
        cst_billingAddress.email        = acc.SHM_billing_email__c;            
        cst_billingAddress.fax          = acc.SHM_billing_fax__c;              
        cst_billingAddress.telephone    = acc.SHM_billing_telephone__c;
        cst_billingAddress.location     = billing_location;
        
        
        System.debug('<<<CHECKPOINT_SOAP_2>>>');
        
        //Main address details
        AddLee_SoapIntegration_Test_v2_Fix.location main_location = new AddLee_SoapIntegration_Test_v2_Fix.location();
        main_location.address       = acc.SHM_main_address__c;               
        main_location.latitude      = getDecimalValue(acc.SHM_main_latitude__c);                    
        main_location.longitude     = getDecimalValue(acc.SHM_main_longitude__c);               
        main_location.postcode      = acc.SHM_main_postcode__c;
        main_location.town          = acc.SHM_main_town__c;
        cst_mainAddress.contactName = acc.SHM_main_contactName__c;
        cst_mainAddress.email       = acc.SHM_main_email__c;
        cst_mainAddress.fax         = acc.SHM_main_fax__c;
        cst_mainAddress.telephone   = acc.SHM_main_telephone__c;
        cst_mainAddress.location    = main_location;
            
        System.debug('<<<CHECKPOINT_SOAP_3>>>');

        //Grade 
        cst_grade.id    = getIntegerValue(acc.SHM_grade_id__c);
        cst_grade.name  = acc.SHM_grade_name__c;
            
        //InvoicingPolicy
        cst_invoicingPolicy.code    = acc.SHM_invoicingPolicy_code__c;
        cst_invoicingPolicy.id      = getIntegerValue(acc.SHM_invoicingPolicy_id__c);
        cst_invoicingPolicy.name    = acc.SHM_invoicingPolicy_name__c;  
            
        //SalesLedger
        cst_salesLedger.code    = acc.SHM_salesLedger_code__c;
        cst_salesLedger.id      = getLongValue(String.ValueOf(acc.SHM_salesLedger_id__c));
        cst_salesLedger.name    = acc.SHM_salesLedger_name__c;

        //Status
        cst_status.code     = acc.SHM_status_code__c;
        cst_status.id       = getIntegerValue(acc.SHM_status_id__c);
        cst_status.name     = acc.SHM_status_name__c;
            
        //Substatus
        cst_substatus.code  = acc.SHM_substatus_code__c;
        cst_substatus.id    = getIntegerValue(acc.SHM_substatus_id__c);
        cst_substatus.name  = acc.SHM_substatus_name__c;       

        //bank
        cst_bank.accountNumber = Addlee_Crypto.decrypt(acc.SHM_Bank_Account_Number__c, Addlee_Crypto.getCryptoKey());
        cst_bank.sortCode = Addlee_Crypto.decrypt(acc.SHM_Bank_Sort_Code__c, Addlee_Crypto.getCryptoKey());
        cst_bank.address = acc.SHM_Bank_Address__c;
        cst_bank.name = acc.SHM_Bank_c__c;  
        cst_bank.accountName = acc.SHM_Bank_Account_Name__c; 
        cst_bank.mandateStartDate = acc.SHM_Bank_Date_DD__c;

        //cst_creditCard
        cst_creditCard.holderName = acc.SHM_Card_Holder_Name__c;
        cst_creditCard.cardNumber = Addlee_Crypto.decrypt(acc.SHM_Card_Long_Number__c, Addlee_Crypto.getCryptoKey());//acc.SHM_Card_Long_Number__c;
        cst_creditCard.expiryDate = acc.SHM_Card_Expiry_Date__c;
        
        //businessArea
        cst_businessArea.name = acc.SHM_Business_Area__c;

        System.DEBUG(logginglevel.ERROR, 'Bank : ' + cst_bank);
        
        // Account Product 
        //cst_product.loyaltyGrade = acc.SHM_Account_Product__c;
        
        
        // Account Product      
        
        

        
        //Customer Salesman
        cst_accountMng.name = acc.SHM_Salesman__c;

        System.DEBUG(logginglevel.ERROR, 'Account Salesman : ' + cst_accountMng);
        
        //Object related
        toReturnCustomer.billingAddress     = cst_billingAddress;
        toReturnCustomer.mainAddress        = cst_mainAddress;
        toReturnCustomer.grade              = cst_grade;
        toReturnCustomer.invoicingPolicy    = cst_invoicingPolicy;
        toReturnCustomer.salesLedger        = cst_salesLedger;
        toReturnCustomer.status             = cst_status;
        toReturnCustomer.subStatus          = cst_substatus;
        toReturnCustomer.bank               = cst_bank;
        toReturnCustomer.creditCard         = cst_creditCard;
        toReturnCustomer.businessArea       = cst_businessArea;
        toReturnCustomer.salesman           = cst_accountMng;
        //toReturnCustomer.offers           = cst_product;
        toReturnCustomer.productOffer       = cst_productoffer;
       
        
        //Other Fields
        toReturnCustomer.birthDate                  = acc.SHM_birthday__c;               
        toReturnCustomer.bookingEmail               = acc.SHM_bookingEmail__c;              
        toReturnCustomer.bookingNote                = acc.SHM_bookingNote__c;           
        toReturnCustomer.creditDays                 = getIntegerValue(acc.SHM_creditDays__c);              
        toReturnCustomer.creditLimit                = acc.SHM_creditLimit__c;                   
        toReturnCustomer.discountRate               = acc.SHM_discountRate__c;                  
        toReturnCustomer.generateIndividualInvoice  = acc.SHM_generateIndividualInvoice__c; 
        toReturnCustomer.id                         = getLongValue(acc.SHM_id__c);          
        toReturnCustomer.invoiceClearanceType       = acc.SHM_invoiceClearanceType__c;      
        toReturnCustomer.message                    = acc.SHM_message__c;           
        toReturnCustomer.name                       = acc.SHM_name__c;      
        toReturnCustomer.number_x                   = (Long)Decimal.valueOf(acc.SHM_number_x__c);                
       // toReturnCustomer.serviceCharge              = acc.SHM_serviceCharge__c;             
        toReturnCustomer.type_x                     = acc.SHM_type_x__c;        
        toReturnCustomer.useWeb                     = acc.SHM_useWeb__c;                        
        toReturnCustomer.vatRate                    = acc.SHM_vatRate__c;
        return toReturnCustomer;
    }
}