<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom object to track agent error forms, or other types of agent evaluation.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Agent_Name__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Case__r.Agent_Name__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Agent Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Agent Evaulations</relationshipLabel>
        <relationshipName>Agent_Evaulations</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Date_and_Time_of_Conversation__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>When the manager and agent spoke</inlineHelpText>
        <label>Date and Time of Conversation</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Details_of_Error__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Case__r.Case_Sub_Type__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Comes from Case Sub-Type</inlineHelpText>
        <label>Details of Error</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Error_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>When the booking was taken</inlineHelpText>
        <label>Error Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Error_Details__c</fullName>
        <description>replaces formula field Details of Error now that those values are no longer case sub-types.</description>
        <externalId>false</externalId>
        <inlineHelpText>What did the agent do incorrectly?</inlineHelpText>
        <label>Error Details</label>
        <picklist>
            <picklistValues>
                <fullName>Booked in error</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cancelled in Error</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Comm Error</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Could Not Locate Booking</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Delay in confirming booking</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Double booked</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect account number</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect address</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect Date/Time</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect email address</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect Package</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect passenger details</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect Pin</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect quote</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect Service</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Feedback__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>What was covered during the discussion?</inlineHelpText>
        <label>Feedback</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Job_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Job_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Job Number that the error occurred on</inlineHelpText>
        <label>Job Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Team_Name__c</fullName>
        <description>Team of the agent who made the error</description>
        <externalId>false</externalId>
        <inlineHelpText>Team of the agent who made the error</inlineHelpText>
        <label>Team Name</label>
        <picklist>
            <picklistValues>
                <fullName>Angel</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Bank</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Battersea</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Belgravia</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fleet Street</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hatton Garden</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hyde Park Corner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ife&apos;s Angels</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Kingsway</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Knightsbridge</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Leicester Square</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Mayfair</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Nemesis</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Nine Elms Lane</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Premier Team</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resource Planning</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Shoreditch</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Team Managers</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>The Shard London Bridge</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tottenham</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Who_Spoke_to_the_Agent__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Manager who spoke to the agent</inlineHelpText>
        <label>Who Spoke to the Agent</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Agent_Evaulations</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Agent Evaluation</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Agent Evaluation Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Agent Evaulations</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Agent_Error_Form</fullName>
        <active>true</active>
        <description>Contact Centre Agent Error Form</description>
        <label>Agent Error Form</label>
        <picklistValues>
            <picklist>Error_Details__c</picklist>
            <values>
                <fullName>Booked in error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Cancelled in Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Comm Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Could Not Locate Booking</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Delay in confirming booking</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Double booked</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect Date%2FTime</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect Package</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect Pin</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect Service</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect account number</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect address</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect email address</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect passenger details</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incorrect quote</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Team_Name__c</picklist>
            <values>
                <fullName>Angel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Bank</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Battersea</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Belgravia</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Fleet Street</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hatton Garden</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hyde Park Corner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Ife%27s Angels</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Kingsway</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Knightsbridge</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Leicester Square</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mayfair</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nemesis</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nine Elms Lane</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Premier Team</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Resource Planning</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Shoreditch</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Team Managers</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>The Shard London Bridge</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tottenham</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
